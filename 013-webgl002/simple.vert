#version 450

precision mediump float;

layout(location = 0) in vec2 a_position;
layout(location = 1) in vec4 a_color;
layout(binding = 0) uniform Uniform {mat4 u_matrix;} ubo;
layout(location = 0) out vec4 v_color;

void main() {
	gl_Position = vec4((ubo.u_matrix * vec4(a_position, 0, 0)).xyz, 20);
	v_color = a_color;
}

