#version 450

layout(set = 2, binding = 0) uniform sampler2D u_image;
layout(set = 1, binding = 0) uniform Uniform1 {
	float u_kernel[9];
} ubo;
layout(set = 1, binding = 1) uniform Uniform2 {
	float u_kernelWeight;
} ubo2;
layout(location = 0) in vec2 v_textCoord;
layout(location = 0) out vec4 outColor;

void main() {
	vec2 onePixel = vec2(1) / vec2(textureSize(u_image, 0));
	vec4 colorSum = 
		texture(u_image, v_textCoord + onePixel * vec2(-1, -1)) * ubo.u_kernel[0] +
		texture(u_image, v_textCoord + onePixel * vec2(0, -1)) * ubo.u_kernel[1] +
		texture(u_image, v_textCoord + onePixel * vec2(1, -1)) * ubo.u_kernel[2] +
		texture(u_image, v_textCoord + onePixel * vec2(-1, 0)) * ubo.u_kernel[3] +
		texture(u_image, v_textCoord + onePixel * vec2(0, 0)) * ubo.u_kernel[4] +
		texture(u_image, v_textCoord + onePixel * vec2(1, 0)) * ubo.u_kernel[5] +
		texture(u_image, v_textCoord + onePixel * vec2(-1, 1)) * ubo.u_kernel[6] +
		texture(u_image, v_textCoord + onePixel * vec2(0, 1)) * ubo.u_kernel[7] +
		texture(u_image, v_textCoord + onePixel * vec2(1, 1)) * ubo.u_kernel[8];
	outColor = vec4((colorSum / ubo2.u_kernelWeight).rgb, 1);
}

