#ifdef _WIN32
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#endif
#include <vulkan_core.h>
#include <glfw_control.h>
#include <vulkan_exception.h>
#include <image_loader.h>
#include <filesystem>
#include <iostream>
#include <execution>
#include <map>
#include <fstream>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_MULTIPLE_MASTERS_H
#include <unicode/unistr.h>

using macsnet::vulkan::glfw::GLFWControl;
using macsnet::vulkan::core::VulkanCore;
using macsnet::vulkan::core::Buffers;
using macsnet::vulkan::core::CoreInit;
using macsnet::vulkan::exception::VulkanException;

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;
const bool WINDOW_FULL = false;
const int MAX_FRAMES_IN_FLIGHT = 2;
const bool RESIZE = true;
const std::string APPNAME = "Freetype";

#ifdef _WIN32
#define STR std::wstring
#define STRP(arg) L##arg
#else
#define STR std::string
#define STRP(arg) arg
#endif

class Texture {
	public:
		Texture(const std::filesystem::path &font, const STR &text);
		~Texture();
		const void *getData() const;
		size_t getSize() const;
		uint32_t getWidth() const;
		uint32_t getHeight() const;
	private:
		std::unique_ptr<unsigned char[]> data;
		size_t size = 0;
		uint32_t w = 640;
		uint32_t h = 470;
};

Texture::Texture(const std::filesystem::path &font, const STR &text)
{
	size = w * h * 4;
	data = std::make_unique<unsigned char[]>(size);
	for (size_t i = 0; i < h; ++i) {
		for (size_t j = 0; j < w ; ++j) {
			auto pos = (i * w + j) * 4;
			data[pos + 0] = i < h / 2 ? 255 : 0;
			data[pos + 1] = i < h / 2 ? 0 : 255;
			data[pos + 2] = j < w / 2 ? 255 : 0;
			data[pos + 3] = 255;
		}
	}
	FT_Library ftlib;
	FT_Face face;
	FT_Init_FreeType(&ftlib);
	std::ifstream ifs;
	ifs.open(font, std::ios::binary | std::ios::ate);
	std::vector<char> d(ifs.tellg());
	ifs.seekg(0);
	ifs.read(d.data(), d.size());
	FT_New_Memory_Face(ftlib, reinterpret_cast<FT_Byte *>(d.data()), d.size(), 0, &face);
	FT_Set_Char_Size(face, 0, 46 * 64, 0, 100);
	//FT_Set_Named_Instance(face, 3);
	size_t width = 0;
	size_t height = 0;
#ifdef _WIN32
	icu::UnicodeString textu32(text.c_str());
#else
	icu::UnicodeString textu32 = icu::UnicodeString::fromUTF8(text);
#endif
	for (int32_t i = 0; i < textu32.length(); ++i) {
		FT_Load_Char(face, textu32[i], FT_LOAD_NO_BITMAP);
		width += face->glyph->advance.x >> 6;
		height = std::max(height, static_cast<size_t>(face->glyph->bitmap_top));
	}
	size_t x = w / 2 - width / 2;
	size_t y = h / 2 + height / 2;
	for (int32_t i = 0; i < textu32.length(); ++i) {
		FT_Load_Char(face, textu32[i], FT_LOAD_RENDER);

		for (size_t i = 0; i < face->glyph->bitmap.rows; ++i) {
			for (size_t j = 0; j < face->glyph->bitmap.width; ++j) {
				std::array<unsigned char, 3> textColor {255, 255, 0};
				float alfa = static_cast<float>(face->glyph->bitmap.buffer[i * face->glyph->bitmap.width + j]) / 255.0f;
				auto pos = ((y - face->glyph->bitmap_top + i) * w + j + x) * 4;
				for (size_t i = 0; i < 3; ++i) {
					data[pos + i] = static_cast<unsigned char>(alfa * textColor[i] + (1 - alfa) * data[pos + i]);
				}
			}
		}
		x += face->glyph->advance.x >> 6;
	}

	FT_Done_Face(face);
	FT_Done_FreeType(ftlib);
}

Texture::~Texture()
{
}

const void *Texture::getData() const
{
	return data.get();
}

size_t Texture::getSize() const
{
	return size;
}

uint32_t Texture::getWidth() const
{
	return w;
}

uint32_t Texture::getHeight() const
{
	return h;
}

class VulkanApp {
	public:
		VulkanApp(const STR &text);
		~VulkanApp();
		void init(bool debug, const std::filesystem::path &path);
		void run();
		void cleanup();

	private:
		struct Vertex {
			glm::vec2 pos;
			glm::vec2 textcoord;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(Vertex),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(Vertex, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(Vertex, textcoord),
				}};
			};
		};

		static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBindings() {
			return {{{
				.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
			}}};
		}

		void recordCommandBuffer(size_t ind);
		void recordCommandBuffer(const vk::CommandBuffer &buf, const vk::RenderPass &rp, const vk::Framebuffer &fb, const vk::Extent2D &ext, const vk::Pipeline &p, const vk::PipelineLayout &pl, const std::vector<vk::DescriptorSet> &ds, const std::vector<vk::ClearValue> &cv);
		void updateUniformBuffer(size_t ind);
		void fillVerticies();
		void printFPS();

		std::string m_appName {APPNAME};
		std::unique_ptr<GLFWControl> m_pWindowControl;
		VulkanCore m_core;
		std::vector<Vertex> verticies;
		std::unique_ptr<Buffers> m_Buffers;
		std::vector<vk::CommandBuffer> m_cmdBufs;
		STR text;
};

VulkanApp::VulkanApp(const STR &text) : m_core(m_appName), text(text)
{
}

VulkanApp::~VulkanApp()
{
}

void VulkanApp::fillVerticies()
{
	auto [width, height] = m_core.getTextureResolution(0);
	verticies = {
			{{0, 0}, {0.0, 0.0}},
			{{width, 0}, {1.0, 0.0}},
			{{0, height}, {0.0, 1.0}},
			{{0, height}, {0.0, 1.0}},
			{{width, 0}, {1.0, 0.0}},
			{{width, height}, {1.0, 1.0}},
			};
}

void VulkanApp::init(bool debug, const std::filesystem::path &path)
{
	m_pWindowControl = std::make_unique<GLFWControl>(m_appName);
	m_pWindowControl->init(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FULL, RESIZE);
	Texture l(path / "../../fonts/Nunito-SemiBold.ttf", text);
	m_core.init(CoreInit {
		.pWindowControl = m_pWindowControl.get(),
		.debug = debug,
	   	.vShader = path / "simple.vert.spv",
	   	.fShader = path / "simple.frag.spv",
	   	.clockwise = true,
	   	.maxFramesInFlight = MAX_FRAMES_IN_FLIGHT,
		.getVertexBindingDescription = Vertex::getBindingDescription,
		.getVertexAttributeDescription = Vertex::getAttributeDescription,
		.getLayoutBindings = getLayoutBindings,
		.uniforms = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(glm::vec2)}}),
		.texturesData = {{l.getData(), l.getSize(), l.getWidth(), l.getHeight(), true}},
		.textureFormat = vk::Format::eR8G8B8A8Unorm,
	});
	fillVerticies();
	auto swapCount = m_core.getImageCount();
	m_cmdBufs = m_core.createCommandBuffer(swapCount);
	m_Buffers = m_core.newBuffers();
	m_Buffers->create({
		{verticies.data(), verticies.size() * sizeof(verticies[0]), vk::BufferUsageFlagBits::eVertexBuffer},
	}, true);
}

void VulkanApp::recordCommandBuffer(size_t ind)
{
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse,
	};
	m_cmdBufs[ind].begin(beginInfo);

	recordCommandBuffer(m_cmdBufs[ind], m_core.getRenderPass(), m_core.getFb(ind), m_core.getCurrentExtent(), m_core.getPipeline(), m_core.getPipelineLayout(), m_core.getDescSets(ind), m_core.getClearValue({0.3f, 0.9f, 0.9f, 1.0f}));

	m_cmdBufs[ind].end();
}

void VulkanApp::recordCommandBuffer(const vk::CommandBuffer &buf, const vk::RenderPass &rp, const vk::Framebuffer &fb, const vk::Extent2D &ext, const vk::Pipeline &p, const vk::PipelineLayout &pl, const std::vector<vk::DescriptorSet> &ds, const std::vector<vk::ClearValue> &cv)
{
	vk::RenderPassBeginInfo renderPassInfo {
		.renderPass = rp,
		.framebuffer = fb,
		.renderArea {{0, 0}, ext},
		.clearValueCount = static_cast<uint32_t>(cv.size()),
		.pClearValues = cv.data(),
	};

	buf.beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
	buf.bindPipeline(vk::PipelineBindPoint::eGraphics, p);
	buf.bindVertexBuffers(0, {m_Buffers->getBuffer(0)}, {0});
	buf.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, pl, 0, ds, {}); 
	buf.draw(verticies.size(), 1, 0, 0);
	buf.endRenderPass();
}

void VulkanApp::printFPS()
{
	static auto fpsStartTime = std::chrono::high_resolution_clock::now();
	auto currentTime = std::chrono::high_resolution_clock::now();
	size_t t = std::chrono::duration<size_t, std::chrono::seconds::period>(std::chrono::duration_cast<std::chrono::seconds>(currentTime - fpsStartTime)).count();
	static size_t frames = 0;
	static bool printed = false;
	if (t && t % 10 == 0) {
		if (!printed) {
			printed = true;
			std::cout << "fps: " << frames / t << std::endl;
			frames = 0;
			fpsStartTime = std::chrono::high_resolution_clock::now();
		}
	} else {
		printed = false;
	}
	++frames;
}

void VulkanApp::updateUniformBuffer(size_t ind)
{
	printFPS();
	glm::vec2 resolution {m_core.getCurrentExtent().width, m_core.getCurrentExtent().height};
	m_core.updateUniform(&resolution, sizeof(resolution), ind, 0);

	m_core.renderScene(m_cmdBufs[ind], ind);
}

void VulkanApp::cleanup()
{
	m_core.waitIdle();
	if (m_Buffers) {
		m_Buffers->destroy();
	}
	m_core.destroyCommandBuffer(m_cmdBufs);
	m_core.cleanup();
	m_pWindowControl->cleanup();
}

void VulkanApp::run()
{
	auto func = [&](bool recreate) {
		if (recreate) {
			m_core.recreateSwapChain();
		}
		for (uint32_t i = 0; i < m_cmdBufs.size(); ++i) {
			recordCommandBuffer(i);
		}
	};
	func(false);
	while (!m_pWindowControl->shouldClose()) {
		m_pWindowControl->pollEvents();
		if (m_pWindowControl->wasResized()) {
			func(true);
		}
		try {
			auto ind = m_core.getImageIndex();
			updateUniformBuffer(ind);
		} catch (VulkanException &e) {
			if (e.isSubopt()) {
				func(true);
			} else {
				throw;
			}
		} catch (vk::OutOfDateKHRError &) {
			func(true);
		}
	}
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	auto argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#else
int main(int argc, const char *argv[])
{
#endif
	STR text {STRP("Specify text as param")};
	auto ret = EXIT_SUCCESS;
	if (argc > 1) {
		text = argv[1];
	}
	VulkanApp app(text);
	try {
		app.init(DBG, std::filesystem::path(argv[0]).parent_path());
		app.run();
	} catch (VulkanException &e) {
		std::cout << e.what() << std::endl;
		ret = EXIT_FAILURE;
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		ret =  EXIT_FAILURE;
	}
	app.cleanup();
	return ret;
}

