#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>
#include <vulkan_base.h>
#include <vulkan_exception.h>

namespace macsnet {
namespace vulkan {
namespace base {

std::vector<vk::ExtensionProperties> vulkanEnumExtProps()
{
	auto res = vk::enumerateInstanceExtensionProperties(nullptr);
	for (const auto &e : res) {
		std::cout << "Instance extension: " << e.extensionName << std::endl;
	}
	return res;
}

void vulkanGetPhysicalDevices(const vk::Instance &inst, const vk::SurfaceKHR &surface, VulkanPhysicalDevices &physDevices)
{
	physDevices.m_devices = inst.enumeratePhysicalDevices();
	size_t di = 0;
	for (const auto &dev : physDevices.m_devices) {
		std::cout << "Device: " << di++ << std::endl;
		physDevices.m_devProps.push_back(dev.getProperties());
		std::cout << "\tName: " << physDevices.m_devProps.back().deviceName << std::endl;
		auto apiVer = physDevices.m_devProps.back().apiVersion;
		std::cout << "\tAPI version: " << VK_VERSION_MAJOR(apiVer) << "." << VK_VERSION_MINOR(apiVer) << "." << VK_VERSION_PATCH(apiVer) << std::endl;
		physDevices.m_qFamilyProps.push_back(dev.getQueueFamilyProperties());
		physDevices.m_qSupportsPresent.push_back({});
		for (size_t q = 0; q < physDevices.m_qFamilyProps.back().size(); ++q) {
			physDevices.m_qSupportsPresent.back().push_back(dev.getSurfaceSupportKHR(q, surface));
		}
		physDevices.m_surfaceFormats.push_back(dev.getSurfaceFormatsKHR(surface));
		for (const auto &sf : physDevices.m_surfaceFormats.back()) {
			std::cout << "\tFormat " << vk::to_string(sf.format) << " color space " << vk::to_string(sf.colorSpace) << std::endl;
		}
		auto caps = dev.getSurfaceCapabilitiesKHR(surface);
		vulkanPrintImageUsageFlags(caps.supportedUsageFlags, "\t");
		physDevices.m_surfacePresentModes.push_back(dev.getSurfacePresentModesKHR(surface));
		std::cout << "\tPresent modes:" << std::endl;
		for (const auto &pm : physDevices.m_surfacePresentModes.back()) {
			std::cout << "\t\t" << vk::to_string(pm) << std::endl;
		}
	}
}

void vulkanPrintImageUsageFlags(const vk::ImageUsageFlags &flags, const std::string &prefix)
{
	if (flags & vk::ImageUsageFlagBits::eTransferSrc) {
		std::cout << prefix << "Image usage transfer src is supported" << std::endl;
	}

	if (flags & vk::ImageUsageFlagBits::eTransferDst) {
		std::cout << prefix << "Image usage transfer dest is supported" << std::endl;
	}

	if (flags & vk::ImageUsageFlagBits::eSampled) {
		std::cout << prefix << "Image usage sampled is supported" << std::endl;
	}

	if (flags & vk::ImageUsageFlagBits::eColorAttachment) {
		std::cout << prefix << "Image usage color attachment is supported" << std::endl;
	}

	if (flags & vk::ImageUsageFlagBits::eDepthStencilAttachment) {
		std::cout << prefix << "Image usage depth stencil attachment is supported" << std::endl;
	}

	if (flags & vk::ImageUsageFlagBits::eTransientAttachment) {
		std::cout << prefix << "Image usage transient attachment is supported" << std::endl;
	}

	if (flags & vk::ImageUsageFlagBits::eInputAttachment) {
		std::cout << prefix << "Image usage input attachment is supported" << std::endl;
	}	
}

vk::ShaderModule vulkanCreateShaderModule(vk::Device &device, const std::filesystem::path &pFileName)
{
	std::ifstream is(pFileName, std::ifstream::binary | std::ifstream::ate);
	if (!is) {
		std::stringstream str;
		str << "File " << pFileName << " not found";
		throw exception::VulkanException(str.str());
	}
	int length = is.tellg();
	is.seekg(0, is.beg);
	auto buf = std::make_unique<char[]>(length);
	is.read(buf.get(), length);
	is.close();

	vk::ShaderModuleCreateInfo shaderCreateInfo {
		.codeSize = static_cast<size_t>(length),
		.pCode = reinterpret_cast<const uint32_t *>(buf.get())
	};

	auto res = device.createShaderModule(shaderCreateInfo);
	std::cout << "Created shader " << pFileName << std::endl;
	return res;
}

}
}
}

