#define STB_IMAGE_IMPLEMENTATION
#include <image_loader.h>
#include <vulkan_exception.h>
#include <sstream>
#include <iostream>
#include <fstream>

namespace macsnet {
namespace vulkan {
namespace image {

ImageLoader::ImageLoader()
{
}

ImageLoader::~ImageLoader()
{
	if (data) {
		stbi_image_free(data);
	}
}

void ImageLoader::load(const std::filesystem::path &path)
{
	std::ifstream fs;
	fs.open(path, std::ios::binary | std::ios::ate);
	if (fs) {
		std::vector<char> d(fs.tellg());
		fs.seekg(0);
		fs.read(d.data(), d.size());
		data = stbi_load_from_memory(reinterpret_cast<unsigned char *>(d.data()), d.size(), &width, &height, &channels, STBI_rgb_alpha);
	}
	if (!data) {
		std::stringstream ss;
		ss << "Can't load texture: " << path;
		throw exception::VulkanException(ss.str());
	} else {
		std::cout << "Texture: " << path << " loaded." << std::endl;
	}
}

void *ImageLoader::getData() const
{
	return data;
}

int ImageLoader::getWidth() const
{
	return width;
}

int ImageLoader::getHeight() const
{
	return height;
}

size_t ImageLoader::getSize() const
{
	return width * height * 4;
}

}
}
}

