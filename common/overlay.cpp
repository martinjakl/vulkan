#include <overlay.h>
#include <fstream>
#include <unicode/unistr.h>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H

namespace macsnet {
namespace vulkan {
namespace overlay {

struct Vertex {
	glm::vec2 pos;
	glm::vec2 tex;

	static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
		std::vector<vk::VertexInputBindingDescription> bd {{
			.binding = 0,
			.stride = sizeof(Vertex),
			.inputRate = vk::VertexInputRate::eVertex,
		}};
		return bd;
	};

	static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
		std::vector<vk::VertexInputAttributeDescription> ad {{
			.location = 0,
			.binding = 0,
			.format = vk::Format::eR32G32Sfloat,
			.offset = offsetof(Vertex, pos),
		}, {
			.location = 1,
			.binding = 0,
			.format = vk::Format::eR32G32Sfloat,
			.offset = offsetof(Vertex, tex),
		}};
		return ad;
	};
};

struct Uniform {
	glm::vec2 resolution;
	glm::vec2 size;

	static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBindings() {
		return {{{
			.binding = 0,
			.descriptorType = vk::DescriptorType::eUniformBuffer,
			.descriptorCount = 1,
			.stageFlags = vk::ShaderStageFlagBits::eVertex,
		}}};
	}
};

static FT_Library ftlib;
static FT_Face face;
static std::vector<char> fontData;

Overlay::Overlay(core::VulkanCore *core) : m_core(core), m_pipe(m_core), m_uni(m_core)
{
}

Overlay::~Overlay()
{
}

void Overlay::createPipe()
{
	m_pipe.init({
		.vs = m_shaders[0],
		.fs = m_shaders[1],
		.getVertexBindingDescription = Vertex::getBindingDescription,
		.getVertexAttributeDescription = Vertex::getAttributeDescription,
		.extent = m_core->getCurrentExtent(),
		.blending = true,
		.depthTest = false,
		.msaaFlags = m_core->getMSAAFlags(),
		.uniLayout = &m_uni.getLayout(),
		.textureLayout = &m_textures[0].getDSLayout(),
		.renderPass = m_core->getRenderPass(),
	});
}

void Overlay::init(const std::filesystem::path &path, size_t frames, uint32_t width, uint32_t height, vk::Format format)
{
	m_width = width;
	m_height = height;

	m_shaders = m_core->readShaders({path / "build" / "shaders"/ "overlay.vert.spv", path / "build" / "shaders" / "overlay.frag.spv"});

	FT_Init_FreeType(&ftlib);
	std::ifstream ifs;
	ifs.open(path / "fonts" / "Nunito-SemiBold.ttf", std::ios::binary | std::ios::ate);
	fontData.resize(ifs.tellg());
	ifs.seekg(0);
	ifs.read(fontData.data(), fontData.size());
	FT_New_Memory_Face(ftlib, reinterpret_cast<FT_Byte *>(fontData.data()), fontData.size(), 0, &face);
	FT_Set_Char_Size(face, 0, 36 * 64, 0, 0);

	const std::vector<Vertex> verticies {
		{{0.0f, 0.0f}, {0.0f, 0.0f}},
		{{1.0f, 0.0f}, {1.0f, 0.0f}},
		{{1.0f, 1.0f}, {1.0f, 1.0f}},
		{{0.0f, 1.0f}, {0.0f, 1.0f}},
	};
	const std::vector<uint16_t> inidcies {0, 2, 1, 2, 0, 3};

	m_vertBuffer = m_core->newBuffers();
	m_vertBuffer->create({
		{verticies.data(), verticies.size() * sizeof(verticies[0]), vk::BufferUsageFlagBits::eVertexBuffer},
		{inidcies.data(), inidcies.size() * sizeof(inidcies[0]), vk::BufferUsageFlagBits::eIndexBuffer},
	}, true);
	m_uni.init(Uniform::getLayoutBindings, std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(Uniform)}}), frames);
	m_textures.resize(frames, core::Textures(m_core));
	for (auto &it : m_textures) {
		it.create({{nullptr, m_width * m_height * 4, m_width, m_height, false, false}}, format, true);
		it.bindBuffer(0);
	}
	createPipe();
	m_updateTexture.resize(frames, false);
}

void Overlay::reinit()
{
	m_pipe.destroyPipe();
	createPipe();
}

void Overlay::addCommandsBegin(size_t ind, vk::CommandBuffer &cmd)
{
	cmd.pipelineBarrier(vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTransfer, static_cast<vk::DependencyFlags>(0), {}, {}, {{
		.dstAccessMask = vk::AccessFlagBits::eTransferWrite,
		.oldLayout = vk::ImageLayout::eUndefined,
		.newLayout = vk::ImageLayout::eTransferDstOptimal,
		.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.image = m_textures[ind].getImages()[0],
		.subresourceRange = {.aspectMask = vk::ImageAspectFlagBits::eColor, .baseMipLevel = 0, .levelCount = 1, .baseArrayLayer = 0, .layerCount = 1},
	}});
	cmd.copyBufferToImage(m_textures[ind].getLocalBuffer(0), m_textures[ind].getImages()[0], vk::ImageLayout::eTransferDstOptimal, {{
		.imageSubresource {.aspectMask = vk::ImageAspectFlagBits::eColor, .layerCount = 1},
		.imageExtent = {.width = m_width, .height = m_height, .depth = 1}, 
	}});
	cmd.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, static_cast<vk::DependencyFlags>(0), {}, {}, {{
		.srcAccessMask = vk::AccessFlagBits::eTransferWrite,
		.dstAccessMask = vk::AccessFlagBits::eShaderRead,
		.oldLayout = vk::ImageLayout::eTransferDstOptimal,
		.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal,
		.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.image = m_textures[ind].getImages()[0],
		.subresourceRange = {.aspectMask = vk::ImageAspectFlagBits::eColor, .baseMipLevel = 0, .levelCount = 1, .baseArrayLayer = 0, .layerCount = 1},
	}});
}

void Overlay::addCommandsEnd(size_t ind, vk::CommandBuffer &cmd)
{
	cmd.bindPipeline(vk::PipelineBindPoint::eGraphics, m_pipe.getPipeline());
	cmd.bindVertexBuffers(0, {m_vertBuffer->getBuffer(0)}, {0});
	cmd.bindIndexBuffer(m_vertBuffer->getBuffer(1), 0, vk::IndexType::eUint16);
	auto ds = m_uni.getDescSets(ind);
	ds.push_back(m_textures[ind].getDecriptorSet());
	cmd.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_pipe.getPipelineLayout(), 0, ds, {}); 
	cmd.drawIndexed(6, 1, 0, 0, 0);
}

void Overlay::update(size_t ind, const std::string &text)
{
	static const unsigned char alfaVal = 178;
	static const std::array<unsigned char, 3> textColor {255, 255, 255};
	if (text != m_lastText) {
		m_lastText = text;
		size_t width = 0;
		size_t height = 0;
		icu::UnicodeString textu32 = icu::UnicodeString::fromUTF8(text);
		for (int32_t i = 0; i < textu32.length(); ++i) {
			FT_Load_Char(face, textu32[i], FT_LOAD_NO_BITMAP);
			width += face->glyph->advance.x >> 6;
			height = std::max(height, static_cast<size_t>(face->glyph->bitmap_top));
		}
		m_data = std::make_unique<unsigned char[]>(m_width * m_height * 4);
		for (size_t i = 0; i < m_height; ++i) {
			for (size_t j = 0; j < m_width ; ++j) {
				auto pos = (i * m_width + j) * 4;
				m_data[pos + 0] = 0;
				m_data[pos + 1] = 0;
				m_data[pos + 2] = 0;
				m_data[pos + 3] = alfaVal;
			}
		}
		size_t x = m_width / 2 - width / 2;
		size_t y = m_height / 2 + height / 2;
		for (int32_t ch = 0; ch < textu32.length(); ++ch) {
			FT_Load_Char(face, textu32[ch], FT_LOAD_RENDER);

			for (size_t i = 0; i < face->glyph->bitmap.rows; ++i) {
				for (size_t j = 0; j < face->glyph->bitmap.width; ++j) {
					float alfa = static_cast<float>(face->glyph->bitmap.buffer[i * face->glyph->bitmap.width + j]) / 255.0f;
					auto pos = ((y - face->glyph->bitmap_top + i) * m_width + j + x) * 4;
					for (size_t di = 0; di < 3; ++di) {
						m_data[pos + di] = static_cast<unsigned char>(alfa * textColor[di] + (1 - alfa) * m_data[pos + di]);
					}
					m_data[pos + 3] = alfa ? 255 : alfaVal;
				}
			}
			x += face->glyph->advance.x >> 6;
		}
		for (size_t i = 0; i < m_updateTexture.size(); ++i) {
			m_updateTexture[i] = true;
		}
	}
	auto [w, h] = m_core->getCurrentExtent();
	Uniform uni {
		.resolution = {w, h},
		.size = {m_width, m_height},
	};
	m_uni.updateUniform(&uni, sizeof(Uniform), ind, 0);
	if (m_updateTexture[ind]) {
		m_updateTexture[ind] = false;
		m_textures[ind].updateBuffer(0, m_data.get(), m_width * m_height * 4);
	}
}

void Overlay::destroy()
{
	m_vertBuffer->destroy();
	m_uni.destroy();
	for (auto &it : m_textures) {
		it.destroy();
	}
	m_pipe.destroy(true);
	m_core->destroyShaders(m_shaders);
	FT_Done_Face(face);
	FT_Done_FreeType(ftlib);
}

}
}
}

