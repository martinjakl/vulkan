#include <glfw_control.h>
#include <iostream>
#include <vulkan_exception.h>

namespace macsnet {
namespace vulkan {
namespace glfw {

GLFWControl::GLFWControl(const std::string &appName) : appName(appName)
{
}

GLFWControl::~GLFWControl()
{
}

void GLFWControl::init(unsigned int width, unsigned int height, bool full, bool resize)
{
	glfwInit();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	if (!resize) {
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	}
	window = glfwCreateWindow(width, height, appName.c_str(), nullptr, nullptr);
	glfwSetWindowUserPointer(window, this);
	glfwSetFramebufferSizeCallback(window, resizeCallback);
	m_width = width;
	m_height = height;
	glfwSetKeyCallback(window, keyCallback);
	if (full) {
		modeFullscreen();
	}
	m_resizable = resize;
}

vk::SurfaceKHR GLFWControl::createSurface(vk::Instance &inst)
{
	VkSurfaceKHR surface;
	glfwCreateWindowSurface(inst, window, 0, &surface);
	return surface;
}

void GLFWControl::cleanup()
{
	glfwDestroyWindow(window);
	glfwTerminate();
}

bool GLFWControl::shouldClose()
{
	return glfwWindowShouldClose(window);
}

void GLFWControl::pollEvents()
{
	glfwPollEvents();
}

std::vector<const char *> GLFWControl::getExtensions()
{
	uint32_t glfwExtensionCount = 0;

	const char **glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
	return std::vector<const char *>(glfwExtensions, glfwExtensions + glfwExtensionCount);
}

std::pair<uint32_t, uint32_t> GLFWControl::getSize()
{
	int w = 0, h = 0;
	glfwGetFramebufferSize(window, &w, &h);
	return std::make_pair(w, h);
}

bool GLFWControl::wasResized()
{
	bool r = resized;
	resized = false;
	return r;
}

void GLFWControl::resizeCallback(GLFWwindow *window, int w, int h)
{
	auto control = reinterpret_cast<GLFWControl *>(glfwGetWindowUserPointer(window));
	control->resized = true;
}

void GLFWControl::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_F1 && action == GLFW_RELEASE) {
		auto control = reinterpret_cast<GLFWControl *>(glfwGetWindowUserPointer(window));
		control->modeWindow();
	}
	if (key == GLFW_KEY_F2 && action == GLFW_RELEASE) {
		auto control = reinterpret_cast<GLFWControl *>(glfwGetWindowUserPointer(window));
		control->modeBorderless();
	}
	if (key == GLFW_KEY_F3 && action == GLFW_RELEASE) {
		auto control = reinterpret_cast<GLFWControl *>(glfwGetWindowUserPointer(window));
		control->modeFullscreen();
	}
	if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
	if (key == GLFW_KEY_W && action == GLFW_RELEASE && mods == GLFW_MOD_CONTROL) {
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}

GLFWmonitor *GLFWControl::getCurrentMonitor(GLFWwindow *window)
{
	int nMonitors, i;
	int wx, wy, ww, wh;
	int mx, my, mw, mh;
	int overlap, bestOverlap;
	GLFWmonitor *bestMonitor;
	GLFWmonitor **monitors;
	const GLFWvidmode *mode;

	bestOverlap = 0;
	bestMonitor = nullptr;

	glfwGetWindowPos(window, &wx, &wy);
	glfwGetWindowSize(window, &ww, &wh);
	monitors = glfwGetMonitors(&nMonitors);

	for (i = 0; i < nMonitors; i++) {
		mode = glfwGetVideoMode(monitors[i]);
		glfwGetMonitorPos(monitors[i], &mx, &my);
		mw = mode->width;
		mh = mode->height;

		overlap = std::max(0, std::min(wx + ww, mx + mw) - std::max(wx, mx)) * std::max(0, std::min(wy + wh, my + mh) - std::max(wy, my));

		if (bestOverlap < overlap) {
			bestOverlap = overlap;
			bestMonitor = monitors[i];
		}
	}

	return bestMonitor;
}

void GLFWControl::modeWindow()
{
	if (m_resizable && m_mode != WINDOW) {
		if (m_mode == FULLSCREEN) {
			glfwSetWindowMonitor(window, nullptr, m_lastx, m_lasty, m_width, m_height, 0);
		}
		glfwSetWindowAttrib(window, GLFW_DECORATED, GLFW_TRUE);
		glfwSetWindowAttrib(window, GLFW_RESIZABLE, GLFW_TRUE);
		glfwSetWindowSize(window, m_width, m_height);
		glfwSetWindowPos(window, m_lastx, m_lasty);
		m_mode = WINDOW;
	}
}

void GLFWControl::modeFullscreen()
{
	if (m_resizable && m_mode != FULLSCREEN) {
		const auto monitor = getCurrentMonitor(window);
		const GLFWvidmode *mode = glfwGetVideoMode(monitor);
		if (m_mode == WINDOW) {
			glfwGetWindowPos(window, &m_lastx, &m_lasty);
		}
		glfwSetWindowMonitor(window, monitor, 0, 0, mode->width, mode->height, 0);
		m_mode = FULLSCREEN;
	}
}

void GLFWControl::modeBorderless()
{
	if (m_resizable && m_mode != BORDERLESS) {
		if (m_mode == FULLSCREEN) {
			glfwSetWindowMonitor(window, nullptr, m_lastx, m_lasty, m_width, m_height, 0);
		}
		const auto monitor = getCurrentMonitor(window);
		const GLFWvidmode *mode = glfwGetVideoMode(monitor);
		if (m_mode == WINDOW) {
			glfwGetWindowPos(window, &m_lastx, &m_lasty);
		}
		glfwSetWindowAttrib(window, GLFW_DECORATED, GLFW_FALSE);
		glfwSetWindowAttrib(window, GLFW_RESIZABLE, GLFW_FALSE);
		glfwSetWindowSize(window, mode->width, mode->height);
		int x = 0, y = 0;
		glfwGetMonitorPos(monitor, &x, &y);
		glfwSetWindowPos(window, x, y);
		m_mode = BORDERLESS;
	}
}

}
}
}

