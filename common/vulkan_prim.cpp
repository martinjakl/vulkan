#include <vulkan_prim.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

namespace macsnet {
namespace vulkan {
namespace primitives {


Primitives::Primitives()
{
}

Primitives::~Primitives()
{
}

const std::vector<Vertex> &Primitives::getVertex() const
{
	return v;
}

uint32_t Primitives::getFilled() const
{
	return f;
}

void Primitives::createBigF()
{
	f = POS | COLOR | TEXCOORD | TEXCOORD2;
	std::vector<glm::vec3> bigF = {
		// left column front
		{0.0f,   0.0f,   0.0f},
		{0.0f, 150.0f,   0.0f},
		{30.0f,  0.0f,   0.0f},
		{0.0f, 150.0f,   0.0f},
		{30.0f,150.0f,   0.0f},
		{30.0f,  0.0f,   0.0f},

		// top rung front
		{30.0f,   0.0f,  0.0f},
		{30.0f,  30.0f,  0.0f},
		{100.0f,  0.0f,  0.0f},
		{30.0f,  30.0f,  0.0f},
		{100.0f, 30.0f,  0.0f},
		{100.0f,  0.0f,  0.0f},

		// middle rung front
		{30.0f,  60.0f,  0.0f},
		{30.0f,  90.0f,  0.0f},
		{67.0f,  60.0f,  0.0f},
		{30.0f,  90.0f,  0.0f},
		{67.0f,  90.0f,  0.0f},
		{67.0f,  60.0f,  0.0f},

		// left column back
		{0.0f,   0.0f,  30.0f},
		{30.0f,  0.0f,  30.0f},
		{0.0f, 150.0f,  30.0f},
		{0.0f, 150.0f,  30.0f},
		{30.0f,  0.0f,  30.0f},
		{30.0f, 150.0f, 30.0f},

		// top rung back
		{30.0f,   0.0f, 30.0f},
		{100.0f,  0.0f, 30.0f},
		{30.0f,  30.0f, 30.0f},
		{30.0f,  30.0f, 30.0f},
		{100.0f,  0.0f, 30.0f},
		{100.0f, 30.0f, 30.0f},

		// middle rung back
		{30.0f,  60.0f, 30.0f},
		{67.0f,  60.0f, 30.0f},
		{30.0f,  90.0f, 30.0f},
		{30.0f,  90.0f, 30.0f},
		{67.0f,  60.0f, 30.0f},
		{67.0f,  90.0f, 30.0f},

		// top
		{0.0f,    0.0f,  0.0f},
		{100.0f,  0.0f,  0.0f},
		{100.0f,  0.0f, 30.0f},
		{0.0f,    0.0f,  0.0f},
		{100.0f,  0.0f, 30.0f},
		{0.0f,    0.0f, 30.0f},

		// top rung right
		{100.0f,  0.0f,  0.0f},
		{100.0f, 30.0f,  0.0f},
		{100.0f, 30.0f, 30.0f},
		{100.0f,  0.0f,  0.0f},
		{100.0f, 30.0f, 30.0f},
		{100.0f,  0.0f, 30.0f},

		// under top rung
		{30.0f,  30.0f,  0.0f},
		{30.0f,  30.0f, 30.0f},
		{100.0f, 30.0f, 30.0f},
		{30.0f,  30.0f,  0.0f},
		{100.0f, 30.0f, 30.0f},
		{100.0f, 30.0f,  0.0f},

		// between top rung and middle
		{30.0f,  30.0f,  0.0f},
		{30.0f,  60.0f, 30.0f},
		{30.0f,  30.0f, 30.0f},
		{30.0f,  30.0f,  0.0f},
		{30.0f,  60.0f,  0.0f},
		{30.0f,  60.0f, 30.0f},

		// top of middle rung
		{30.0f,  60.0f,  0.0f},
		{67.0f,  60.0f, 30.0f},
		{30.0f,  60.0f, 30.0f},
		{30.0f,  60.0f,  0.0f},
		{67.0f,  60.0f,  0.0f},
		{67.0f,  60.0f, 30.0f},

		// right of middle rung
		{67.0f,  60.0f,  0.0f},
		{67.0f,  90.0f, 30.0f},
		{67.0f,  60.0f, 30.0f},
		{67.0f,  60.0f,  0.0f},
		{67.0f,  90.0f,  0.0f},
		{67.0f,  90.0f, 30.0f},

		// bottom of middle rung.
		{30.0f,  90.0f,  0.0f},
		{30.0f,  90.0f, 30.0f},
		{67.0f,  90.0f, 30.0f},
		{30.0f,  90.0f,  0.0f},
		{67.0f,  90.0f, 30.0f},
		{67.0f,  90.0f,  0.0f},

		// right of bottom
		{30.0f,  90.0f,  0.0f},
		{30.0f, 150.0f, 30.0f},
		{30.0f,  90.0f, 30.0f},
		{30.0f,  90.0f,  0.0f},
		{30.0f, 150.0f,  0.0f},
		{30.0f, 150.0f, 30.0f},

		// bottom
		{0.0f,  150.0f,  0.0f},
		{0.0f,  150.0f, 30.0f},
		{30.0f, 150.0f, 30.0f},
		{0.0f,  150.0f,  0.0f},
		{30.0f, 150.0f, 30.0f},
		{30.0f, 150.0f,  0.0f},

		// left side
		{0.0f,    0.0f,  0.0f},
		{0.0f,    0.0f, 30.0f},
		{0.0f,  150.0f, 30.0f},
		{0.0f,    0.0f,  0.0f},
		{0.0f,  150.0f, 30.0f},
		{0.0f,  150.0f,  0.0f},
	};
	auto matrix = glm::rotate(glm::pi<float>(), glm::vec3 {1.0f, 0.0f, 0.0f});
	for (auto &vert : bigF) {
		vert.y *= -1;
		v.push_back({.pos = matrix * glm::vec4 {vert, 1.0f}});
	}

	std::vector<glm::u8vec3> col(v.size());
	for (size_t i = 0; i < 16 * 6; ++i) {
		switch (static_cast<size_t>(floor(static_cast<double>(i) / 6))) {
			case 0:
			case 1:
			case 2:
				col[i] = {200, 70, 120};
				break;
			case 3:
			case 4:
			case 5:
				col[i] = {80, 70, 200};
				break;
			case 6:
				col[i] = {70, 200, 210};
				break;
			case 7:
				col[i] = {200, 200, 70};
				break;
			case 8:
				col[i] = {210, 100, 70};
				break;
			case 9:
				col[i] = {210, 160, 70};
				break;
			case 10:
				col[i] = {70, 180, 210};
				break;
			case 11:
				col[i] = {100, 70, 210};
				break;
			case 12:
				col[i] = {76, 210, 100};
				break;
			case 13:
				col[i] = {140, 210, 80};
				break;
			case 14:
				col[i] = {90, 130, 110};
				break;
			case 15:
				col[i] = {160, 160, 220};
				break;
		}
	}
	for (size_t i = 0; i < col.size(); ++i) {
		v[i].color = {static_cast<float>(col[i].r) / 255.0f, static_cast<float>(col[i].g) / 255.0f, static_cast<float>(col[i].b) / 255.0f, 1.0f};
	}
	std::vector<glm::vec2> tcoord;
	tcoord.reserve(v.size());
	for (size_t i = 0; i < 16; ++i) {
		switch (i) {
			case 0:
				tcoord.push_back({38.0f / 255.0f, 44.0f / 255.0f});
				tcoord.push_back({38.0f / 255.0f, 223.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 44.0f / 255.0f});
				tcoord.push_back({38.0f / 255.0f, 223.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 223.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 44.0f / 255.0f});
				break;
			case 1:
				tcoord.push_back({113.0f / 255.0f, 44.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 85.0f / 255.0f});
				tcoord.push_back({218.0f / 255.0f, 44.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 85.0f / 255.0f});
				tcoord.push_back({218.0f / 255.0f, 85.0f / 255.0f});
				tcoord.push_back({218.0f / 255.0f, 44.0f / 255.0f});
				break;
			case 2:
				tcoord.push_back({113.0f / 255.0f, 112.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 151.0f / 255.0f});
				tcoord.push_back({203.0f / 255.0f, 112.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 151.0f / 255.0f});
				tcoord.push_back({203.0f / 255.0f, 151.0f / 255.0f});
				tcoord.push_back({203.0f / 255.0f, 112.0f / 255.0f});
				break;
			case 3:
				tcoord.push_back({38.0f / 255.0f, 44.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 44.0f / 255.0f});
				tcoord.push_back({38.0f / 255.0f, 223.0f / 255.0f});
				tcoord.push_back({38.0f / 255.0f, 223.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 44.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 223.0f / 255.0f});
				break;
			case 4:
				tcoord.push_back({113.0f / 255.0f, 44.0f / 255.0f});
				tcoord.push_back({218.0f / 255.0f, 44.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 85.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 85.0f / 255.0f});
				tcoord.push_back({218.0f / 255.0f, 44.0f / 255.0f});
				tcoord.push_back({218.0f / 255.0f, 85.0f / 255.0f});
				break;
			case 5:
				tcoord.push_back({113.0f / 255.0f, 112.0f / 255.0f});
				tcoord.push_back({203.0f / 255.0f, 112.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 151.0f / 255.0f});
				tcoord.push_back({113.0f / 255.0f, 151.0f / 255.0f});
				tcoord.push_back({203.0f / 255.0f, 112.0f / 255.0f});
				tcoord.push_back({203.0f / 255.0f, 151.0f / 255.0f});
				break;
			case 6:
			case 7:
			case 8:
				tcoord.push_back({0.0f, 0.0f});
				tcoord.push_back({1.0f, 0.0f});
				tcoord.push_back({1.0f, 1.0f});
				tcoord.push_back({0.0f, 0.0f});
				tcoord.push_back({1.0f, 1.0f});
				tcoord.push_back({0.0f, 1.0f});
				break;
			case 9:
			case 10:
			case 11:
			case 13:
				tcoord.push_back({0.0f, 0.0f});
				tcoord.push_back({1.0f, 1.0f});
				tcoord.push_back({0.0f, 1.0f});
				tcoord.push_back({0.0f, 0.0f});
				tcoord.push_back({1.0f, 0.0f});
				tcoord.push_back({1.0f, 1.0f});
				break;
			case 12:
			case 14:
			case 15:
				tcoord.push_back({0.0f, 0.0f});
				tcoord.push_back({0.0f, 1.0f});
				tcoord.push_back({1.0f, 1.0f});
				tcoord.push_back({0.0f, 0.0f});
				tcoord.push_back({1.0f, 1.0f});
				tcoord.push_back({1.0f, 0.0f});
				break;
		}
	}
	for (size_t i = 0; i < tcoord.size(); ++i) {
		v[i].texcoord = tcoord[i];
	}

	std::vector<glm::vec2> tcoord2;
	tcoord2.reserve(v.size());
	for (size_t i = 0; i < 16; ++i) {
		switch (i) {
			case 0:
			case 1:
			case 2:
				tcoord2.push_back({0.0f, 0.0f});
				tcoord2.push_back({0.0f, 1.0f});
				tcoord2.push_back({1.0f, 0.0f});
				tcoord2.push_back({0.0f, 1.0f});
				tcoord2.push_back({1.0f, 1.0f});
				tcoord2.push_back({1.0f, 0.0f});
				break;
			case 3:
			case 4:
			case 5:
				tcoord2.push_back({0.0f, 0.0f});
				tcoord2.push_back({1.0f, 0.0f});
				tcoord2.push_back({0.0f, 1.0f});
				tcoord2.push_back({0.0f, 1.0f});
				tcoord2.push_back({1.0f, 0.0f});
				tcoord2.push_back({1.0f, 1.0f});
				break;
			case 6:
			case 7:
			case 8:
				tcoord2.push_back({0.0f, 0.0f});
				tcoord2.push_back({1.0f, 0.0f});
				tcoord2.push_back({1.0f, 1.0f});
				tcoord2.push_back({0.0f, 0.0f});
				tcoord2.push_back({1.0f, 1.0f});
				tcoord2.push_back({0.0f, 1.0f});
				break;
			case 9:
			case 10:
			case 11:
			case 13:
				tcoord2.push_back({0.0f, 0.0f});
				tcoord2.push_back({1.0f, 1.0f});
				tcoord2.push_back({0.0f, 1.0f});
				tcoord2.push_back({0.0f, 0.0f});
				tcoord2.push_back({1.0f, 0.0f});
				tcoord2.push_back({1.0f, 1.0f});
				break;
			case 12:
			case 14:
			case 15:
				tcoord2.push_back({0.0f, 0.0f});
				tcoord2.push_back({0.0f, 1.0f});
				tcoord2.push_back({1.0f, 1.0f});
				tcoord2.push_back({0.0f, 0.0f});
				tcoord2.push_back({1.0f, 1.0f});
				tcoord2.push_back({1.0f, 0.0f});
				break;
		}
	}
	for (size_t i = 0; i < tcoord2.size(); ++i) {
		v[i].texcoord2 = tcoord2[i];
	}

	std::vector<glm::vec3> normal(16 * 6);
	for (size_t i = 0; i < 16; ++i) {
		switch (i) {
			case 0:
			case 1:
			case 2:
				for (size_t j = 0; j < 6; ++j) {
					normal[i * 6 + j] = {0.0f, 0.0f, 1.0f};
				}
				break;
			case 3:
			case 4:
			case 5:
				for (size_t j = 0; j < 6; ++j) {
					normal[i * 6 + j] = {0.0f, 0.0f, -1.0f};
				}
				break;
			case 6:
			case 10:
				for (size_t j = 0; j < 6; ++j) {
					normal[i * 6 + j] = {0.0f, 1.0f, 0.0f};
				}
				break;
			case 7:
			case 9:
			case 11:
			case 13:
				for (size_t j = 0; j < 6; ++j) {
					normal[i * 6 + j] = {1.0f, 0.0f, 0.0f};
				}
				break;
			case 8:
			case 12:
			case 14:
				for (size_t j = 0; j < 6; ++j) {
					normal[i * 6 + j] = {0.0f, -1.0f, 0.0f};
				}
				break;
			case 15:
				for (size_t j = 0; j < 6; ++j) {
					normal[i * 6 + j] = {-1.0f, 0.0f, 0.0f};
				}
				break;
		}
	}
	for (size_t i = 0; i < normal.size(); ++i) {
		v[i].normal = normal[i];
	}
}

void Primitives::createQuad(float size)
{
	f = POS | TEXCOORD | TEXCOORD2;
	float p = size / 2.0f;
	auto pos = std::vector<glm::vec3> {
		{-p, -p, 0.0f},
		{-p, p, 0.0f},
		{p, -p, 0.0f},
		{-p, p, 0.0f},
		{p, p, 0.0f},
		{p, -p, 0.0f},
	};
	auto tex = std::vector<glm::vec2> {
		{0.0f, 0.0f},
		{0.0f, 1.0f},
		{1.0f, 0.0f},
		{0.0f, 1.0f},
		{1.0f, 1.0f},
		{1.0f, 0.0f},
	};
	auto tex2 = std::vector<glm::vec2> {
		{-3.0f, -1.0f},
		{-3.0f, 4.0f},
		{2.0f, -1.0f},
		{-3.0f, 4.0f},
		{2.0f, 4.0f},
		{2.0f, -1.0f},
	};
	v.resize(pos.size());
	for (size_t i = 0; i < pos.size(); ++i) {
		v[i].pos = glm::vec4(pos[i], 1.0f);
		v[i].texcoord = tex[i];
		v[i].texcoord2 = tex2[i];
	}
}

void Primitives::center()
{
	float minX = 0.0f;
	float minY = 0.0f;
	float minZ = 0.0f;
	float maxX = 0.0f;
	float maxY = 0.0f;
	float maxZ = 0.0f;
	for (const auto &i : v) {
		minX = std::min(minX, i.pos[0]);
		minY = std::min(minY, i.pos[1]);
		minZ = std::min(minZ, i.pos[2]);
		maxX = std::max(maxX, i.pos[0]);
		maxY = std::max(maxY, i.pos[1]);
		maxZ = std::max(maxZ, i.pos[2]);
	}
	auto tr = glm::translate(glm::identity<glm::mat4>(), {-((maxX - minX) / 2.0f + minX), -((maxY - minY) / 2.0f + minY), -((maxZ - minZ) / 2.0f + minZ)});
	for (auto &i : v) {
		i.pos = tr * i.pos;
	}
}

}
}
}

