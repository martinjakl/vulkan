#include <iostream>
#include <cmath>
#include <vulkan_core.h>
#include <vulkan_exception.h>
#include <image_loader.h>

namespace macsnet {
namespace vulkan {
namespace core {

VKAPI_ATTR VkBool32 VKAPI_CALL myDebugReportCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objectType, uint64_t object, size_t location, int32_t messageCode, const char *pLayerPrefix, const char *pMessage, void *pUserData)
{
	std::cout << pMessage << std::endl;
	return VK_FALSE;
}

Pipeline::Pipeline(VulkanCore *core) : m_core(core)
{
}

Pipeline::~Pipeline()
{
}

void Pipeline::init(const PipeInit &init)
{
	std::vector<vk::PipelineShaderStageCreateInfo> shaderStageCreateInfo {{
		.stage = vk::ShaderStageFlagBits::eVertex,
		.module = init.vs,
		.pName = "main"
	}, {
		.stage = vk::ShaderStageFlagBits::eFragment,
		.module = init.fs,
		.pName = "main"
	}};

	std::vector<vk::VertexInputBindingDescription> bd;
	std::vector<vk::VertexInputAttributeDescription> ad;

	if (init.getVertexBindingDescription) {
		bd = init.getVertexBindingDescription();
	}

	if (init.getVertexAttributeDescription) {
		ad = init.getVertexAttributeDescription();
	}

	vk::PipelineVertexInputStateCreateInfo vertexInputInfo {
		.vertexBindingDescriptionCount = static_cast<uint32_t>(bd.size()),
		.pVertexBindingDescriptions = bd.data(),
		.vertexAttributeDescriptionCount = static_cast<uint32_t>(ad.size()),
		.pVertexAttributeDescriptions = ad.data(),
	};

	vk::PipelineInputAssemblyStateCreateInfo pipelineIACreateInfo {
		.topology = vk::PrimitiveTopology::eTriangleList,
	};

	vk::Viewport vp {
		.x = 0.0f,
		.y = 0.0f,
		.width = static_cast<float>(init.extent.width),
		.height = static_cast<float>(init.extent.height),
		.minDepth = 0.0f,
		.maxDepth = 1.0f,
	};

	vk::Rect2D scissor {
		.offset {0, 0},
		.extent = init.extent,
	};

	vk::PipelineViewportStateCreateInfo vpCreateInfo {
		.viewportCount = 1,
		.pViewports = &vp,
		.scissorCount = 1,
		.pScissors = &scissor,
	};

	vk::PipelineRasterizationStateCreateInfo rastCreateInfo {
		.polygonMode = vk::PolygonMode::eFill,
		.cullMode = init.noCull ? vk::CullModeFlagBits::eNone : vk::CullModeFlagBits::eBack,
		.frontFace = init.clockwise ? vk::FrontFace::eClockwise : vk::FrontFace::eCounterClockwise,
		.lineWidth = 1.0f,
	};

	vk::PipelineMultisampleStateCreateInfo pipelineMSCreateInfo {
		.rasterizationSamples = init.msaaFlags,
	};
	if (init.sampleShading) {
		pipelineMSCreateInfo.sampleShadingEnable = VK_TRUE;
		pipelineMSCreateInfo.minSampleShading = 0.5f;
	}

	vk::PipelineColorBlendAttachmentState blendAttachStateNoBlend {
		.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA,
	}; 

	vk::PipelineColorBlendAttachmentState blendAttachStateBlend {
		.blendEnable = VK_TRUE,
		.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha,
		.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha,
		.colorBlendOp = vk::BlendOp::eAdd,
		.srcAlphaBlendFactor = vk::BlendFactor::eOne,
		.dstAlphaBlendFactor = vk::BlendFactor::eZero,
		.alphaBlendOp = vk::BlendOp::eAdd,
		.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA,
	}; 

	vk::PipelineColorBlendStateCreateInfo blendCreateInfo {
		.logicOp = vk::LogicOp::eCopy,
		.attachmentCount = 1,
		.pAttachments = init.blending ? &blendAttachStateBlend : &blendAttachStateNoBlend,
	};

	vk::PipelineDepthStencilStateCreateInfo depthCreateInfoT {
		.depthTestEnable = VK_TRUE,
		.depthWriteEnable = VK_TRUE,
		.depthCompareOp = vk::CompareOp::eLess,
		.depthBoundsTestEnable = VK_FALSE,
		.stencilTestEnable = VK_FALSE,
		.front {},
		.back {},
		.minDepthBounds = 0.0f,
		.maxDepthBounds = 1.0f,
	};

	vk::PipelineDepthStencilStateCreateInfo depthCreateInfoF {
		.depthTestEnable = VK_FALSE,
		.depthWriteEnable = VK_TRUE,
		.depthCompareOp = vk::CompareOp::eLess,
		.depthBoundsTestEnable = VK_FALSE,
		.stencilTestEnable = VK_FALSE,
		.front {},
		.back {},
		.minDepthBounds = 0.0f,
		.maxDepthBounds = 1.0f,
	};

	if (!m_pipelineLayout) {
		std::vector<vk::DescriptorSetLayout> dsl;
		if (init.uniLayout) {
			dsl.insert(dsl.end(), init.uniLayout->begin(), init.uniLayout->end());
		}
		if (init.textureLayout) {
			dsl.push_back(*init.textureLayout);
		}

		vk::PipelineLayoutCreateInfo layoutInfo {
			.setLayoutCount = static_cast<uint32_t>(dsl.size()),
			.pSetLayouts = dsl.data(),
		};
		m_pipelineLayout = m_core->getDevice().createPipelineLayout(layoutInfo);
	}

	if (!m_pipelineCache) {
		m_pipelineCache = m_core->getDevice().createPipelineCache(vk::PipelineCacheCreateInfo());
	}

	m_pipeline = m_core->getDevice().createGraphicsPipeline(m_pipelineCache, {
		.stageCount = static_cast<uint32_t>(shaderStageCreateInfo.size()),
		.pStages = shaderStageCreateInfo.data(),
		.pVertexInputState = &vertexInputInfo,
		.pInputAssemblyState = &pipelineIACreateInfo,
		.pViewportState = &vpCreateInfo,
		.pRasterizationState = &rastCreateInfo,
		.pMultisampleState = &pipelineMSCreateInfo,
		.pDepthStencilState = init.depthTest ? &depthCreateInfoT : &depthCreateInfoF,
		.pColorBlendState = &blendCreateInfo,
		.layout = m_pipelineLayout,
		.renderPass = init.renderPass,
		.basePipelineIndex = -1,
	}).value;
}

void Pipeline::destroy(bool complete)
{
	m_core->getDevice().destroyPipelineLayout(m_pipelineLayout);
	m_core->getDevice().destroyPipelineCache(m_pipelineCache);
	if (complete) {
		destroyPipe();
	}
}

void Pipeline::destroyPipe()
{
	m_core->getDevice().destroyPipeline(m_pipeline);
}

const vk::Pipeline &Pipeline::getPipeline() const
{
	return m_pipeline;
}

const vk::PipelineLayout &Pipeline::getPipelineLayout() const
{
	return m_pipelineLayout;
}

FrameBuffer::FrameBuffer(VulkanCore *core) : m_core(core), m_pipeline(core)
{
}

FrameBuffer::~FrameBuffer()
{
}

void FrameBuffer::init(const FBInit &init)
{
	if (init.images) {
		m_frames.resize(init.images->size());
		for (size_t i = 0; i < init.images->size(); ++i) {
			m_frames[i].image = init.images->at(i);
		}
	} else {
		m_frames.resize(init.numImages);
	}
	m_extent = init.extent;
	if (!m_uniforms && init.uniforms) {
		m_uniforms = std::make_shared<Uniforms>(m_core);
		m_uniforms->init(init.getLayoutBindings, init.uniforms, m_frames.size());
	}
	createDepthResource(init);
	createRenderPass(init);
	createFrameBuffer(init);
	m_pipeline.init({
		.vs = init.vs,
		.fs = init.fs,
		.getVertexBindingDescription = init.getVertexBindingDescription,
		.getVertexAttributeDescription = init.getVertexAttributeDescription,
		.extent = m_extent,
		.noCull = init.noCull,
		.clockwise = init.clockwise,
		.sampleShading = init.sampleShading,
		.blending = init.blending,
		.depthTest = init.depthTest,
		.msaaFlags = init.msaaFlags,
		.uniLayout = m_uniforms ? &m_uniforms->getLayout() : nullptr,
		.textureLayout = init.textureLayout,
		.renderPass = m_renderPass
	});
}

void FrameBuffer::createRenderPass(const FBInit &init)
{
	if (!m_renderPass) {
		m_renderPass = m_core->createRenderPass(true, init.offscreen, init.surfaceFormat, init.depthFormat, init.msaaFlags);
	}
}

void FrameBuffer::createFrameBuffer(const FBInit &init)
{
	if (init.images) {
		for (auto &f : m_frames) {
			f.view = m_core->getDevice().createImageView({
				.image = f.image,
				.viewType = vk::ImageViewType::e2D,
				.format = init.surfaceFormat,
				.components {vk::ComponentSwizzle::eIdentity, vk::ComponentSwizzle::eIdentity, vk::ComponentSwizzle::eIdentity, vk::ComponentSwizzle::eIdentity},
				.subresourceRange {.aspectMask = vk::ImageAspectFlagBits::eColor, .levelCount = 1, .layerCount = 1},
			});
			std::vector<vk::ImageView> attachments;
			if (init.msaaFlags != vk::SampleCountFlagBits::e1) {
				attachments = {
					m_colorImageView,
					m_depthImageView,
					f.view,
				};
			} else {
				attachments = {
					f.view,
					m_depthImageView,
				};
			}
			f.fb = m_core->getDevice().createFramebuffer({
				.renderPass = m_renderPass,
				.attachmentCount = static_cast<uint32_t>(attachments.size()),
				.pAttachments = attachments.data(),
				.width = m_extent.width,
				.height = m_extent.height,
				.layers = 1,
			});
		}
	}
}

void FrameBuffer::createDepthResource(const FBInit &init)
{
	vk::Format depthFormat = init.depthFormat;
	m_depthImage = m_core->getDevice().createImage({
		.imageType = vk::ImageType::e2D,
		.format = depthFormat,
		.extent = {.width = m_extent.width, .height = m_extent.height, .depth = 1},
		.mipLevels = 1,
		.arrayLayers = 1,
		.samples = init.msaaFlags,
		.tiling = vk::ImageTiling::eOptimal,
		.usage = vk::ImageUsageFlagBits::eDepthStencilAttachment,
		.sharingMode = vk::SharingMode::eExclusive,
		.initialLayout = vk::ImageLayout::eUndefined,
	});
	auto dmr = m_core->getDevice().getImageMemoryRequirements(m_depthImage);
	auto bits = dmr.memoryTypeBits;
	vk::MemoryRequirements cmr {};
	if (init.msaaFlags != vk::SampleCountFlagBits::e1) {
		m_colorImage = m_core->getDevice().createImage({
			.imageType = vk::ImageType::e2D,
			.format = init.surfaceFormat,
			.extent = {.width = m_extent.width, .height = m_extent.height, .depth = 1},
			.mipLevels = 1,
			.arrayLayers = 1,
			.samples = init.msaaFlags,
			.tiling = vk::ImageTiling::eOptimal,
			.usage = vk::ImageUsageFlagBits::eTransientAttachment | vk::ImageUsageFlagBits::eColorAttachment,
			.sharingMode = vk::SharingMode::eExclusive,
			.initialLayout = vk::ImageLayout::eUndefined,
		});
		cmr = m_core->getDevice().getImageMemoryRequirements(m_colorImage);
		bits &= cmr.memoryTypeBits;
	}
	m_depthColorImageMemory = m_core->getDevice().allocateMemory({
		.allocationSize = dmr.size + cmr.size,
		.memoryTypeIndex = m_core->findMemoryType(bits, vk::MemoryPropertyFlagBits::eDeviceLocal),
	});
	m_core->getDevice().bindImageMemory(m_depthImage, m_depthColorImageMemory, 0);
	m_depthImageView = m_core->getDevice().createImageView({
		.image = m_depthImage,
		.viewType = vk::ImageViewType::e2D,
		.format = depthFormat,
		.subresourceRange {.aspectMask = vk::ImageAspectFlagBits::eDepth, .levelCount = 1, .layerCount = 1},
	});
	if (init.msaaFlags != vk::SampleCountFlagBits::e1) {
		m_core->getDevice().bindImageMemory(m_colorImage, m_depthColorImageMemory, dmr.size);
		m_colorImageView = m_core->getDevice().createImageView({
			.image = m_colorImage,
			.viewType = vk::ImageViewType::e2D,
			.format = init.surfaceFormat,
			.subresourceRange {.aspectMask = vk::ImageAspectFlagBits::eColor, .levelCount = 1, .layerCount = 1},
		});
	}
}

void FrameBuffer::cleanup(bool complete)
{
	if (complete) {
		cleanupFB();
	}
	m_pipeline.destroy(false);
	if (m_uniforms) {
		m_uniforms->destroy();
	}
	m_core->destroyRenderPass(m_renderPass);
}

void FrameBuffer::cleanupFB()
{
	for (const auto &f : m_frames) {
		m_core->getDevice().destroyFramebuffer(f.fb);
		m_core->getDevice().destroyImageView(f.view);
	}
	m_frames.clear();
	m_pipeline.destroyPipe();
	m_core->getDevice().destroyImageView(m_colorImageView);
	m_core->getDevice().destroyImageView(m_depthImageView);
	m_core->getDevice().freeMemory(m_depthColorImageMemory);
	m_core->getDevice().destroyImage(m_colorImage);
	m_core->getDevice().destroyImage(m_depthImage);
}

Frame &FrameBuffer::getFrame(size_t i)
{
	return m_frames[i];
}

const Frame &FrameBuffer::getFrame(size_t i) const
{
	return m_frames[i];
}

void FrameBuffer::updateUniform(const void *b, size_t s, size_t i, size_t ui)
{
	m_uniforms->updateUniform(b, s, i, ui);
}

size_t FrameBuffer::getFrameCount() const
{
	return m_frames.size();
}

const vk::RenderPass &FrameBuffer::getRenderPass() const
{
	return m_renderPass;
}

const vk::Pipeline &FrameBuffer::getPipeline() const
{
	return m_pipeline.getPipeline();
}

const vk::PipelineLayout &FrameBuffer::getPipelineLayout() const
{
	return m_pipeline.getPipelineLayout();
}

const vk::Extent2D &FrameBuffer::getExtent() const
{
	return m_extent;
}

const std::vector<vk::DescriptorSet> &FrameBuffer::getDescSets(size_t i) const
{
	return m_uniforms->getDescSets(i);
}

VulkanCore::VulkanCore(const std::string &pAppName) : m_appName(pAppName), m_fb(this)
{
}

VulkanCore::~VulkanCore()
{
}

bool VulkanCore::init(base::VulkanWindowControl *pWindowControl, bool debug, const std::filesystem::path &vShader, const std::filesystem::path &fShader, bool clockwise, int maxFramesInFlight)
{
	return init({
		.pWindowControl = pWindowControl,
		.debug = debug,
		.vShader = vShader,
		.fShader = fShader,
		.clockwise = clockwise,
		.maxFramesInFlight = maxFramesInFlight,
	});
}

bool VulkanCore::init(const CoreInit &i)
{
	m_init = i;
	auto extProps = base::vulkanEnumExtProps();
	createInstance();
	m_surface = m_init.pWindowControl->createSurface(m_inst);
	if (!m_surface) {
		std::cout << "createSurface failed" << std::endl;
		return false;
	}
	vulkanGetPhysicalDevices(m_inst, m_surface, m_physDevices);
	selectPhysicalDevice(i.preferedFormat);
	if (m_init.msaaFlags != vk::SampleCountFlagBits::e1) {
		m_msaaFlags = getMaxUsableSampleCount(m_init.msaaFlags);
	}
	createLogicalDevice();
	m_queue = m_device.getQueue(m_gfxQueueFamily, 0);
	createShaders();
	createCmdBufPool();
	createTextures();
	createSwapChain();
	return true;
}

void VulkanCore::selectPhysicalDevice(vk::Format preferedFormat)
{
	for (size_t i = 0; i < m_physDevices.m_devices.size(); ++i) {
		std::cout << "Device: " << i << std::endl;
		for (size_t j = 0; j < m_physDevices.m_qFamilyProps[i].size(); ++j) {
			auto &qFamilyProp = m_physDevices.m_qFamilyProps[i][j];
			std::cout << "\tFamily: " << j << " Num queues: " << qFamilyProp.queueCount << std::endl;
			vk::QueueFlags flags = qFamilyProp.queueFlags;
			auto checkFlags = [flags](auto flag) {return (flags & flag) ? "Yes" : "No";};
			std::cout << "\t\tGFX " << checkFlags(vk::QueueFlagBits::eGraphics) << ", Compute " << checkFlags(vk::QueueFlagBits::eCompute) << ", Transfer " << checkFlags(vk::QueueFlagBits::eTransfer) << ", Sparse binding " << checkFlags(vk::QueueFlagBits::eSparseBinding) << std::endl;
			if (flags & vk::QueueFlagBits::eGraphics) {
				if (!m_physDevices.m_qSupportsPresent[i][j]) {
					std::cout << "Present is not supported" << std::endl;
					continue;
				}
				if (m_gfxDevIndex == -1) {
					m_gfxDevIndex = i;
					m_gfxQueueFamily = j;
				}
			}
		}
	}
	if (m_gfxDevIndex == -1) {
		throw exception::VulkanException("No GFX device found!");
	} else {
		std::cout << "Using GFX device " << m_gfxDevIndex << " and queue family " << m_gfxQueueFamily << std::endl;
	}
	for (size_t i = 0; i < m_physDevices.m_surfaceFormats[m_gfxDevIndex].size(); ++i) {
		if (m_physDevices.m_surfaceFormats[m_gfxDevIndex][i].format == preferedFormat) {
			m_gfxFormat = static_cast<int>(i);
			std::cout << "Format " << vk::to_string(preferedFormat) << " found." << std::endl;
		}
	}
	if (m_gfxFormat == -1) {
		m_gfxFormat = 0;
		std::cout << "Format " << vk::to_string(preferedFormat) << " not found. Using: " << vk::to_string(m_physDevices.m_surfaceFormats[m_gfxDevIndex][0].format) << std::endl;
	}
}

void VulkanCore::createLogicalDevice()
{
	float qPriorities = 1.0f;
	vk::DeviceQueueCreateInfo qInfo {
		.queueFamilyIndex = static_cast<uint32_t>(m_gfxQueueFamily),
		.queueCount = 1,
		.pQueuePriorities = &qPriorities,
	};
	std::vector<const char *> devExt{VK_KHR_SWAPCHAIN_EXTENSION_NAME};
	std::vector<const char *> devLayers;
	if (m_init.debug) {
		devLayers.push_back("VK_LAYER_KHRONOS_validation");
	}
	vk::PhysicalDeviceFeatures features {
		.samplerAnisotropy = VK_TRUE,
	};
	if (m_init.sampleShading) {
		features.sampleRateShading = VK_TRUE;
	}
	m_device = getPhysDevice().createDevice(vk::DeviceCreateInfo{
		.queueCreateInfoCount = 1,
		.pQueueCreateInfos = &qInfo,
		.enabledLayerCount = static_cast<uint32_t>(devLayers.size()),
		.ppEnabledLayerNames = devLayers.data(),
		.enabledExtensionCount = static_cast<uint32_t>(devExt.size()),
		.ppEnabledExtensionNames = devExt.data(),
		.pEnabledFeatures = &features,
	});
	std::cout << "Device created" << std::endl;
}

void VulkanCore::createInstance()
{
	vk::ApplicationInfo appInfo {
		.pApplicationName = m_appName.c_str(),
		.applicationVersion = VK_MAKE_VERSION(1, 0, 0),
		.pEngineName = "MacsEngine",
		.engineVersion = VK_MAKE_VERSION(1, 0, 0),
		.apiVersion = VK_API_VERSION_1_2,
	};
	std::vector<const char *> instExt = m_init.pWindowControl->getExtensions();
	std::vector<const char *> instLayers;
	if (m_init.debug) {
		instExt.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
		instLayers.push_back("VK_LAYER_KHRONOS_validation");
	}
	m_inst = vk::createInstance({
		.pApplicationInfo = &appInfo,
		.enabledLayerCount = static_cast<uint32_t>(instLayers.size()),
		.ppEnabledLayerNames = instLayers.data(),
		.enabledExtensionCount = static_cast<uint32_t>(instExt.size()),
		.ppEnabledExtensionNames = instExt.data(),
	});
	if (m_init.debug) {
		VkDebugReportCallbackCreateInfoEXT callbackCreateInfo {
			.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT,
			.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT,
			.pfnCallback = myDebugReportCallback,
		};
		auto my_vkCreateDebugReportCallbackEXT = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(vkGetInstanceProcAddr(m_inst, "vkCreateDebugReportCallbackEXT"));
		my_vkCreateDebugReportCallbackEXT(m_inst, &callbackCreateInfo, 0, &m_callback);
	}
}

void VulkanCore::createSwapChain()
{
	auto surfaceCaps = getSurfaceCaps();
	auto size = m_init.pWindowControl->getSize();
	unsigned int numImages = m_init.maxFramesInFlight;
	if (numImages < surfaceCaps.minImageCount) {
		std::cout << "Number of images: " << numImages << " is smaller than minimum image count: " << surfaceCaps.minImageCount << ". Adjusting." << std::endl;
		numImages = surfaceCaps.minImageCount;
	}
	if (surfaceCaps.maxImageCount && numImages > surfaceCaps.maxImageCount) {
		std::cout << "Number of images: " << numImages << " is higher than maximum image count: " << surfaceCaps.maxImageCount << ". Adjusting." << std::endl;
		numImages = surfaceCaps.maxImageCount;
	}
	auto extent = chooseSwapExtent(surfaceCaps, size);
	auto pm = m_init.preferedPresentMode;
	if (!m_init.forcePresentMode && std::find(m_physDevices.m_surfacePresentModes[m_gfxDevIndex].begin(), m_physDevices.m_surfacePresentModes[m_gfxDevIndex].end(), pm) == m_physDevices.m_surfacePresentModes[m_gfxDevIndex].end()) {
		pm = vk::PresentModeKHR::eFifo;
	}
	m_swapChainKHR = m_device.createSwapchainKHR({
		.surface = m_surface,
		.minImageCount = numImages,
		.imageFormat = getSurfaceFormat().format,
		.imageColorSpace = getSurfaceFormat().colorSpace,
		.imageExtent = extent,
		.imageArrayLayers = 1,
		.imageUsage = surfaceCaps.supportedUsageFlags & (vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferDst),
		.imageSharingMode = vk::SharingMode::eExclusive,
		.preTransform = vk::SurfaceTransformFlagBitsKHR::eIdentity,
		.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque,
		.presentMode = pm,
		.clipped = true,
	});
	auto images = m_device.getSwapchainImagesKHR(m_swapChainKHR);
	m_fb.init({
		.extent = extent,
		.images = &images,
		.msaaFlags = m_msaaFlags,
		.surfaceFormat = getSurfaceFormat().format,
		.depthFormat = findDepthFormat(),
		.vs = m_vsModule,
		.fs = m_fsModule,
		.getVertexBindingDescription = m_init.getVertexBindingDescription,
		.getVertexAttributeDescription = m_init.getVertexAttributeDescription,
		.clockwise = m_init.clockwise,
		.sampleShading = m_init.sampleShading,
		.noCull = m_init.noCull,
		.blending = m_init.blending,
		.depthTest = m_init.depthTest,
		.getLayoutBindings = m_init.getLayoutBindings,
		.uniforms = m_init.uniforms,
		.textureLayout = m_textures ? &m_textures->getDSLayout() : nullptr,
	});
	m_presentSems.reserve(images.size());
	m_renderSems.reserve(images.size());
	m_inFlightFences.reserve(images.size());
	for (size_t i = 0; i < images.size(); ++i) {
		m_presentSems.push_back(createSemaphore());
		m_renderSems.push_back(createSemaphore());
		m_freePresentSems.push(i);
		m_inFlightFences.push_back(getDevice().createFence({.flags = vk::FenceCreateFlagBits::eSignaled}));
	}
}

const vk::PhysicalDevice &VulkanCore::getPhysDevice() const
{
	return m_physDevices.m_devices[m_gfxDevIndex];
}

const vk::SurfaceCapabilitiesKHR VulkanCore::getSurfaceCaps() const
{
	return m_physDevices.m_devices[m_gfxDevIndex].getSurfaceCapabilitiesKHR(m_surface);
}

const vk::SurfaceFormatKHR &VulkanCore::getSurfaceFormat() const
{
	return m_physDevices.m_surfaceFormats[m_gfxDevIndex][m_gfxFormat];
}

vk::Semaphore VulkanCore::createSemaphore()
{
	auto res = m_device.createSemaphore({});
	return res;
}

vk::Extent2D VulkanCore::chooseSwapExtent(const vk::SurfaceCapabilitiesKHR& capabilities, const std::pair<uint32_t, uint32_t> &size)
{
	if (capabilities.currentExtent.width != UINT32_MAX) {
		return capabilities.currentExtent;
	} else {
		auto [w, h] = size;

		vk::Extent2D actualExtent = {w, h};
		actualExtent.width = std::clamp(actualExtent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
		actualExtent.height = std::clamp(actualExtent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

		return actualExtent;
	}
}

void VulkanCore::createShaders()
{
	m_vsModule = base::vulkanCreateShaderModule(m_device, m_init.vShader);
	m_fsModule = base::vulkanCreateShaderModule(m_device, m_init.fShader);
}

std::vector<vk::ShaderModule> VulkanCore::readShaders(const std::vector<std::filesystem::path> &paths)
{
	std::vector<vk::ShaderModule> res;
	res.reserve(paths.size());
	std::transform(paths.begin(), paths.end(), std::back_inserter(res), [this](const auto &p) {return base::vulkanCreateShaderModule(m_device, p);});
	return res;
}

void VulkanCore::destroyShaders(const std::vector<vk::ShaderModule> &shaders)
{
	for (const auto &s : shaders) {
		m_device.destroyShaderModule(s);
	}
}

vk::SampleCountFlagBits VulkanCore::getMSAAFlags() const
{
	return m_msaaFlags;
}

vk::RenderPass VulkanCore::createRenderPass(bool clear, bool offscreen, vk::Format surfaceFormat, vk::Format depthFormat, vk::SampleCountFlagBits msaaFlags)
{
	vk::AttachmentReference attachRef {
		.attachment = 0,
		.layout = vk::ImageLayout::eColorAttachmentOptimal,
	};

	vk::AttachmentReference depthAttachRef {
		.attachment = 1,
		.layout = vk::ImageLayout::eDepthStencilAttachmentOptimal,
	};

	vk::AttachmentReference colorResolveAttachRef {
		.attachment = 2,
		.layout = vk::ImageLayout::eColorAttachmentOptimal,
	};

	vk::SubpassDescription subpassDesc {
		.pipelineBindPoint = vk::PipelineBindPoint::eGraphics,
		.colorAttachmentCount = 1,
		.pColorAttachments = &attachRef,
		.pDepthStencilAttachment = &depthAttachRef,
	};
	std::vector<vk::AttachmentDescription> attachDesc;
	if (offscreen) {
		attachDesc = {{
			.format = surfaceFormat,
			.samples = msaaFlags,
			.loadOp = clear ? vk::AttachmentLoadOp::eClear : vk::AttachmentLoadOp::eLoad,
			.storeOp = vk::AttachmentStoreOp::eStore,
			.stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
			.stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
			.initialLayout = clear ? vk::ImageLayout::eUndefined : vk::ImageLayout::eShaderReadOnlyOptimal,
			.finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal,
		}, {
			.format = depthFormat,
			.samples = msaaFlags,
			.loadOp = clear ? vk::AttachmentLoadOp::eClear : vk::AttachmentLoadOp::eLoad,
			.storeOp = vk::AttachmentStoreOp::eStore,
			.stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
			.stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
			.initialLayout = clear ? vk::ImageLayout::eUndefined : vk::ImageLayout::eDepthStencilAttachmentOptimal,
			.finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal,
		}};
	} else {
		if (msaaFlags != vk::SampleCountFlagBits::e1) {
			subpassDesc.pResolveAttachments = &colorResolveAttachRef;
			attachDesc = {{
				.format = surfaceFormat,
				.samples = msaaFlags,
				.loadOp = clear ? vk::AttachmentLoadOp::eClear : vk::AttachmentLoadOp::eLoad,
				.storeOp = vk::AttachmentStoreOp::eStore,
				.stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
				.stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
				.initialLayout = clear ? vk::ImageLayout::eUndefined : vk::ImageLayout::eColorAttachmentOptimal,
				.finalLayout = vk::ImageLayout::eColorAttachmentOptimal,
			}, {
				.format = depthFormat,
				.samples = msaaFlags,
				.loadOp = clear ? vk::AttachmentLoadOp::eClear : vk::AttachmentLoadOp::eLoad,
				.storeOp = vk::AttachmentStoreOp::eStore,
				.stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
				.stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
				.initialLayout = clear ? vk::ImageLayout::eUndefined : vk::ImageLayout::eDepthStencilAttachmentOptimal,
				.finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal,
			}, {
				.format = surfaceFormat,
				.samples = vk::SampleCountFlagBits::e1,
				.loadOp = vk::AttachmentLoadOp::eDontCare,
				.storeOp = vk::AttachmentStoreOp::eStore,
				.stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
				.stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
				.initialLayout = vk::ImageLayout::eUndefined,
				.finalLayout = vk::ImageLayout::ePresentSrcKHR,
			}};
		} else {
			attachDesc = {{
				.format = surfaceFormat,
				.samples = msaaFlags,
				.loadOp = clear ? vk::AttachmentLoadOp::eClear : vk::AttachmentLoadOp::eLoad,
				.storeOp = vk::AttachmentStoreOp::eStore,
				.stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
				.stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
				.initialLayout = clear ? vk::ImageLayout::eUndefined : vk::ImageLayout::ePresentSrcKHR,
				.finalLayout = vk::ImageLayout::ePresentSrcKHR,
			}, {
				.format = depthFormat,
				.samples = msaaFlags,
				.loadOp = clear ? vk::AttachmentLoadOp::eClear : vk::AttachmentLoadOp::eLoad,
				.storeOp = vk::AttachmentStoreOp::eStore,
				.stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
				.stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
				.initialLayout = clear ? vk::ImageLayout::eUndefined : vk::ImageLayout::eDepthStencilAttachmentOptimal,
				.finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal,
			}};
		}
	}

	std::vector<vk::SubpassDependency> subpassDependencies;
	if (offscreen) {
		subpassDependencies = {{
			.srcSubpass = VK_SUBPASS_EXTERNAL,
			.srcStageMask = vk::PipelineStageFlagBits::eFragmentShader,
			.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput,
			.srcAccessMask = vk::AccessFlagBits::eShaderRead,
			.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite,
			.dependencyFlags = vk::DependencyFlagBits::eByRegion,
		}, {
			.dstSubpass = VK_SUBPASS_EXTERNAL,
			.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput,
			.dstStageMask = vk::PipelineStageFlagBits::eFragmentShader,
			.srcAccessMask = vk::AccessFlagBits::eColorAttachmentWrite,
			.dstAccessMask = vk::AccessFlagBits::eShaderRead,
			.dependencyFlags = vk::DependencyFlagBits::eByRegion,
		}};
	} else {
		subpassDependencies = {{
			.srcSubpass = VK_SUBPASS_EXTERNAL,
			.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput | vk::PipelineStageFlagBits::eEarlyFragmentTests,
			.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput | vk::PipelineStageFlagBits::eEarlyFragmentTests,
			.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite | vk::AccessFlagBits::eDepthStencilAttachmentWrite,
		}};
	}

	return getDevice().createRenderPass({
		.attachmentCount = static_cast<uint32_t>(attachDesc.size()),
		.pAttachments = attachDesc.data(),
		.subpassCount = 1,
		.pSubpasses = &subpassDesc,
		.dependencyCount = static_cast<uint32_t>(subpassDependencies.size()),
		.pDependencies = subpassDependencies.data(),
	});
}

void VulkanCore::destroyRenderPass(const vk::RenderPass &rp)
{
	getDevice().destroyRenderPass(rp);
}

void VulkanCore::createTextures()
{
	if (!m_init.textures.empty() || !m_init.texturesData.empty()) {
		m_textures = newTextures();
		std::vector<std::tuple<const void *, size_t, uint32_t, uint32_t, bool, bool>> data;
		data.reserve(m_init.textures.size() + m_init.texturesData.size());
		std::vector<image::ImageLoader> textureImage(m_init.textures.size());
		for (size_t i = 0; i < textureImage.size(); ++i) {
			textureImage[i].load(m_init.textures[i].first);
			data.emplace_back(textureImage[i].getData(), textureImage[i].getSize(), textureImage[i].getWidth(), textureImage[i].getHeight(), m_init.textures[i].second, false);
		}
		for (const auto &td : m_init.texturesData) {
			data.emplace_back(std::get<0>(td), std::get<1>(td), std::get<2>(td), std::get<3>(td), std::get<4>(td), false);
		}
		m_textures->create(data, m_init.textureFormat, false, m_init.samplerAddressMode);
	}
}

uint32_t VulkanCore::getImageIndex()
{
	size_t sem = m_freePresentSems.front();
	m_freePresentSems.pop();
	auto r = m_device.waitForFences({m_inFlightFences[sem]}, VK_TRUE, UINT64_MAX);
	if (r == vk::Result::eTimeout) {
	}
	m_device.resetFences({m_inFlightFences[sem]});
	auto res = m_device.acquireNextImageKHR(m_swapChainKHR, UINT64_MAX, m_presentSems[sem]).value;
	m_usedPresentSemaphores[res] = sem;
	return res;
}

void VulkanCore::renderScene(const vk::CommandBuffer &cmdBuf, uint32_t ind)
{
	vk::PipelineStageFlags waitFlags = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	m_queue.submit({{
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = &m_presentSems[m_usedPresentSemaphores[ind]],
		.pWaitDstStageMask = &waitFlags,
		.commandBufferCount = 1,
		.pCommandBuffers = &cmdBuf,
		.signalSemaphoreCount = 1,
		.pSignalSemaphores = &m_renderSems[ind],
	}}, m_inFlightFences[m_usedPresentSemaphores[ind]]);

	auto res = m_queue.presentKHR({
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = &m_renderSems[ind],
		.swapchainCount = 1,
		.pSwapchains = &m_swapChainKHR,
		.pImageIndices = &ind,
	});
	exception::checkVulkanResult("presentKHR failed:", res);
	m_freePresentSems.push(m_usedPresentSemaphores[ind]);
	m_usedPresentSemaphores.erase(ind);
}

void VulkanCore::createCmdBufPool()
{
	m_cmdBufPool = m_device.createCommandPool({
		.flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer | vk::CommandPoolCreateFlagBits::eTransient,
		.queueFamilyIndex = static_cast<uint32_t>(m_gfxQueueFamily),
	});
}

std::vector<vk::CommandBuffer> VulkanCore::createCommandBuffer(uint32_t count, bool secondary)
{
	return m_device.allocateCommandBuffers({
		.commandPool = m_cmdBufPool,
		.level = secondary ? vk::CommandBufferLevel::eSecondary: vk::CommandBufferLevel::ePrimary,
		.commandBufferCount = count,
	});
}

vk::Format VulkanCore::findSupportedFormat(const std::vector<vk::Format> &candidates, vk::ImageTiling tiling, vk::FormatFeatureFlags features)
{
	for (const auto &format : candidates) {
		auto props = getPhysDevice().getFormatProperties(format);
		if (tiling == vk::ImageTiling::eLinear && (props.linearTilingFeatures & features) == features) {
			return format;
		} else if (tiling == vk::ImageTiling::eOptimal && (props.optimalTilingFeatures & features) == features) {
			return format;
		}
	}
	throw exception::VulkanException("failed to find supported format");
}

vk::Format VulkanCore::findDepthFormat()
{
	return findSupportedFormat({vk::Format::eD32Sfloat, vk::Format::eD32SfloatS8Uint, vk::Format::eD24UnormS8Uint}, vk::ImageTiling::eOptimal, vk::FormatFeatureFlagBits::eDepthStencilAttachment);
}

bool VulkanCore::hasStencilComponent(const vk::Format &format)
{
	return format == vk::Format::eD32SfloatS8Uint || format == vk::Format::eD24UnormS8Uint;
}

void VulkanCore::updateUniform(const void *b, size_t s, size_t i, size_t ui)
{
	m_fb.updateUniform(b, s, i, ui);
}

void VulkanCore::cleanup()
{
	if (m_textures) {
		m_textures->destroy();
	}
	m_device.destroyCommandPool(m_cmdBufPool);
	m_fb.cleanup(false);
	cleanupSwapChain();
	for (const auto &sem : m_presentSems) {
		getDevice().destroySemaphore(sem);
	}
	for (const auto &sem : m_renderSems) {
		getDevice().destroySemaphore(sem);
	}
	for (const auto &fence : m_inFlightFences) {
		getDevice().destroyFence(fence);
	}
	m_device.destroyShaderModule(m_fsModule);
	m_device.destroyShaderModule(m_vsModule);
	m_inst.destroySurfaceKHR(m_surface);
	m_device.destroy();
	if (m_init.debug) {
		auto my_vkDestroyDebugReportCallbackEXT = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(vkGetInstanceProcAddr(m_inst, "vkDestroyDebugReportCallbackEXT"));
		my_vkDestroyDebugReportCallbackEXT(m_inst, m_callback, 0);
	}
	m_inst.destroy();
}

void VulkanCore::cleanupSwapChain()
{
	m_fb.cleanupFB();
	m_device.destroySwapchainKHR(m_swapChainKHR);
}

void VulkanCore::destroyCommandBuffer(const std::vector<vk::CommandBuffer> &b)
{
	m_device.freeCommandBuffers(m_cmdBufPool, b);
}

void VulkanCore::waitIdle()
{
	m_device.waitIdle();
}

void VulkanCore::recreateSwapChain()
{
	waitIdle();
	cleanupSwapChain();
	createSwapChain();
	m_usedPresentSemaphores.clear();
	std::queue<size_t> tmp;
	for (size_t i = 0; i < getImageCount(); ++i) {
		tmp.push(i);
	}
	m_freePresentSems.swap(tmp);
	std::cout << "Swap chain recreated" << std::endl;
}

uint32_t VulkanCore::findMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags props)
{
	auto physDevMemProps = getPhysDevice().getMemoryProperties();
	for (uint32_t i = 0; i < physDevMemProps.memoryTypeCount; ++i) {
		if ((typeFilter & (1 << i)) && (physDevMemProps.memoryTypes[i].propertyFlags & props) == props) {
			return i;
		}
	}
	throw exception::VulkanException("failed to find suitable memory type!");
}

std::tuple<std::vector<Buffer>, vk::DeviceMemory> VulkanCore::createBuffer(const std::vector<std::tuple<size_t, vk::BufferUsageFlags>> data, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties)
{
	vk::DeviceSize currentOffset = 0;
	std::vector<Buffer> bufs;
	bufs.reserve(data.size());
	uint32_t memoryTypeBits = 0;
	for (const auto &d : data) {
		auto [size, use] = d;
		if (size) {
			bufs.emplace_back(m_device.createBuffer({
				.size = size,
				.usage = usage | use,
				.sharingMode = vk::SharingMode::eExclusive,
			}), currentOffset);
			vk::MemoryRequirements mr = m_device.getBufferMemoryRequirements(bufs.back().buf);
			currentOffset += mr.size;
			if (currentOffset % mr.alignment) {
				currentOffset += mr.alignment - currentOffset % mr.alignment;
			}
			if (!memoryTypeBits) {
				memoryTypeBits = mr.memoryTypeBits;
			} else {
				memoryTypeBits &= mr.memoryTypeBits;
			}
		} else {
			bufs.emplace_back(vk::Buffer(), 0);
		}
	}

	vk::DeviceMemory mem;
	if (currentOffset) {
		mem = m_device.allocateMemory({
			.allocationSize = currentOffset,
			.memoryTypeIndex = findMemoryType(memoryTypeBits, properties), 
		});
	}

	return {bufs, mem};
}

void VulkanCore::updateBuffer(const vk::DeviceMemory &mem, vk::DeviceSize offset, const void *data, vk::DeviceSize size)
{
	auto dataMap = m_device.mapMemory(mem, offset, size, static_cast<vk::MemoryMapFlags>(0));
	memcpy(dataMap, data, size);
	m_device.unmapMemory(mem);
}

std::unique_ptr<Buffers> VulkanCore::newBuffers()
{
	return std::make_unique<Buffers>(this);
}

std::shared_ptr<Textures> VulkanCore::newTextures()
{
	return std::make_shared<Textures>(this);
}

std::vector<vk::ClearValue> VulkanCore::getClearValue(const std::array<float, 4> &color)
{
	vk::ClearColorValue clearColor(color);
	vk::ClearDepthStencilValue clearDepthStencil {1.0f, 0};
	return std::vector<vk::ClearValue> {
		clearColor,
		clearDepthStencil,
	};
}

vk::SampleCountFlagBits VulkanCore::getMaxUsableSampleCount(vk::SampleCountFlags chosenMax)
{
	unsigned int cm = static_cast<unsigned int>(chosenMax);
	std::cout << "Max Samples: " << vk::to_string(static_cast<vk::SampleCountFlagBits>(cm)) << std::endl;
	auto counts = m_physDevices.m_devProps[m_gfxDevIndex].limits.framebufferColorSampleCounts & m_physDevices.m_devProps[m_gfxDevIndex].limits.framebufferDepthSampleCounts;
	vk::SampleCountFlagBits result = vk::SampleCountFlagBits::e1;
	unsigned int sa = static_cast<unsigned int>(counts);
	counts &= vk::SampleCountFlags((~sa) >> 1);
	result = static_cast<vk::SampleCountFlagBits>(std::min(cm, static_cast<unsigned int>(counts)));
	std::cout << "Samples: " << vk::to_string(result) << std::endl;
	return result;
}

uint32_t VulkanCore::getImageCount() const
{
	return m_fb.getFrameCount();
}

const vk::Image &VulkanCore::getImage(size_t ind) const
{
	return m_fb.getFrame(ind).image;
}

const vk::RenderPass &VulkanCore::getRenderPass() const
{
	return m_fb.getRenderPass();
}

const vk::Framebuffer &VulkanCore::getFb(size_t ind) const
{
	return m_fb.getFrame(ind).fb;
}

const vk::Pipeline &VulkanCore::getPipeline() const
{
	return m_fb.getPipeline();
}

const vk::PipelineLayout &VulkanCore::getPipelineLayout() const
{
	return m_fb.getPipelineLayout();
}

std::vector<vk::DescriptorSet> VulkanCore::getDescSets(size_t i, bool appendTextures) const
{
	std::vector<vk::DescriptorSet> res = m_fb.getDescSets(i);
	if (m_textures && appendTextures) {
		res.push_back(m_textures->getDecriptorSet());
	}
	return res;
}

vk::Extent2D VulkanCore::getCurrentExtent() const
{
	return m_fb.getExtent();
}

vk::Device &VulkanCore::getDevice()
{
	return m_device;
}

vk::Queue &VulkanCore::getQueue()
{
	return m_queue;
}

float VulkanCore::getMaxAnisotropy() const
{
	return m_physDevices.m_devProps[m_gfxDevIndex].limits.maxSamplerAnisotropy;
}

const vk::SurfaceKHR &VulkanCore::getSurface() const
{
	return m_surface;
}

std::pair<uint32_t, uint32_t> VulkanCore::getTextureResolution(size_t i) const
{
	return m_textures->getResolution(i);
}

const vk::ShaderModule &VulkanCore::getVS() const
{
	return m_vsModule;
}

const vk::ShaderModule &VulkanCore::getFS() const
{
	return m_fsModule;
}

std::shared_ptr<Textures> VulkanCore::getTextures() const
{
	return m_textures;
}

void Buffers::create(const std::vector<std::tuple<const void *, size_t, vk::BufferUsageFlags>> data, bool staging)
{
	std::vector<std::tuple<size_t, vk::BufferUsageFlags>> d;
	std::transform(data.cbegin(), data.cend(), std::back_inserter(d), [](auto i) {return std::make_tuple(get<1>(i), get<2>(i));});
	auto [b, m] = m_core->createBuffer(d, staging ? vk::BufferUsageFlagBits::eTransferSrc : static_cast<vk::BufferUsageFlags>(0), vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
	for (size_t i = 0; i < data.size(); ++i) {
		m_core->getDevice().bindBufferMemory(b[i].buf, m, b[i].offset);
		if (std::get<0>(data[i])) {
			m_core->updateBuffer(m, b[i].offset, std::get<0>(data[i]), std::get<1>(data[i]));
		}
	}
	if (staging) {
		std::tie(m_bufs, m_mem) = m_core->createBuffer(d, vk::BufferUsageFlagBits::eTransferDst, vk::MemoryPropertyFlagBits::eDeviceLocal);
		auto cmd = m_core->createCommandBuffer(data.size());
		std::vector<vk::Fence> f;
		f.reserve(data.size());
		for (size_t i = 0; i < data.size(); ++i) {
			m_core->getDevice().bindBufferMemory(m_bufs[i].buf, m_mem, m_bufs[i].offset);
			f.push_back(m_core->getDevice().createFence({}));
			cmd[i].begin({.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit});
			cmd[i].copyBuffer(b[i].buf, m_bufs[i].buf, {{0, 0, get<1>(data[i])}});
			cmd[i].end();
			m_core->getQueue().submit({{.commandBufferCount = 1, .pCommandBuffers = &cmd[i]}}, f[i]);
		}
		auto r = m_core->getDevice().waitForFences(f, VK_TRUE, UINT64_MAX);
		if (r == vk::Result::eTimeout) {};
		m_core->destroyCommandBuffer(cmd);
		for (const auto &i : f) {
			m_core->getDevice().destroyFence(i);
		}
		for (const auto &i : b) {
			m_core->getDevice().destroyBuffer(i.buf);
		}
		m_core->getDevice().freeMemory(m);
	} else {
		m_bufs = b;
		m_mem = m;
	}
}

void Buffers::update(const void *b, size_t s, size_t i)
{
	m_core->updateBuffer(m_mem, m_bufs[i].offset, b, s);
}

void Buffers::destroy()
{
	for (const auto &buf : m_bufs) {
		m_core->getDevice().destroyBuffer(buf.buf);
	}
	m_core->getDevice().freeMemory(m_mem);
}

const vk::Buffer &Buffers::getBuffer(size_t i) const
{
	return m_bufs[i].buf;
}

void Textures::create(const std::vector<std::tuple<const void *, size_t, uint32_t, uint32_t, bool, bool>> data, vk::Format format, bool keepLocal, vk::SamplerAddressMode samplerAddressMode)
{
	m_keepLocal = keepLocal;
	auto formatProperties = m_core->getPhysDevice().getFormatProperties(format);
	if (!(formatProperties.optimalTilingFeatures & vk::FormatFeatureFlagBits::eSampledImageFilterLinear)) {
		throw exception::VulkanException("linear filtering not supported");
	}
	std::vector<std::tuple<size_t, vk::BufferUsageFlags>> d;
	std::transform(data.cbegin(), data.cend(), std::back_inserter(d), [](auto i) {return std::make_tuple(get<1>(i), static_cast<vk::BufferUsageFlagBits>(0));});
	std::tie(m_localBuffers, m_localMem) = m_core->createBuffer(d, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
	vk::DeviceSize memSize = 0;
	uint32_t memoryTypeBits = 0;
	m_textures.resize(data.size());
	std::vector<uint32_t> mipLevels;
	for (size_t i = 0; i < data.size(); ++i) {
		auto [d, size, width, height, genMipMap, fb] = data[i];
		uint32_t ml = 1;
		if (genMipMap) {
			ml = static_cast<uint32_t>(std::floor(std::log2(std::max(width, height)))) + 1;
		}
		mipLevels.push_back(ml);
		m_textures[i].width = width;
		m_textures[i].height = height;
		m_textures[i].image = m_core->getDevice().createImage({
			.imageType = vk::ImageType::e2D,
			.format = format,
			.extent = {.width = width, .height = height, .depth = 1},
			.mipLevels = ml,
			.arrayLayers = 1,
			.samples = vk::SampleCountFlagBits::e1,
			.tiling = vk::ImageTiling::eOptimal,
			.usage = fb ? vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eColorAttachment : vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled,
			.sharingMode = vk::SharingMode::eExclusive,
			.initialLayout = vk::ImageLayout::eUndefined,
		});
		if (d) {
			m_core->getDevice().bindBufferMemory(m_localBuffers[i].buf, m_localMem, m_localBuffers[i].offset);
			m_core->updateBuffer(m_localMem, m_localBuffers[i].offset, d, size);
		}
		auto mr = m_core->getDevice().getImageMemoryRequirements(m_textures[i].image);
		m_textures[i].offset = memSize;
		memSize += mr.size;
		if (memSize % mr.alignment) {
			memSize += mr.alignment - memSize % mr.alignment;
		}
		if (!memoryTypeBits) {
			memoryTypeBits = mr.memoryTypeBits;
		} else {
			memoryTypeBits &= mr.memoryTypeBits;
		}
	}
	std::vector<vk::CommandBuffer> cmd;
	m_mem = m_core->getDevice().allocateMemory({
		.allocationSize = memSize,
		.memoryTypeIndex = m_core->findMemoryType(memoryTypeBits, vk::MemoryPropertyFlagBits::eDeviceLocal),
	});
	cmd = m_core->createCommandBuffer(data.size());
	std::vector<vk::Fence> f;
	f.reserve(data.size());
	for (size_t i = 0; i < data.size(); ++i) {
		auto [d, size, width, height, genMipMap, fb] = data[i];
		m_core->getDevice().bindImageMemory(m_textures[i].image, m_mem, m_textures[i].offset);
		if (d) {
			f.push_back(m_core->getDevice().createFence({}));
			cmd[i].begin({.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit});
			cmd[i].pipelineBarrier(vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTransfer, static_cast<vk::DependencyFlags>(0), {}, {}, {{
				.dstAccessMask = vk::AccessFlagBits::eTransferWrite,
				.oldLayout = vk::ImageLayout::eUndefined,
				.newLayout = vk::ImageLayout::eTransferDstOptimal,
				.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
				.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
				.image = m_textures[i].image,
				.subresourceRange = {.aspectMask = vk::ImageAspectFlagBits::eColor, .baseMipLevel = 0, .levelCount = mipLevels[i], .baseArrayLayer = 0, .layerCount = 1},
			}});
			cmd[i].copyBufferToImage(m_localBuffers[i].buf, m_textures[i].image, vk::ImageLayout::eTransferDstOptimal, {{
				.imageSubresource {.aspectMask = vk::ImageAspectFlagBits::eColor, .layerCount = 1},
				.imageExtent = {.width = width, .height = height, .depth = 1}, 
			}});
			if (genMipMap) {
				int32_t mw = width;
				int32_t mh = height;
				for (uint32_t j = 1; j < mipLevels[i]; ++j) {
					cmd[i].pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eTransfer, static_cast<vk::DependencyFlags>(0), {}, {}, {{
						.srcAccessMask = vk::AccessFlagBits::eTransferWrite,
						.dstAccessMask = vk::AccessFlagBits::eTransferRead,
						.oldLayout = vk::ImageLayout::eTransferDstOptimal,
						.newLayout = vk::ImageLayout::eTransferSrcOptimal,
						.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
						.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
						.image = m_textures[i].image,
						.subresourceRange = {.aspectMask = vk::ImageAspectFlagBits::eColor, .baseMipLevel = j - 1, .levelCount = 1, .baseArrayLayer = 0, .layerCount = 1},
					}});
					cmd[i].blitImage(m_textures[i].image, vk::ImageLayout::eTransferSrcOptimal, m_textures[i].image, vk::ImageLayout::eTransferDstOptimal, {vk::ImageBlit {
						.srcSubresource = {.aspectMask = vk::ImageAspectFlagBits::eColor, .mipLevel = j - 1, .baseArrayLayer = 0, .layerCount = 1},
						.srcOffsets = std::array<vk::Offset3D, 2> {vk::Offset3D {0, 0, 0}, vk::Offset3D {mw, mh, 1}},
						.dstSubresource = {.aspectMask = vk::ImageAspectFlagBits::eColor, .mipLevel = j, .baseArrayLayer = 0, .layerCount = 1},
						.dstOffsets = std::array<vk::Offset3D, 2> {vk::Offset3D {0, 0, 0}, vk::Offset3D {mw > 1 ? mw / 2 : 1, mh > 1 ? mh / 2 : 1, 1}},
					}}, vk::Filter::eLinear);
					cmd[i].pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, static_cast<vk::DependencyFlags>(0), {}, {}, {{
						.srcAccessMask = vk::AccessFlagBits::eTransferRead,
						.dstAccessMask = vk::AccessFlagBits::eShaderRead,
						.oldLayout = vk::ImageLayout::eTransferSrcOptimal,
						.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal,
						.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
						.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
						.image = m_textures[i].image,
						.subresourceRange = {.aspectMask = vk::ImageAspectFlagBits::eColor, .baseMipLevel = j - 1, .levelCount = 1, .baseArrayLayer = 0, .layerCount = 1},
					}});
					if (mw > 1) {
						mw /= 2;
					}
					if (mh > 1) {
						mh /= 2;
					}
				}
			}
			cmd[i].pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, static_cast<vk::DependencyFlags>(0), {}, {}, {{
				.srcAccessMask = vk::AccessFlagBits::eTransferWrite,
				.dstAccessMask = vk::AccessFlagBits::eShaderRead,
				.oldLayout = vk::ImageLayout::eTransferDstOptimal,
				.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal,
				.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
				.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
				.image = m_textures[i].image,
				.subresourceRange = {.aspectMask = vk::ImageAspectFlagBits::eColor, .baseMipLevel = mipLevels[i] - 1, .levelCount = 1, .baseArrayLayer = 0, .layerCount = 1},
			}});
			cmd[i].end();
			m_core->getQueue().submit({{.commandBufferCount = 1, .pCommandBuffers = &cmd[i]}}, f[i]);
		}
	}
	if (!f.empty()) {
		auto r = m_core->getDevice().waitForFences(f, VK_TRUE, UINT64_MAX);
		if (r == vk::Result::eTimeout) {};
	}
	m_core->destroyCommandBuffer(cmd);
	for (const auto &i : f) {
		m_core->getDevice().destroyFence(i);
	}
	if (!m_keepLocal) {
		for (const auto &i : m_localBuffers) {
			m_core->getDevice().destroyBuffer(i.buf);
		}
		m_core->getDevice().freeMemory(m_localMem);
	}

	float maxmip = 0;
	for (size_t i = 0; i < data.size(); ++i) {
		m_textures[i].view = m_core->getDevice().createImageView({
			.image =  m_textures[i].image,
			.viewType = vk::ImageViewType::e2D,
			.format = format,
			.subresourceRange = {.aspectMask = vk::ImageAspectFlagBits::eColor, .levelCount = mipLevels[i], .layerCount = 1},
		});
		maxmip = std::max(maxmip, static_cast<float>(mipLevels[i]));
	}
	m_sampler = m_core->getDevice().createSampler({
		.magFilter = vk::Filter::eLinear,
		.minFilter = vk::Filter::eLinear,
		.mipmapMode = vk::SamplerMipmapMode::eLinear,
		.addressModeU = samplerAddressMode,
		.addressModeV = samplerAddressMode,
		.addressModeW = samplerAddressMode,
		.anisotropyEnable = VK_TRUE,
		.maxAnisotropy = m_core->getMaxAnisotropy(),
		.compareEnable = VK_FALSE,
		.compareOp = vk::CompareOp::eAlways,
		.minLod = 0.0f,
		.maxLod = maxmip,
		.borderColor = vk::BorderColor::eIntOpaqueBlack,
		.unnormalizedCoordinates = VK_FALSE,
	});
	createDescriptorSet();
}

void Textures::destroy()
{
	m_core->getDevice().destroySampler(m_sampler);
	for (const auto &t : m_textures) {
		m_core->getDevice().destroyImageView(t.view);
		m_core->getDevice().destroyImage(t.image);
	}
	m_core->getDevice().freeMemory(m_mem);
	m_core->getDevice().destroyDescriptorPool(m_descPool);
	m_core->getDevice().destroyDescriptorSetLayout(m_descriptorLayout);
	if (m_keepLocal) {
		for (const auto &i : m_localBuffers) {
			m_core->getDevice().destroyBuffer(i.buf);
		}
		m_core->getDevice().freeMemory(m_localMem);
	}
}

const vk::Sampler &Textures::getSampler() const
{
	return m_sampler;
}

std::vector<vk::ImageView> Textures::getViews() const
{
	std::vector<vk::ImageView> res;
	std::transform(m_textures.cbegin(), m_textures.cend(), std::back_inserter(res), [](auto t) {return t.view;});
	return res;
}

std::vector<vk::Image> Textures::getImages() const
{
	std::vector<vk::Image> res;
	std::transform(m_textures.cbegin(), m_textures.cend(), std::back_inserter(res), [](auto t) {return t.image;});
	return res;
}

std::pair<uint32_t, uint32_t> Textures::getResolution(size_t i) const
{
	return std::make_pair(m_textures[i].width, m_textures[i].height);
}

size_t Textures::getSize() const
{
	return m_textures.size();
}

const vk::DescriptorSet &Textures::getDecriptorSet() const
{
	return m_desSet;
}

const vk::DescriptorSetLayout &Textures::getDSLayout() const
{
	return m_descriptorLayout;
}

const vk::Buffer &Textures::getLocalBuffer(size_t i) const
{
	return m_localBuffers[i].buf;
}

void Textures::bindBuffer(size_t i) const
{
	m_core->getDevice().bindBufferMemory(m_localBuffers[i].buf, m_localMem, m_localBuffers[i].offset);
}

void Textures::updateBuffer(size_t i, void *data, size_t size) const
{
	m_core->updateBuffer(m_localMem, m_localBuffers[i].offset, data, size);
}

void Textures::createDescriptorSet()
{
	std::vector<vk::DescriptorPoolSize> poolSize;
	poolSize.push_back({
		.type = vk::DescriptorType::eCombinedImageSampler,
		.descriptorCount = static_cast<uint32_t>(getSize()),
	});
	m_descPool = m_core->getDevice().createDescriptorPool({
		.maxSets = 1,
		.poolSizeCount = static_cast<uint32_t>(poolSize.size()),
		.pPoolSizes = poolSize.data(),
	});
	std::vector<vk::WriteDescriptorSet> wr;
	std::vector<vk::DescriptorImageInfo> ii;
	for (const auto &v : getViews()) {
		ii.push_back({
			.sampler = getSampler(),
			.imageView = v,
			.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal,
		});
	}
	std::vector<vk::DescriptorSetLayoutBinding> slb;
	for (uint32_t i = 0; i < getSize(); ++i) {
		slb.push_back({
			.binding = i,
			.descriptorType = vk::DescriptorType::eCombinedImageSampler,
			.descriptorCount = 1,
			.stageFlags = vk::ShaderStageFlagBits::eFragment,
		});
	}
	m_descriptorLayout = m_core->getDevice().createDescriptorSetLayout({
		.bindingCount = static_cast<uint32_t>(slb.size()),
		.pBindings = slb.data(),
	});
	m_desSet = m_core->getDevice().allocateDescriptorSets({
		.descriptorPool = m_descPool,
		.descriptorSetCount = 1,
		.pSetLayouts = &m_descriptorLayout,
	})[0];
	wr.push_back({
		.dstSet = m_desSet,
		.dstBinding = 0,
		.dstArrayElement = 0,
		.descriptorCount = static_cast<uint32_t>(ii.size()),
		.descriptorType = vk::DescriptorType::eCombinedImageSampler,
		.pImageInfo = ii.data(),
	});
	m_core->getDevice().updateDescriptorSets(wr, {});
}

void Uniforms::destroy()
{
	if (m_buf) {
		m_buf->destroy();
	}
	for (auto &i : m_descriptorLayout) {
		m_core->getDevice().destroyDescriptorSetLayout(i);
	}
	m_core->getDevice().destroyDescriptorPool(m_descPool);
}

void Uniforms::init(std::function<std::vector<std::vector<vk::DescriptorSetLayoutBinding>>()> getLayoutBindings, std::shared_ptr<std::vector<std::vector<size_t>>> uniforms, size_t frames)
{
	if (getLayoutBindings) {
		auto slb = getLayoutBindings();
		for (const auto &s : slb) {
			m_descriptorLayout.push_back(m_core->getDevice().createDescriptorSetLayout({
				.bindingCount = static_cast<uint32_t>(s.size()),
				.pBindings = s.data(),
			}));
		}
	}

	if (uniforms && !uniforms->empty()) {
		std::vector<vk::DescriptorPoolSize> poolSize;
		for (const auto &u : *uniforms) {
			poolSize.push_back({
				.type = vk::DescriptorType::eUniformBuffer,
				.descriptorCount = static_cast<uint32_t>(frames * u.size()),
			});
		}

		m_descPool = m_core->getDevice().createDescriptorPool({
			.maxSets = static_cast<uint32_t>(frames * uniforms->size()),
			.poolSizeCount = static_cast<uint32_t>(poolSize.size()),
			.pPoolSizes = poolSize.data(),
		});

		std::vector<std::tuple<const void *, size_t, vk::BufferUsageFlags>> ub;
		for (size_t i = 0; i < frames; ++i) {
			for (const auto &uni : *uniforms) {
				for (const auto &u : uni) {
					ub.push_back({0, u, vk::BufferUsageFlagBits::eUniformBuffer});
				}
			}
		}
		m_buf = m_core->newBuffers();
		m_buf->create(ub, false);

		m_desSets.reserve(frames);
		for (size_t i = 0; i < frames; ++i) {
			m_desSets.push_back( m_core->getDevice().allocateDescriptorSets({
				.descriptorPool = m_descPool,
				.descriptorSetCount = static_cast<uint32_t>(m_descriptorLayout.size()),
				.pSetLayouts = m_descriptorLayout.data(),
			}));
		}

		std::vector<std::vector<std::vector<vk::DescriptorBufferInfo>>> dbi;
		dbi.resize(frames);
		size_t ind = 0;
		for (size_t i = 0; i < frames; ++i) {
			dbi[i].resize(uniforms->size());
			for (size_t j = 0; j < uniforms->size(); ++j) {
				for (size_t k = 0; k < uniforms->at(j).size(); ++k) {
					dbi[i][j].push_back({
						.buffer = m_buf->getBuffer(ind++),
						.offset = 0,
						.range = uniforms->at(j)[k],
					});
					if (!i) {
						++m_uniformCount;
					}
				}
			}
		}

		std::vector<vk::WriteDescriptorSet> wr;
		std::vector<vk::DescriptorImageInfo> ii;
		for (size_t i = 0; i < frames; ++i) {
			for (size_t j = 0; j < uniforms->size(); ++j) {
				wr.push_back({
					.dstSet = m_desSets[i][j],
					.dstBinding = 0,
					.dstArrayElement = 0,
					.descriptorCount = static_cast<uint32_t>(uniforms->at(j).size()),
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.pBufferInfo = dbi[i][j].data(),
				});
			}
		}
		m_core->getDevice().updateDescriptorSets(wr, {});
	}
}

void Uniforms::updateUniform(const void *b, size_t s, size_t i, size_t ui)
{
	m_buf->update(b, s, i * m_uniformCount + ui);
}

const std::vector<vk::DescriptorSetLayout> &Uniforms::getLayout() const
{
	return m_descriptorLayout;
}

const std::vector<vk::DescriptorSet> &Uniforms::getDescSets(size_t i) const
{
	return m_desSets[i];
}

}
}
}

