#include <vulkan_exception.h>
#include <sstream>

namespace macsnet {
namespace vulkan {
namespace exception {

VulkanException::VulkanException(const std::string &mes, VkResult res)
{
	std::stringstream str;
	str << mes << " " << res;
	message = str.str();
}

VulkanException::VulkanException(const std::string &mes, vk::Result res, bool subopt) : subopt(subopt)
{
	std::stringstream str;
	str << mes << " " << res;
	message = str.str();
}

VulkanException::~VulkanException()
{
}

const char *VulkanException::what() const noexcept
{
	return message.c_str();
}

void checkVulkanResult(const std::string &mes, VkResult res)
{
	if (res != VK_SUCCESS) {
		throw VulkanException(mes, res);
	}
}

void checkVulkanResult(const std::string &mes, vk::Result res)
{
	if (res != vk::Result::eSuccess) {
		bool subopt = false;
		if (res == vk::Result::eSuboptimalKHR) {
			subopt = true;
		}
		throw VulkanException(mes, res, subopt);
	}
}

}
}
}

