#pragma once

#include <vulkan_base.h>
#include <GLFW/glfw3.h>
#include <atomic>

namespace macsnet {
namespace vulkan {
namespace glfw {

class GLFWControl : public base::VulkanWindowControl {
	public:
		GLFWControl(const std::string &appName);
		~GLFWControl();
		virtual void init(unsigned int width, unsigned int height, bool full, bool resize) override;
		virtual vk::SurfaceKHR createSurface(vk::Instance &inst) override;
		void cleanup();
		bool shouldClose();
		void pollEvents();
		virtual std::vector<const char *> getExtensions() override;
		virtual std::pair<uint32_t, uint32_t> getSize() override;
		bool wasResized();
	private:
		static void resizeCallback(GLFWwindow *window, int w, int h);
		static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		GLFWmonitor *getCurrentMonitor(GLFWwindow *window);
		void modeWindow();
		void modeFullscreen();
		void modeBorderless();

		enum WINDOW_MODE {WINDOW, BORDERLESS, FULLSCREEN};
		GLFWwindow *window = nullptr;
		std::atomic<bool> resized = false;
		std::string appName;
		bool m_resizable = false;
		unsigned int m_width = 0;
		unsigned int m_height = 0;
		int m_lastx = 0;
		int m_lasty = 0;
		WINDOW_MODE m_mode = WINDOW;
};

}
}
}

