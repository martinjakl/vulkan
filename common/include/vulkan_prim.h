#pragma once
#include <vector>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

namespace macsnet {
namespace vulkan {
namespace primitives {

struct Vertex {
	glm::vec4 pos;
	glm::vec4 color;
	glm::vec2 texcoord;
	glm::vec2 texcoord2;
	glm::vec3 normal;
};

class Primitives {
	public:
		enum FILLED {POS = 0x1, COLOR = 0x2, TEXCOORD = 0x4, TEXCOORD2 = 0x8, NORMAL = 0x10};
		Primitives();
		~Primitives();

		const std::vector<Vertex> &getVertex() const;
		uint32_t getFilled() const;

		void createBigF();
		void createQuad(float size);
		void center();

	private:
		std::vector<Vertex> v;
		uint32_t f = 0;
};

}
}
}

