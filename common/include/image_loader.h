#pragma once

#include <filesystem>
#include <stb/stb_image.h>

namespace macsnet {
namespace vulkan {
namespace image {

class ImageLoader {
	public:
		ImageLoader();
		~ImageLoader();
		void load(const std::filesystem::path &path);
		void *getData() const;
		int getWidth() const;
		int getHeight() const;
		size_t getSize() const;
	private:
		stbi_uc *data = 0;
		int width = 0;
		int height = 0;
		int channels = 0;
};

}
}
}

