#pragma once
#include <string>
#include <memory>
#include <map>
#include <queue>
#include <functional>
#include "vulkan_base.h"

namespace macsnet {
namespace vulkan {
namespace core {

struct Frame {
	vk::Image image;
	vk::ImageView view;
	vk::Framebuffer fb;
};

class VulkanCore;

struct CoreInit {
	base::VulkanWindowControl *pWindowControl;
	bool debug = false;
	std::filesystem::path vShader;
	std::filesystem::path fShader;
	bool clockwise = false;
	int maxFramesInFlight = 0;
	std::function<std::vector<vk::VertexInputBindingDescription>()>  getVertexBindingDescription;
	std::function<std::vector<vk::VertexInputAttributeDescription>()>  getVertexAttributeDescription;
	std::function<std::vector<std::vector<vk::DescriptorSetLayoutBinding>>()> getLayoutBindings;
	std::shared_ptr<std::vector<std::vector<size_t>>> uniforms;
	std::vector<std::pair<std::filesystem::path, bool>> textures;
	std::vector<std::tuple<const void *, size_t, uint32_t, uint32_t, bool>> texturesData;
	vk::SampleCountFlags msaaFlags = vk::SampleCountFlagBits::e1;
	bool sampleShading = false;
	bool noCull = false;
	bool blending = false;
	bool depthTest = true;
	vk::Format preferedFormat = vk::Format::eB8G8R8A8Unorm;
	vk::Format textureFormat = vk::Format::eR8G8B8A8Srgb;
	vk::PresentModeKHR preferedPresentMode = vk::PresentModeKHR::eFifo;
	bool forcePresentMode = false;
	vk::SamplerAddressMode samplerAddressMode = vk::SamplerAddressMode::eRepeat;
};

struct Buffer {
	vk::Buffer buf;
	vk::DeviceSize offset;
};

class Buffers {
	public:
		Buffers(VulkanCore *core) : m_core(core) {};
		~Buffers() {};
		void create(const std::vector<std::tuple<const void *, size_t, vk::BufferUsageFlags>> data, bool staging);
		void destroy();
		void update(const void *b, size_t s, size_t i);
		const vk::Buffer &getBuffer(size_t i) const;

	private:
		VulkanCore *m_core;
		vk::DeviceMemory m_mem;
		std::vector<Buffer> m_bufs;
};

class Uniforms {
	public:
		Uniforms(VulkanCore *core) : m_core(core) {}
		~Uniforms() {}
		void destroy();
		void init(std::function<std::vector<std::vector<vk::DescriptorSetLayoutBinding>>()> getLayoutBindings, std::shared_ptr<std::vector<std::vector<size_t>>> uniforms, size_t frames);
		const std::vector<vk::DescriptorSetLayout> &getLayout() const;
		void updateUniform(const void *b, size_t s, size_t i, size_t ui);
		const std::vector<vk::DescriptorSet> &getDescSets(size_t i) const;

	private:
		VulkanCore *m_core;
		std::shared_ptr<Buffers> m_buf;
		std::vector<vk::DescriptorSetLayout> m_descriptorLayout;
		vk::DescriptorPool m_descPool;
		std::vector<std::vector<vk::DescriptorSet>> m_desSets;
		size_t m_uniformCount = 0;
};

class Textures {
	public:
		Textures(VulkanCore *core) : m_core(core) {};
		~Textures() {};
		void create(const std::vector<std::tuple<const void *, size_t, uint32_t, uint32_t, bool, bool>> data, vk::Format format = vk::Format::eR8G8B8A8Srgb, bool keepLocal = false, vk::SamplerAddressMode samplerAddressMode = vk::SamplerAddressMode::eRepeat);
		void destroy();
		const vk::Sampler &getSampler() const;
		std::vector<vk::ImageView> getViews() const;
		std::vector<vk::Image> getImages() const;
		std::pair<uint32_t, uint32_t> getResolution(size_t i) const;
		size_t getSize() const;
		const vk::DescriptorSet &getDecriptorSet() const;
		const vk::DescriptorSetLayout &getDSLayout() const;
		const vk::Buffer &getLocalBuffer(size_t i) const;
		void bindBuffer(size_t i) const;
		void updateBuffer(size_t i, void *data, size_t size) const;

	private:
		void createDescriptorSet();
		struct Texture {
			vk::Image image;
			vk::ImageView view;
			uint32_t width;
			uint32_t height;
			vk::DeviceSize offset;
		};

		VulkanCore *m_core;
		vk::Sampler m_sampler;
		vk::DeviceMemory m_mem;
		std::vector<Texture> m_textures;
		vk::DescriptorPool m_descPool;
		vk::DescriptorSet m_desSet;
		vk::DescriptorSetLayout m_descriptorLayout;
		std::vector<Buffer> m_localBuffers;
		vk::DeviceMemory m_localMem;
		bool m_keepLocal = false;
};

struct PipeInit {
	const vk::ShaderModule &vs;
	const vk::ShaderModule &fs;
	std::function<std::vector<vk::VertexInputBindingDescription>()> getVertexBindingDescription;
   	std::function<std::vector<vk::VertexInputAttributeDescription>()> getVertexAttributeDescription;
   	const vk::Extent2D &extent;
	bool noCull = false;
	bool clockwise = false;
	bool sampleShading = false;
	bool blending = false;
	bool depthTest = true;
	vk::SampleCountFlagBits msaaFlags = vk::SampleCountFlagBits::e1;
   	const std::vector<vk::DescriptorSetLayout> *uniLayout = nullptr;
	const vk::DescriptorSetLayout *textureLayout = nullptr;
	const vk::RenderPass &renderPass;
};

class Pipeline {
	public:
		Pipeline(VulkanCore *core);
		~Pipeline();
		void init(const PipeInit &init);
		void destroy(bool complete);
		void destroyPipe();
		const vk::Pipeline &getPipeline() const;
		const vk::PipelineLayout &getPipelineLayout() const;

	private:
		VulkanCore *m_core;
		vk::Pipeline m_pipeline;
		vk::PipelineCache m_pipelineCache;
		vk::PipelineLayout m_pipelineLayout;
};

struct FBInit {
	const vk::Extent2D &extent;
   	const std::vector<vk::Image> *images = nullptr;
   	vk::SampleCountFlagBits msaaFlags = vk::SampleCountFlagBits::e1;
   	vk::Format surfaceFormat;
   	vk::Format depthFormat;
	vk::ShaderModule vs;
	vk::ShaderModule fs;
	bool offscreen = false;
	uint32_t numImages = 0;
	std::function<std::vector<vk::VertexInputBindingDescription>()> getVertexBindingDescription;
	std::function<std::vector<vk::VertexInputAttributeDescription>()> getVertexAttributeDescription;
	bool clockwise = false;
	bool sampleShading = false;
	bool noCull = false;
	bool blending = false;
	bool depthTest = true;
	std::function<std::vector<std::vector<vk::DescriptorSetLayoutBinding>>()> getLayoutBindings;
	std::shared_ptr<std::vector<std::vector<size_t>>> uniforms;
	const vk::DescriptorSetLayout *textureLayout = nullptr;
};

class FrameBuffer {
	public:
		FrameBuffer(VulkanCore *core);
		~FrameBuffer();
		void init(const FBInit &init);
		void createRenderPass(const FBInit &init);
		void createFrameBuffer(const FBInit &init);
		void cleanup(bool complete = true);
		void cleanupFB();
		void createDepthResource(const FBInit &init);
		Frame &getFrame(size_t i);
		const Frame &getFrame(size_t i) const;
		void updateUniform(const void *b, size_t s, size_t i, size_t ui);
		size_t getFrameCount() const;
		const vk::RenderPass &getRenderPass() const;
		const vk::Pipeline &getPipeline() const;
		const vk::PipelineLayout &getPipelineLayout() const;
		const vk::Extent2D &getExtent() const;
		const std::vector<vk::DescriptorSet> &getDescSets(size_t i) const;

	private:
		VulkanCore *m_core;
		std::vector<Frame> m_frames;
		vk::Extent2D m_extent;
		vk::RenderPass m_renderPass;
		vk::Image m_depthImage;
		vk::Image m_colorImage;
		vk::DeviceMemory m_depthColorImageMemory;
		vk::ImageView m_depthImageView;
		vk::ImageView m_colorImageView;
		std::shared_ptr<Uniforms> m_uniforms;
		Pipeline m_pipeline;
};

class VulkanCore {
	public:
		VulkanCore(const std::string &AppName);
		~VulkanCore();
		bool init(base::VulkanWindowControl *pWindowControl, bool debug, const std::filesystem::path &vShader, const std::filesystem::path &fShader, bool clockwise, int maxFramesInFlight);
		bool init(const CoreInit &i);
		uint32_t getImageCount() const;
		const vk::Image &getImage(size_t ind) const;
		const vk::RenderPass &getRenderPass() const;
		const vk::Framebuffer &getFb(size_t ind) const;
		const vk::Pipeline &getPipeline() const;
		const vk::PipelineLayout &getPipelineLayout() const;
		std::vector<vk::DescriptorSet> getDescSets(size_t i, bool appendTextures = true) const;
		uint32_t getImageIndex();
		void renderScene(const vk::CommandBuffer &cmdBuf, uint32_t ind);
		std::vector<vk::CommandBuffer> createCommandBuffer(uint32_t count, bool secondary = false);
		void destroyCommandBuffer(const std::vector<vk::CommandBuffer> &b);
		void cleanup();
		void waitIdle();
		void recreateSwapChain();
		vk::Extent2D getCurrentExtent() const;
		std::unique_ptr<Buffers> newBuffers();
		std::shared_ptr<Textures> newTextures();
		void updateUniform(const void *b, size_t s, size_t i, size_t ui);
		std::vector<vk::ClearValue> getClearValue(const std::array<float, 4> &color);
		vk::Semaphore createSemaphore();
		uint32_t findMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags props);
		std::tuple<std::vector<Buffer>, vk::DeviceMemory> createBuffer(const std::vector<std::tuple<size_t, vk::BufferUsageFlags>> data, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties);
		void updateBuffer(const vk::DeviceMemory &mem, vk::DeviceSize offset, const void *data, vk::DeviceSize size);
		vk::Device &getDevice();
		vk::Queue &getQueue();
		const vk::PhysicalDevice &getPhysDevice() const;
		float getMaxAnisotropy() const;
		std::pair<uint32_t, uint32_t> getTextureResolution(size_t i) const;
		vk::Format findDepthFormat();
		const vk::SurfaceFormatKHR &getSurfaceFormat() const;
		const vk::ShaderModule &getVS() const;
		const vk::ShaderModule &getFS() const;
		std::shared_ptr<Textures> getTextures() const;
		std::vector<vk::ShaderModule> readShaders(const std::vector<std::filesystem::path> &paths);
		void destroyShaders(const std::vector<vk::ShaderModule> &shaders);
		vk::SampleCountFlagBits getMSAAFlags() const;
		vk::RenderPass createRenderPass(bool clear, bool offscreen, vk::Format surfaceFormat, vk::Format depthFormat, vk::SampleCountFlagBits msaaFlags);
		void destroyRenderPass(const vk::RenderPass &rp);

	private:
		const vk::SurfaceCapabilitiesKHR getSurfaceCaps() const;
		const vk::SurfaceKHR &getSurface() const;
		void createInstance();
		void createSurface();
		void selectPhysicalDevice(vk::Format preferedformat);
		void createLogicalDevice();
		void createShaders();
		void createSwapChain();
		void createCmdBufPool();
		void createTextures();
		void cleanupSwapChain();
		vk::Extent2D chooseSwapExtent(const vk::SurfaceCapabilitiesKHR& capabilities, const std::pair<uint32_t, uint32_t> &size);
		vk::Format findSupportedFormat(const std::vector<vk::Format> &candidates, vk::ImageTiling tiling, vk::FormatFeatureFlags features);
		bool hasStencilComponent(const vk::Format &format);
		vk::SampleCountFlagBits getMaxUsableSampleCount(vk::SampleCountFlags chosenMax);

		CoreInit m_init;
		vk::Instance m_inst;
		vk::Device m_device;
		vk::SurfaceKHR m_surface;
		base::VulkanPhysicalDevices m_physDevices;
		std::string m_appName;
		int m_gfxDevIndex = -1;
		int m_gfxQueueFamily = -1;
		int m_gfxFormat = -1;
		vk::ShaderModule m_vsModule;
		vk::ShaderModule m_fsModule;
		vk::Queue m_queue;
		vk::CommandPool m_cmdBufPool;
		VkDebugReportCallbackEXT m_callback;
		std::shared_ptr<Textures> m_textures;
		vk::SampleCountFlagBits m_msaaFlags = vk::SampleCountFlagBits::e1;
		FrameBuffer m_fb;
		vk::SwapchainKHR m_swapChainKHR;
		std::vector<vk::Semaphore> m_presentSems;
		std::vector<vk::Semaphore> m_renderSems;
		std::queue<size_t> m_freePresentSems;
		std::map<uint32_t, size_t> m_usedPresentSemaphores;
		std::vector<vk::Fence> m_inFlightFences;
};

}
}
}

