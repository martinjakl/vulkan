#pragma once
#include <vulkan_core.h>

namespace macsnet {
namespace vulkan {
namespace overlay {

class Overlay {
	public:
		Overlay(core::VulkanCore *core);
		~Overlay();
		void init(const std::filesystem::path &path, size_t frames, uint32_t width, uint32_t height, vk::Format format);
		void reinit();
		void addCommandsBegin(size_t ind, vk::CommandBuffer &cmd);
		void addCommandsEnd(size_t ind, vk::CommandBuffer &cmd);
		void update(size_t ind, const std::string &text);
		void destroy();
	private:
		void createPipe();
		core::VulkanCore *m_core;
		std::vector<vk::ShaderModule> m_shaders;
		std::unique_ptr<core::Buffers> m_vertBuffer;
		core::Pipeline m_pipe;
		core::Uniforms m_uni;
		std::vector<core::Textures> m_textures;
		uint32_t m_width = 0;
		uint32_t m_height = 0;
		std::string m_lastText;
		std::unique_ptr<unsigned char[]> m_data;
		std::vector<bool> m_updateTexture;
};

}
}
}

