#pragma once

#include <vector>
#include <filesystem>

#define VULKAN_HPP_NO_STRUCT_CONSTRUCTORS
#include <vulkan/vulkan.hpp>
#include <vulkan/vk_platform.h>

namespace macsnet {
namespace vulkan {
namespace base {

struct VulkanPhysicalDevices {
	std::vector<vk::PhysicalDevice> m_devices;
	std::vector<vk::PhysicalDeviceProperties> m_devProps;
	std::vector<std::vector<vk::QueueFamilyProperties>> m_qFamilyProps;
	std::vector<std::vector<vk::Bool32>> m_qSupportsPresent;
	std::vector<std::vector<vk::SurfaceFormatKHR>> m_surfaceFormats;
	std::vector<std::vector<vk::PresentModeKHR>> m_surfacePresentModes;
};

class VulkanWindowControl {
	protected:
		VulkanWindowControl() {}
	public:
		~VulkanWindowControl() {}
		virtual void init(unsigned int width, unsigned int height, bool full, bool resize) = 0;
		virtual vk::SurfaceKHR createSurface(vk::Instance &inst) = 0;
		virtual std::vector<const char *> getExtensions() = 0;
		virtual std::pair<uint32_t, uint32_t> getSize() = 0;
};

std::vector<vk::ExtensionProperties> vulkanEnumExtProps();
void vulkanGetPhysicalDevices(const vk::Instance &inst, const vk::SurfaceKHR &surface, VulkanPhysicalDevices &physDevices);
void vulkanPrintImageUsageFlags(const vk::ImageUsageFlags &flags, const std::string &prefix);
vk::ShaderModule vulkanCreateShaderModule(vk::Device &device, const std::filesystem::path &pFileName);

}
}
}

