#pragma once

#include <exception>
#include <string>
#include <vulkan_base.h>

namespace macsnet {
namespace vulkan {
namespace exception {

class VulkanException : public std::exception {
	public:
		VulkanException(const std::string &mes, VkResult res);
		VulkanException(const std::string &mes, vk::Result res, bool suopt = false);
		VulkanException(const std::string &mes) : message(mes) {}
		virtual ~VulkanException();
		const char *what() const noexcept override;
		bool isSubopt() {return subopt;}
	private:
		std::string message;
		bool subopt = false;
};

void checkVulkanResult(const std::string &mes, VkResult res);
void checkVulkanResult(const std::string &mes, vk::Result res);

}
}
}

