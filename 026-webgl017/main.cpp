#ifdef _WIN32
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#endif
#include <vulkan_core.h>
#include <glfw_control.h>
#include <vulkan_exception.h>
#include <vulkan_prim.h>
#include <filesystem>
#include <iostream>
#include <map>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace macsnet::vulkan;

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;
const bool WINDOW_FULL = false;
const int MAX_FRAMES_IN_FLIGHT = 2;
const bool RESIZE = true;
const uint32_t FCOUNT = 15;
const std::string APPNAME = "Text from glyph map";

class VulkanApp {
	public:
		VulkanApp();
		~VulkanApp();
		void init(bool debug, const std::filesystem::path &path);
		void run();
		void cleanup();

	private:
		struct Vertex {
			glm::vec4 pos;
			glm::vec4 color;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(Vertex),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(Vertex, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(Vertex, color),
				}};
			};
		};

		struct Vertex2 {
			glm::vec4 pos;
			glm::vec2 tex;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(Vertex2),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(Vertex2, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(Vertex2, tex),
				}};
			};
		};

		struct Uniform {
			glm::mat4 model;
			glm::mat4 projection;
			glm::mat4 view;

			static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBindings() {
				return {{{
					.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
				}}};
			}
		};

		struct Uniform2 {
			glm::mat4 model;
			glm::mat4 projection;

			static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBindings() {
				return {{{
					.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
				}}};
			}
		};

		struct GlyphInfo {
			unsigned char x;
			unsigned char y;
			unsigned char width;
		};

		struct FontInfo {
			unsigned char letterHeight;
			unsigned char spaceWidth;
			int spacing;
			unsigned char textureWidth;
			unsigned char textureHeight;
			std::map<char, GlyphInfo> glyphInfos;
		};

		std::vector<Vertex2> createString(const std::string &text);
		void recordCommandBuffer(size_t ind);
		void recordCommandBuffer(size_t ind, const std::vector<std::string> &texts);
		void updateUniformBuffer(size_t ind);
		void fillVerticies();
		void fillColor();
		void printFPS();
		void initPipe();

		std::string m_appName {APPNAME};
		std::unique_ptr<glfw::GLFWControl> m_pWindowControl;
		core::VulkanCore m_core;
		size_t m_vertSize = 0;
		std::unique_ptr<core::Buffers> m_Buffers;
		std::vector<std::unique_ptr<core::Buffers>> m_StringsBuffer;
		std::vector<vk::CommandBuffer> m_cmdBufsP;
		std::vector<vk::CommandBuffer> m_cmdBufsS;
		core::Uniforms m_uniforms;
		core::Uniforms m_uniforms1;
		core::Pipeline m_pipe;
		std::vector<vk::ShaderModule> m_shaders;
		const std::vector<std::string> m_names {"anna", "colin", "james", "danny", "kalin", "hiro", "eddie", "shu", "brian", "tami", "rick", "gene", "natalie", "evan", "sakura", "kai"};
		const FontInfo m_fontInfo {8, 8, -1, 64, 40, {
			{'a', {.x =  0, .y =  0, .width = 8}},
			{'b', {.x =  8, .y =  0, .width = 8}},
			{'c', {.x = 16, .y =  0, .width = 8}},
			{'d', {.x = 24, .y =  0, .width = 8}},
			{'e', {.x = 32, .y =  0, .width = 8}},
			{'f', {.x = 40, .y =  0, .width = 8}},
			{'g', {.x = 48, .y =  0, .width = 8}},
			{'h', {.x = 56, .y =  0, .width = 8}},
			{'i', {.x =  0, .y =  8, .width = 8}},
			{'j', {.x =  8, .y =  8, .width = 8}},
			{'k', {.x = 16, .y =  8, .width = 8}},
			{'l', {.x = 24, .y =  8, .width = 8}},
			{'m', {.x = 32, .y =  8, .width = 8}},
			{'n', {.x = 40, .y =  8, .width = 8}},
			{'o', {.x = 48, .y =  8, .width = 8}},
			{'p', {.x = 56, .y =  8, .width = 8}},
			{'q', {.x =  0, .y = 16, .width = 8}},
			{'r', {.x =  8, .y = 16, .width = 8}},
			{'s', {.x = 16, .y = 16, .width = 8}},
			{'t', {.x = 24, .y = 16, .width = 8}},
			{'u', {.x = 32, .y = 16, .width = 8}},
			{'v', {.x = 40, .y = 16, .width = 8}},
			{'w', {.x = 48, .y = 16, .width = 8}},
			{'x', {.x = 56, .y = 16, .width = 8}},
			{'y', {.x =  0, .y = 24, .width = 8}},
			{'z', {.x =  8, .y = 24, .width = 8}},
			{'0', {.x = 16, .y = 24, .width = 8}},
			{'1', {.x = 24, .y = 24, .width = 8}},
			{'2', {.x = 32, .y = 24, .width = 8}},
			{'3', {.x = 40, .y = 24, .width = 8}},
			{'4', {.x = 48, .y = 24, .width = 8}},
			{'5', {.x = 56, .y = 24, .width = 8}},
			{'6', {.x =  0, .y = 32, .width = 8}},
			{'7', {.x =  8, .y = 32, .width = 8}},
			{'8', {.x = 16, .y = 32, .width = 8}},
			{'9', {.x = 24, .y = 32, .width = 8}},
			{'-', {.x = 32, .y = 32, .width = 8}},
			{'*', {.x = 40, .y = 32, .width = 8}},
			{'!', {.x = 48, .y = 32, .width = 8}},
			{'?', {.x = 56, .y = 32, .width = 8}},
		}};
		vk::RenderPass m_noClearRP;
};

VulkanApp::VulkanApp() : m_core(m_appName), m_uniforms(&m_core), m_uniforms1(&m_core), m_pipe(&m_core)
{
}

VulkanApp::~VulkanApp()
{
}

void VulkanApp::initPipe()
{
	m_pipe.init({
		.vs = m_shaders[0],
		.fs = m_shaders[1],
		.getVertexBindingDescription = Vertex2::getBindingDescription,
		.getVertexAttributeDescription = Vertex2::getAttributeDescription,
		.extent = m_core.getCurrentExtent(),
		.sampleShading = true,
		.blending = true,
		.depthTest = false,
		.msaaFlags = m_core.getMSAAFlags(),
		.uniLayout = &m_uniforms1.getLayout(),
		.textureLayout = &m_core.getTextures()->getDSLayout(),
		.renderPass = m_core.getRenderPass(),
	});
}

void VulkanApp::init(bool debug, const std::filesystem::path &path)
{
	m_pWindowControl = std::make_unique<glfw::GLFWControl>(m_appName);
	m_pWindowControl->init(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FULL, RESIZE);
	auto u = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(Uniform)}});
	auto u1 = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(Uniform2)}});
	m_core.init({
		.pWindowControl = m_pWindowControl.get(),
		.debug = debug,
	   	.vShader = path / "color.vert.spv",
	   	.fShader = path / "color.frag.spv",
	   	.clockwise = false,
	   	.maxFramesInFlight = MAX_FRAMES_IN_FLIGHT,
		.getVertexBindingDescription = Vertex::getBindingDescription,
		.getVertexAttributeDescription = Vertex::getAttributeDescription,
		.getLayoutBindings = Uniform::getLayoutBindings,
		.uniforms = u,
		.textures = {{path / "../../textures/8x8-font.png", false}},
		.msaaFlags = vk::SampleCountFlagBits::e64,
		.sampleShading = true,
		.textureFormat = vk::Format::eR8G8B8A8Unorm,
	});

	primitives::Primitives prim;
	prim.createBigF();
	const auto &f = prim.getVertex();
	std::vector<Vertex> verticies;
	verticies.reserve(f.size());
	std::transform(f.begin(), f.end(), std::back_inserter(verticies), [](auto i) {return Vertex {i.pos, i.color};});
	m_vertSize = verticies.size();

	auto swapCount = m_core.getImageCount();
	m_cmdBufsP = m_core.createCommandBuffer(swapCount);
	m_cmdBufsS = m_core.createCommandBuffer(swapCount, true);
	m_Buffers = m_core.newBuffers();
	m_Buffers->create({
		{verticies.data(), verticies.size() * sizeof(verticies[0]), vk::BufferUsageFlagBits::eVertexBuffer},
	}, true);
	m_StringsBuffer.resize(swapCount);
	m_uniforms.init(Uniform::getLayoutBindings, u, swapCount * (FCOUNT - 1));
	m_uniforms1.init(Uniform2::getLayoutBindings, u1, swapCount * FCOUNT);
	m_shaders = m_core.readShaders({path / "texture.vert.spv", path / "texture.frag.spv"});
	initPipe();
	m_noClearRP = m_core.createRenderPass(false, false, m_core.getSurfaceFormat().format, m_core.findDepthFormat(), m_core.getMSAAFlags());
}

std::vector<VulkanApp::Vertex2> VulkanApp::createString(const std::string &text)
{
	std::vector<Vertex2> v;
	v.reserve(text.size());
	float x = 0;
	const float maxX = m_fontInfo.textureWidth;
	const float maxY = m_fontInfo.textureHeight;
	for (size_t ii = 0; ii < text.size(); ++ii) {
		char letter = text[ii];
		auto glyphInfo = m_fontInfo.glyphInfos.find(letter);
		if (glyphInfo != m_fontInfo.glyphInfos.end()) {
			float x2 = x + glyphInfo->second.width;
			float u1 = glyphInfo->second.x / maxX;
			float v1 = (glyphInfo->second.y + m_fontInfo.letterHeight - 1) / maxY;
			float u2 = (glyphInfo->second.x + glyphInfo->second.width - 1) / maxX;
			float v2 = glyphInfo->second.y / maxY;

			v.push_back({{x, 0.0f, 0.0f, 1.0f}, {u1, v1}});
			v.push_back({{x2, 0.0f, 0.0f, 1.0f}, {u2, v1}});
			v.push_back({{x, -m_fontInfo.letterHeight, 0.0f, 1.0f}, {u1, v2}});
			v.push_back({{x, -m_fontInfo.letterHeight, 0.0f, 1.0f}, {u1, v2}});
			v.push_back({{x2, 0.0f, 0.0f, 1.0f}, {u2, v1}});
			v.push_back({{x2, -m_fontInfo.letterHeight, 0.0f, 1.0f}, {u2, v2}});

			x += glyphInfo->second.width + m_fontInfo.spacing;
		} else {
			x += m_fontInfo.spaceWidth;
		}
	}
	return v;
}

void VulkanApp::recordCommandBuffer(size_t ind)
{
	vk::CommandBufferInheritanceInfo ii {
		.renderPass = m_core.getRenderPass(),
	};
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse | vk::CommandBufferUsageFlagBits::eRenderPassContinue,
		.pInheritanceInfo = &ii,
	};
	m_cmdBufsS[ind].begin(beginInfo);
	m_cmdBufsS[ind].bindPipeline(vk::PipelineBindPoint::eGraphics, m_core.getPipeline());
	m_cmdBufsS[ind].bindVertexBuffers(0, {m_Buffers->getBuffer(0)}, {0});
	for (size_t i = 0; i < FCOUNT; ++i) {
		m_cmdBufsS[ind].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_core.getPipelineLayout(), 0, i ? m_uniforms.getDescSets(ind * (FCOUNT - 1) + i - 1) : m_core.getDescSets(ind, false), {}); 
		m_cmdBufsS[ind].draw(m_vertSize, 1, 0, 0);
	}
	m_cmdBufsS[ind].end();
}

void VulkanApp::recordCommandBuffer(size_t ind, const std::vector<std::string> &texts)
{
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse,
	};
	m_cmdBufsP[ind].begin(beginInfo);
	auto cv = m_core.getClearValue({0.9f, 0.9f, 0.9f, 1.0f});
	vk::RenderPassBeginInfo renderPassInfo {
		.renderPass = m_core.getRenderPass(),
		.framebuffer = m_core.getFb(ind),
		.renderArea {{0, 0}, m_core.getCurrentExtent()},
		.clearValueCount = static_cast<uint32_t>(cv.size()),
		.pClearValues = cv.data(),
	};
	vk::RenderPassBeginInfo renderPassInfo1 {
		.renderPass = m_noClearRP,
		.framebuffer = m_core.getFb(ind),
		.renderArea {{0, 0}, m_core.getCurrentExtent()},
	};

	m_cmdBufsP[ind].beginRenderPass(renderPassInfo, vk::SubpassContents::eSecondaryCommandBuffers);
	m_cmdBufsP[ind].executeCommands(m_cmdBufsS[ind]);
	m_cmdBufsP[ind].endRenderPass();
	m_cmdBufsP[ind].beginRenderPass(renderPassInfo1, vk::SubpassContents::eInline);
	m_cmdBufsP[ind].bindPipeline(vk::PipelineBindPoint::eGraphics, m_pipe.getPipeline());
	if (m_StringsBuffer[ind]) {
		m_StringsBuffer[ind]->destroy();

	}
	std::vector<std::tuple<const void *, size_t, vk::BufferUsageFlags>> data;
	std::vector<std::vector<VulkanApp::Vertex2>> vs;
	data.reserve(texts.size());
	vs.reserve(texts.size());
	for (const auto &t : texts) {
		vs.push_back(createString(t));
		data.emplace_back(vs.rbegin()->data(), vs.rbegin()->size() * sizeof(vs.rbegin()->at(0)), vk::BufferUsageFlagBits::eVertexBuffer);
	}
	m_StringsBuffer[ind] = m_core.newBuffers();
	m_StringsBuffer[ind]->create(data, true);
	for (size_t i = 0; i < FCOUNT; ++i) {
		m_cmdBufsP[ind].bindVertexBuffers(0, {m_StringsBuffer[ind]->getBuffer(i)}, {0});
		auto ds = m_uniforms1.getDescSets(ind * FCOUNT + i);
		ds.push_back(m_core.getTextures()->getDecriptorSet());
		m_cmdBufsP[ind].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_pipe.getPipelineLayout(), 0, ds, {}); 
		m_cmdBufsP[ind].draw(vs[i].size(), 1, 0, 0);
	}
	m_cmdBufsP[ind].endRenderPass();

	m_cmdBufsP[ind].end();
}

void VulkanApp::printFPS()
{
	static auto fpsStartTime = std::chrono::high_resolution_clock::now();
	auto currentTime = std::chrono::high_resolution_clock::now();
	size_t t = std::chrono::duration<size_t, std::chrono::seconds::period>(std::chrono::duration_cast<std::chrono::seconds>(currentTime - fpsStartTime)).count();
	static size_t frames = 0;
	static bool printed = false;
	if (t && t % 10 == 0) {
		if (!printed) {
			printed = true;
			std::cout << "fps: " << frames / t << std::endl;
			frames = 0;
			fpsStartTime = std::chrono::high_resolution_clock::now();
		}
	} else {
		printed = false;
	}
	++frames;
}

void VulkanApp::updateUniformBuffer(size_t ind)
{
	static const glm::vec3 translation {0.0f, 30.0f, 0.0f};
	static glm::vec3 rotation {glm::radians(190.0f), 0.0f, 0.0f};
	static const glm::vec3 scale {1.0f, 1.0f, 1.0f};
	static const float fieldOfViewRadians = glm::radians(60.0f);
	static const float rotationSpeed = 1.2f;
	static const auto start = std::chrono::high_resolution_clock::now();
	static auto then = std::chrono::high_resolution_clock::now();
	static const float cameraRadius = 360.0f;
	static const float zNear = 1.0f;
	static const float zFar = 2000.0f;
	static const glm::vec3 target {0.0f, 0.0f, 0.0f};
	static const glm::vec3 up {0.0f, 1.0f, 0.0f};
	static const float spread = 170.0f;

	printFPS();
	auto [w, h] = m_core.getCurrentExtent();

	auto now = std::chrono::high_resolution_clock::now();
	float deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(now - then).count() * 0.001f;
	float sinceEpoch = std::chrono::duration_cast<std::chrono::milliseconds>(now - start).count() * 0.001f;
	then = now;
	rotation[1] += rotationSpeed * deltaTime;

	Uniform uni;
	uni.projection = glm::perspectiveFov(fieldOfViewRadians, static_cast<float>(w), static_cast<float>(h), zNear, zFar);

	glm::vec3 cameraPosition {cos(sinceEpoch) * cameraRadius, 0, sin(sinceEpoch) * cameraRadius};
	uni.view = glm::lookAt(cameraPosition, target, up);

	std::vector<glm::vec3> textPositions;
	size_t i = 0;
	for (int yy = -1; yy <= 1; ++yy) {
		for (int xx = -2; xx <= 2; ++xx, ++i) {
			uni.model = glm::translate(glm::identity<glm::mat4>(), {translation[0] + xx * spread, translation[1] + yy * spread, translation[2]});
			uni.model = glm::rotate(uni.model, rotation[0], {1.0f, 0.0f, 0.0f});
			uni.model = glm::rotate(uni.model, rotation[1] + yy * xx * 0.2f, {0.0f, 1.0f, 0.0f});
			uni.model = glm::rotate(uni.model, rotation[2] + sinceEpoch + (yy * 3 + xx) * 0.1f, {0.0f, 0.0f, 1.0f});
			uni.model = glm::scale(uni.model, scale);
			uni.model = glm::translate(uni.model, {-50.0f, -75.0f, 0.0f});
			textPositions.push_back({glm::value_ptr(uni.model)[12], glm::value_ptr(uni.model)[13], glm::value_ptr(uni.model)[14]});
			if (i) {
				m_uniforms.updateUniform(&uni, sizeof(uni), ind * (FCOUNT - 1) + i - 1, 0);
			} else {
				m_core.updateUniform(&uni, sizeof(uni), ind, 0);
			}
		}
	}

	std::vector<std::string> t;
	t.reserve(textPositions.size());
	for (size_t i = 0; i < textPositions.size(); ++i) {
		std::stringstream s;
		auto pos = textPositions[i];
		s << m_names[i] << ':' << static_cast<int>(pos[0]) << "," << static_cast<int>(pos[1]) << "," << static_cast<int>(pos[2]);
		t.push_back(s.str());
	}
	recordCommandBuffer(ind, t);
	Uniform2 uni2;
	uni2.projection = uni.projection;
	for (size_t i = 0; i < textPositions.size(); ++i) {
		glm::vec3 tp = uni.view * glm::vec4(textPositions[i], 1.0f);
		auto fromEye = glm::normalize(tp);
		float amountToMoveTowardEye = 150.0f;
		float viewX = tp[0] - fromEye[0] * amountToMoveTowardEye;
		float viewY = tp[1] - fromEye[1] * amountToMoveTowardEye;
		float viewZ = tp[2] - fromEye[2] * amountToMoveTowardEye;
		float desiredScale = -1.0f / h;
		float scale = viewZ * desiredScale * 2.0f;
		uni2.model = glm::translate(glm::identity<glm::mat4>(), {viewX, viewY, viewZ});
		uni2.model = glm::scale(uni2.model, {scale, scale, 1.0f});
		m_uniforms1.updateUniform(&uni2, sizeof(uni2), ind * FCOUNT + i, 0);
	}

	m_core.renderScene(m_cmdBufsP[ind], ind);
}

void VulkanApp::cleanup()
{
	m_core.waitIdle();
	if (m_Buffers) {
		m_Buffers->destroy();
	}
	for (auto &sb : m_StringsBuffer) {
		if (sb) {
			sb->destroy();
		}
	}
	m_uniforms.destroy();
	m_uniforms1.destroy();
	m_pipe.destroy(true);
	m_core.destroyCommandBuffer(m_cmdBufsP);
	m_core.destroyCommandBuffer(m_cmdBufsS);
	m_core.destroyShaders(m_shaders);
	m_core.destroyRenderPass(m_noClearRP);
	m_core.cleanup();
	m_pWindowControl->cleanup();
}

void VulkanApp::run()
{
	auto func = [&](bool recreate) {
		if (recreate) {
			m_core.recreateSwapChain();
			m_pipe.destroyPipe();
			initPipe();
		}
		for (uint32_t i = 0; i < m_cmdBufsS.size(); ++i) {
			recordCommandBuffer(i);
		}
	};
	func(false);
	while (!m_pWindowControl->shouldClose()) {
		m_pWindowControl->pollEvents();
		if (m_pWindowControl->wasResized()) {
			func(true);
		}
		try {
			auto ind = m_core.getImageIndex();
			updateUniformBuffer(ind);
		} catch (vk::OutOfDateKHRError &) {
			func(true);
		} catch (exception::VulkanException &e) {
			if (e.isSubopt()) {
				func(true);
			} else {
				throw;
			}
		}
	}
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	auto argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#else
int main(int argc, const char *argv[])
{
#endif
	auto ret = EXIT_SUCCESS;
	VulkanApp app;
	try {
		app.init(DBG, std::filesystem::path(argv[0]).parent_path());
		app.run();
	} catch (exception::VulkanException &e) {
		std::cout << e.what() << std::endl;
		ret = EXIT_FAILURE;
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		ret = EXIT_FAILURE;
	}
	app.cleanup();
	return ret;
}

