#version 450

layout(location = 0) in vec4 a_position;
layout(location = 1) in vec2 a_texcoord;
layout(binding = 0) uniform U {
	mat4 u_model;
	mat4 u_proj;
} u;
layout(location = 0) out vec2 v_texcoord;

void main() {
	gl_Position = u.u_proj * u.u_model * a_position;
	v_texcoord = a_texcoord;
}

