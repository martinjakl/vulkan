#ifdef _WIN32
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#endif
#include <vulkan_core.h>
#include <glfw_control.h>
#include <memory>
#include <iostream>
#include <sstream>
#include <vulkan_exception.h>
#include <filesystem>

using macsnet::vulkan::glfw::GLFWControl;
using macsnet::vulkan::core::VulkanCore;
using macsnet::vulkan::exception::VulkanException;

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;
const int MAX_FRAMES_IN_FLIGHT = 2;
const std::string APPNAME = "Trangle OGLdev";

class VulkanApp {
	public:
		VulkanApp(const std::string &pAppName);
		~VulkanApp();
		void init(bool debug, const std::filesystem::path &p);
		void run();
	private:
		void recordCommandBuffer(const vk::CommandBuffer &buf, uint32_t ind);

		std::string m_appName;
		std::unique_ptr<GLFWControl> m_pWindowControl;
		VulkanCore m_core;
		std::vector<vk::CommandBuffer> m_cmdBufs;
};

VulkanApp::VulkanApp(const std::string &pAppName) : m_appName(pAppName), m_core(pAppName)
{
}

VulkanApp::~VulkanApp()
{
}

void VulkanApp::init(bool debug, const std::filesystem::path &p)
{
	m_pWindowControl = std::make_unique<GLFWControl>(m_appName);
	m_pWindowControl->init(WINDOW_WIDTH, WINDOW_HEIGHT, false, false);
	m_core.init(m_pWindowControl.get(), debug, p / "simple.vert.spv", p / "simple.frag.spv", false, MAX_FRAMES_IN_FLIGHT);
	m_cmdBufs = m_core.createCommandBuffer(m_core.getImageCount());
}

void VulkanApp::recordCommandBuffer(const vk::CommandBuffer &buf, uint32_t ind)
{
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse,
	};
	auto clearValue = m_core.getClearValue({164.0f / 256.0f, 30.0f / 256.0f, 34.0f / 256.0f, 0.0f});

	vk::RenderPassBeginInfo renderPassInfo {
		.renderPass = m_core.getRenderPass(),
		.framebuffer = m_core.getFb(ind),
		.renderArea {{0, 0}, {WINDOW_WIDTH, WINDOW_HEIGHT}},
		.clearValueCount = static_cast<uint32_t>(clearValue.size()),
		.pClearValues = clearValue.data(),
	};

	buf.begin(beginInfo);

	buf.beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
	buf.bindPipeline(vk::PipelineBindPoint::eGraphics, m_core.getPipeline());
	buf.draw(3, 1, 0, 0);
	buf.endRenderPass();

	buf.end();
}

void VulkanApp::run()
{
	for (uint32_t i = 0; i < m_cmdBufs.size(); ++i) {
		recordCommandBuffer(m_cmdBufs[i], i);
	}
	while (!m_pWindowControl->shouldClose()) {
		m_pWindowControl->pollEvents();
		auto ind = m_core.getImageIndex();
		m_core.renderScene(m_cmdBufs[ind], ind);
	}
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	auto argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#else
int main(int argc, const char *argv[])
{
#endif
	try {
		VulkanApp app(APPNAME );
		app.init(DBG, std::filesystem::path(argv[0]).parent_path());
		app.run();
	} catch (VulkanException &e) {
		std::cout << e.what() << std::endl;
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}
	return 0;
}

