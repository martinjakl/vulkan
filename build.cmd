@echo off

set FREETYPE_DIR=..\..\freetype-windows-binaries
set ICU_ROOT=..\..\icu
set BT=Release
if NOT "%~1" == "" (
	set BT=%1
)

if not exist build\ (
	md build
	cmake -S "." -B "build" 
)
cmake --build "build" --config %BT% -j 32
for /D %%d in (build\???-*) do (
	copy %%d\%BT% %%d
)
copy build\common\%BT%\*.dll libs
