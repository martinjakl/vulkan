#ifdef _WIN32
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#endif
#include <vulkan_core.h>
#include <glfw_control.h>
#include <memory>
#include <iostream>
#include <vulkan_exception.h>
#include <cstdlib>
#include <filesystem>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <chrono>
#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

using macsnet::vulkan::glfw::GLFWControl;
using macsnet::vulkan::core::VulkanCore;
using macsnet::vulkan::core::Buffers;
using macsnet::vulkan::core::CoreInit;
using macsnet::vulkan::exception::VulkanException;

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;
const bool WINDOW_FULL = false;
const int MAX_FRAMES_IN_FLIGHT = 2;
const bool RESIZE = true;
const std::string APPNAME = "Loading models";

class VulkanApp {
	public:
		VulkanApp();
		~VulkanApp();
		void init(bool debug, const std::filesystem::path &path);
		void run();
		void cleanup();
	private:
		struct Vertex {
			glm::vec3 pos;
			glm::vec2 texCoord;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(Vertex),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32B32Sfloat,
					.offset = offsetof(Vertex, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(Vertex, texCoord),
				}};
			};
		};

		struct Uniform {
			glm::mat4 model;
			glm::mat4 view;
			glm::mat4 proj;

			static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBinding() {
				return {{{
					.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
				}}};
			}
		};

		void recordCommandBuffer(const vk::CommandBuffer &buf, uint32_t ind);
		void updateUniformBuffer(uint32_t ind);
		void loadModel(const std::filesystem::path &path);

		std::string m_appName {APPNAME};
		std::unique_ptr<GLFWControl> m_pWindowControl;
		VulkanCore m_core;
		std::vector<vk::CommandBuffer> m_cmdBufs;
		std::vector<Vertex> verticies;
		std::vector<uint32_t> indicies;
		std::unique_ptr<Buffers> m_Buffers;
};

VulkanApp::VulkanApp() : m_core(m_appName)
{
}

VulkanApp::~VulkanApp()
{
}

void VulkanApp::init(bool debug, const std::filesystem::path &path)
{
	loadModel(path);
	m_pWindowControl = std::make_unique<GLFWControl>(m_appName);
	m_pWindowControl->init(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FULL, RESIZE);
	m_core.init(CoreInit {
		.pWindowControl = m_pWindowControl.get(),
		.debug = debug,
	   	.vShader = path / "simple.vert.spv",
	   	.fShader = path / "simple.frag.spv",
	   	.clockwise = false,
	   	.maxFramesInFlight = MAX_FRAMES_IN_FLIGHT,
		.getVertexBindingDescription = Vertex::getBindingDescription,
		.getVertexAttributeDescription = Vertex::getAttributeDescription,
		.getLayoutBindings = Uniform::getLayoutBinding,
		.uniforms = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(Uniform)}}),
		.textures = {{path / "../../textures/viking_room.png", false}},
		.preferedFormat = vk::Format::eB8G8R8A8Srgb,
		.textureFormat = vk::Format::eR8G8B8A8Srgb,
	});
	auto swapCount = m_core.getImageCount();
	m_cmdBufs = m_core.createCommandBuffer(swapCount);
	m_Buffers = m_core.newBuffers();
	m_Buffers->create({
		{verticies.data(), verticies.size() * sizeof(verticies[0]), vk::BufferUsageFlagBits::eVertexBuffer},
		{indicies.data(), indicies.size() * sizeof(indicies[0]), vk::BufferUsageFlagBits::eIndexBuffer}
	}, true);
}

void VulkanApp::recordCommandBuffer(const vk::CommandBuffer &buf, uint32_t ind)
{
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse,
	};

	auto clearValue = m_core.getClearValue({0.0f, 0.0f, 0.0f, 1.0f});

	vk::RenderPassBeginInfo renderPassInfo {
		.renderPass = m_core.getRenderPass(),
		.framebuffer = m_core.getFb(ind),
		.renderArea {{0, 0}, m_core.getCurrentExtent()},
		.clearValueCount = static_cast<uint32_t>(clearValue.size()),
		.pClearValues = clearValue.data(),
	};

	buf.begin(beginInfo);

	buf.beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
	buf.bindPipeline(vk::PipelineBindPoint::eGraphics, m_core.getPipeline());
	buf.bindVertexBuffers(0, {m_Buffers->getBuffer(0)}, {0});
	buf.bindIndexBuffer(m_Buffers->getBuffer(1), 0, vk::IndexType::eUint32);
	buf.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_core.getPipelineLayout(), 0, m_core.getDescSets(ind), {}); 
	buf.drawIndexed(indicies.size(), 1, 0, 0, 0);
	buf.endRenderPass();

	buf.end();
}

void VulkanApp::updateUniformBuffer(uint32_t ind)
{
	static auto fpsStartTime = std::chrono::high_resolution_clock::now();
	static size_t frames = 0;
	static bool printed = false;
	auto currentTime = std::chrono::high_resolution_clock::now();
	size_t t = std::chrono::duration<size_t, std::chrono::seconds::period>(std::chrono::duration_cast<std::chrono::seconds>(currentTime - fpsStartTime)).count();
	if (t && t % 10 == 0) {
		if (!printed) {
			printed = true;
			std::cout << "fps: " << frames / t << std::endl;
			frames = 0;
			fpsStartTime = std::chrono::high_resolution_clock::now();
		}
	} else {
		printed = false;
	}
	auto extent = m_core.getCurrentExtent();
	Uniform ubo {
		.model = glm::rotate(glm::mat4(1.0f), 0 * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
		.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
		.proj = glm::perspectiveFov(glm::radians(45.0f), static_cast<float>(extent.width), static_cast<float>(extent.height), 0.1f, 10.0f),
	};
	ubo.proj[1][1] *= -1.0f;
	m_core.updateUniform(&ubo, sizeof(ubo), ind, 0);
	++frames;
}

void VulkanApp::loadModel(const std::filesystem::path &path)
{
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warn, err;

	std::ifstream ifs;
	ifs.open(path / "../../meshes/viking_room.obj");
	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, &ifs)) {
		throw VulkanException(err.c_str());
	}

	for (const auto &shape : shapes) {
		std::map<std::tuple<int, int>, uint32_t> usedPositions;
		for (const auto &index : shape.mesh.indices) {
			auto t = std::make_tuple(index.vertex_index, index.texcoord_index);
			if (auto f = usedPositions.find(t); f == usedPositions.end()) {
				usedPositions.emplace(t, static_cast<uint32_t>(verticies.size()));
				indicies.push_back(verticies.size());
				verticies.push_back({
					.pos = {
						attrib.vertices[3 * index.vertex_index + 0],
						attrib.vertices[3 * index.vertex_index + 1],
						attrib.vertices[3 * index.vertex_index + 2],
					},
					.texCoord {
						attrib.texcoords[2 * index.texcoord_index + 0],
						1.0 - attrib.texcoords[2 * index.texcoord_index + 1],
					},
				});
			} else {
				indicies.push_back(f->second);
			}
		}
	}
}

void VulkanApp::cleanup()
{
	m_core.waitIdle();
	if (m_Buffers) {
		m_Buffers->destroy();
	}
	m_core.destroyCommandBuffer(m_cmdBufs);
	m_core.cleanup();
	m_pWindowControl->cleanup();
}

void VulkanApp::run()
{
	auto func = [&](bool recreate) {
		if (recreate) {
			m_core.recreateSwapChain();
		}
		for (uint32_t i = 0; i < m_cmdBufs.size(); ++i) {
			recordCommandBuffer(m_cmdBufs[i], i);
		}
	};
	func(false);
	while (!m_pWindowControl->shouldClose()) {
		m_pWindowControl->pollEvents();
		if (m_pWindowControl->wasResized()) {
			func(true);
		}
		try {
			auto ind = m_core.getImageIndex();
			updateUniformBuffer(ind);
			m_core.renderScene(m_cmdBufs[ind], ind);
		} catch (VulkanException &e) {
			if (e.isSubopt()) {
				func(true);
			} else {
				throw;
			}
		} catch (vk::OutOfDateKHRError &) {
			func(true);
		}
	}
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	auto argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#else
int main(int argc, const char *argv[])
{
#endif
	auto ret = EXIT_SUCCESS;
	VulkanApp app;
	try {
		app.init(DBG, std::filesystem::path(argv[0]).parent_path());
		app.run();
	} catch (VulkanException &e) {
		std::cout << e.what() << std::endl;
		ret = EXIT_FAILURE;
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		ret =  EXIT_FAILURE;
	}
	app.cleanup();
	return ret;
}

