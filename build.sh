#!/bin/bash

if [ ! -d "build" ]
then
	mkdir "build"
	BT="Debug"
	if [ -n "$1" ]
	then
		BT=$1
	fi
	cmake -D CMAKE_EXPORT_COMPILE_COMMANDS=TRUE -D CMAKE_BUILD_TYPE=$BT -D CMAKE_VERBOSE_MAKEFILE=TRUE -S "." -B "build" 
	ln -sfn build/compile_commands.json
fi
cmake --build "build" -j `grep processor /proc/cpuinfo | wc -l`

