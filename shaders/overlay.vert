#version 450

layout(location = 0) in vec2 a_position;
layout(location = 1) in vec2 a_textCoord;
layout(set = 0, binding = 0) uniform Uniform {
	vec2 u_resolution;
	vec2 u_size;
} ubo;
layout(location = 0) out vec2 v_textCoord;

void main() {
	vec2 zeroToOne = a_position * ubo.u_size / ubo.u_resolution;
	vec2 clipSpace = zeroToOne - 1.0;
	gl_Position = vec4(clipSpace * vec2(1, 1), 0, 1);
	v_textCoord = a_textCoord;
}

