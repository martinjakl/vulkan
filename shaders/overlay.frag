#version 450

layout(set = 1, binding = 0) uniform sampler2D u_image;
layout(location = 0) in vec2 v_textCoord;
layout(location = 0) out vec4 outColor;

void main() {
	outColor = texture(u_image, v_textCoord);
}

