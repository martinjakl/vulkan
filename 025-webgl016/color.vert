#version 450

layout(location = 0) in vec4 a_position;
layout(location = 1) in vec4 a_color;
layout(binding = 0) uniform U {
	mat4 u_model;
	mat4 u_proj;
	mat4 u_view;
} u;
layout(location = 0) out vec4 v_color;

void main() {
	gl_Position = u.u_proj * u.u_view * u.u_model * a_position;
	v_color = a_color;
}

