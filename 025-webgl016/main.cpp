#ifdef _WIN32
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#endif
#include <vulkan_core.h>
#include <glfw_control.h>
#include <vulkan_exception.h>
#include <vulkan_prim.h>
#include <filesystem>
#include <iostream>
#include <numeric>
#include <fstream>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H

using namespace macsnet::vulkan;

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;
const bool WINDOW_FULL = false;
const int MAX_FRAMES_IN_FLIGHT = 2;
const bool RESIZE = true;
const uint32_t FCOUNT = 15;
const std::string APPNAME = "Text textures individual letters (slow)";

class Texture {
	public:
		Texture(const std::filesystem::path &font, char l, uint32_t w, uint32_t h);
		~Texture();
		const void *getData() const;
		size_t getSize() const;
		uint32_t getWidth() const;
		uint32_t getHeight() const;
	private:
		std::unique_ptr<unsigned char[]> data;
		size_t size = 0;
		uint32_t w = 0;
		uint32_t h = 0;
};

Texture::Texture(const std::filesystem::path &font, char l, uint32_t w, uint32_t h) : w(w), h(h)
{
	size = w * h * 4;
	data = std::make_unique<unsigned char[]>(size);
	for (size_t i = 0; i < h; ++i) {
		for (size_t j = 0; j < w ; ++j) {
			auto pos = (i * w + j) * 4;
			data[pos + 0] = 255;
			data[pos + 1] = 255;
			data[pos + 2] = 255;
			data[pos + 3] = 0;
		}
	}
	FT_Library ftlib;
	FT_Face face;
	FT_Init_FreeType(&ftlib);
	std::ifstream ifs;
	ifs.open(font, std::ios::binary | std::ios::ate);
	std::vector<char> d(ifs.tellg());
	ifs.seekg(0);
	ifs.read(d.data(), d.size());
	FT_New_Memory_Face(ftlib, reinterpret_cast<FT_Byte *>(d.data()), d.size(), 0, &face);
	FT_Set_Char_Size(face, 0, 46 * 64, 0, h);
	FT_Load_Char(face, l, FT_LOAD_RENDER);

	for (size_t i = 0; i < face->glyph->bitmap.rows; ++i) {
		for (size_t j = 0; j < face->glyph->bitmap.width; ++j) {
			std::array<unsigned char, 3> textColor {0, 0, 0};
			float alfa = static_cast<float>(face->glyph->bitmap.buffer[i * face->glyph->bitmap.width + j]) / 255.0f;
			auto pos = ((h / 2 - face->glyph->bitmap_top + i) * w + j) * 4;
			for (size_t i = 0; i < 3; ++i) {
				data[pos + i] = static_cast<unsigned char>(alfa * textColor[i] + (1 - alfa) * data[pos + i]);
			}
			data[pos + 3] = alfa ? 255 : 0;
		}
	}

	FT_Done_Face(face);
	FT_Done_FreeType(ftlib);
}

Texture::~Texture()
{
}

const void *Texture::getData() const
{
	return data.get();
}

size_t Texture::getSize() const
{
	return size;
}

uint32_t Texture::getWidth() const
{
	return w;
}

uint32_t Texture::getHeight() const
{
	return h;
}

class VulkanApp {
	public:
		VulkanApp();
		~VulkanApp();
		void init(bool debug, const std::filesystem::path &path);
		void run();
		void cleanup();

	private:
		struct Vertex {
			glm::vec4 pos;
			glm::vec4 color;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(Vertex),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(Vertex, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(Vertex, color),
				}};
			};
		};

		struct Vertex2 {
			glm::vec4 pos;
			glm::vec2 tex;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(Vertex2),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(Vertex2, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(Vertex2, tex),
				}};
			};
		};

		struct Uniform {
			glm::mat4 model;
			glm::mat4 projection;
			glm::mat4 view;

			static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBindings() {
				return {{{
					.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
				}}};
			}
		};

		struct Uniform2 {
			glm::mat4 model;
			glm::mat4 projection;

			static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBindings() {
				return {{{
					.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
				}}};
			}
		};

		void recordCommandBuffer(size_t ind);
		void updateUniformBuffer(size_t ind);
		void fillVerticies();
		void fillColor();
		void printFPS();
		void createTextures(const std::filesystem::path &path);
		void initPipe();

		std::string m_appName {APPNAME};
		std::unique_ptr<glfw::GLFWControl> m_pWindowControl;
		core::VulkanCore m_core;
		size_t m_vertSize = 0;
		size_t m_vertSize2 = 0;
		std::unique_ptr<core::Buffers> m_Buffers;
		std::vector<vk::CommandBuffer> m_cmdBufs;
		core::Uniforms m_uniforms;
		core::Uniforms m_uniforms1;
		core::Pipeline m_pipe;
		std::vector<core::Textures> m_textures;
		std::vector<vk::ShaderModule> m_shaders;
		const std::vector<std::string> m_names {"anna", "colin", "james", "danny", "kalin", "hiro", "eddie", "shu", "brian", "tami", "rick", "gene", "natalie", "evan", "sakura", "kai"};
		size_t m_uni1count = 0;
};

VulkanApp::VulkanApp() : m_core(m_appName), m_uniforms(&m_core), m_uniforms1(&m_core), m_pipe(&m_core)
{
}

VulkanApp::~VulkanApp()
{
}

void VulkanApp::createTextures(const std::filesystem::path &path)
{
	std::vector<std::shared_ptr<Texture>> textures;
	for (char i = 'a'; i <= 'z'; ++i) {
		textures.push_back(std::make_shared<Texture>(path / "../../fonts/SourceCodePro-Black.ttf", i, 100, 260));
	}
	m_textures.reserve(textures.size());
	for (const auto &it : textures) {
		m_textures.emplace_back(&m_core);
		m_textures.rbegin()->create({{it->getData(), it->getSize(), it->getWidth(), it->getHeight(), true, false}}, vk::Format::eR8G8B8A8Unorm);
	}
}

void VulkanApp::initPipe()
{
	m_pipe.init({
		.vs = m_shaders[0],
		.fs = m_shaders[1],
		.getVertexBindingDescription = Vertex2::getBindingDescription,
		.getVertexAttributeDescription = Vertex2::getAttributeDescription,
		.extent = m_core.getCurrentExtent(),
		.sampleShading = true,
		.blending = true,
		.depthTest = false,
		.msaaFlags = m_core.getMSAAFlags(),
		.uniLayout = &m_uniforms1.getLayout(),
		.textureLayout = &m_textures[0].getDSLayout(),
		.renderPass = m_core.getRenderPass()
	});
}

void VulkanApp::init(bool debug, const std::filesystem::path &path)
{
	m_pWindowControl = std::make_unique<glfw::GLFWControl>(m_appName);
	m_pWindowControl->init(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FULL, RESIZE);
	auto u = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(Uniform)}});
	auto u1 = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(Uniform2)}});
	m_core.init({
		.pWindowControl = m_pWindowControl.get(),
		.debug = debug,
	   	.vShader = path / "color.vert.spv",
	   	.fShader = path / "color.frag.spv",
	   	.clockwise = false,
	   	.maxFramesInFlight = MAX_FRAMES_IN_FLIGHT,
		.getVertexBindingDescription = Vertex::getBindingDescription,
		.getVertexAttributeDescription = Vertex::getAttributeDescription,
		.getLayoutBindings = Uniform::getLayoutBindings,
		.uniforms = u,
		.msaaFlags = vk::SampleCountFlagBits::e64,
		.sampleShading = true,
	});

	primitives::Primitives prim;
	prim.createBigF();
	const auto &f = prim.getVertex();
	std::vector<Vertex> verticies;
	verticies.reserve(f.size());
	std::transform(f.begin(), f.end(), std::back_inserter(verticies), [](auto i) {return Vertex {i.pos, i.color};});
	m_vertSize = verticies.size();

	primitives::Primitives prim1;
	prim1.createQuad(1.0f);
	const auto &f1 = prim1.getVertex();
	std::vector<Vertex2> verticies1;
	verticies1.reserve(f1.size());
	std::transform(f1.begin(), f1.end(), std::back_inserter(verticies1), [](auto i) {return Vertex2 {i.pos, i.texcoord};});
	m_vertSize2 = verticies1.size();

	auto swapCount = m_core.getImageCount();
	m_cmdBufs = m_core.createCommandBuffer(swapCount);
	m_Buffers = m_core.newBuffers();
	m_Buffers->create({
		{verticies.data(), verticies.size() * sizeof(verticies[0]), vk::BufferUsageFlagBits::eVertexBuffer},
		{verticies1.data(), verticies1.size() * sizeof(verticies1[0]), vk::BufferUsageFlagBits::eVertexBuffer},
	}, true);
	m_uniforms.init(Uniform::getLayoutBindings, u, swapCount * (FCOUNT - 1));
	m_uni1count = std::accumulate(m_names.begin(), m_names.end(), static_cast<size_t>(0), [](const size_t &val, const std::string &s) {return val + s.size();});
	m_uniforms1.init(Uniform2::getLayoutBindings, u1, swapCount * m_uni1count);
	createTextures(path);
	m_shaders = m_core.readShaders({path / "texture.vert.spv", path / "texture.frag.spv"});
	initPipe();
}

void VulkanApp::recordCommandBuffer(size_t ind)
{
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse,
	};
	m_cmdBufs[ind].begin(beginInfo);
	auto cv = m_core.getClearValue({0.9f, 0.9f, 0.9f, 1.0f});
	vk::RenderPassBeginInfo renderPassInfo {
		.renderPass = m_core.getRenderPass(),
		.framebuffer = m_core.getFb(ind),
		.renderArea {{0, 0}, m_core.getCurrentExtent()},
		.clearValueCount = static_cast<uint32_t>(cv.size()),
		.pClearValues = cv.data(),
	};

	m_cmdBufs[ind].beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
	m_cmdBufs[ind].bindPipeline(vk::PipelineBindPoint::eGraphics, m_core.getPipeline());
	m_cmdBufs[ind].bindVertexBuffers(0, {m_Buffers->getBuffer(0)}, {0});
	for (size_t i = 0; i < FCOUNT; ++i) {
		m_cmdBufs[ind].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_core.getPipelineLayout(), 0, i ? m_uniforms.getDescSets(ind * (FCOUNT - 1) + i - 1) : m_core.getDescSets(ind), {}); 
		m_cmdBufs[ind].draw(m_vertSize, 1, 0, 0);
	}
	m_cmdBufs[ind].bindPipeline(vk::PipelineBindPoint::eGraphics, m_pipe.getPipeline());
	m_cmdBufs[ind].bindVertexBuffers(0, {m_Buffers->getBuffer(1)}, {0});
	size_t i = 0;
	for (const auto &name : m_names) {
		for (char letter [[maybe_unused]] : name) {
			auto ds = m_uniforms1.getDescSets(ind * m_uni1count + (i++));
			ds.push_back(m_textures[letter - 'a'].getDecriptorSet());
			m_cmdBufs[ind].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_pipe.getPipelineLayout(), 0, ds, {}); 
			m_cmdBufs[ind].draw(m_vertSize2, 1, 0, 0);
		}
	}
	m_cmdBufs[ind].endRenderPass();

	m_cmdBufs[ind].end();
}

void VulkanApp::printFPS()
{
	static auto fpsStartTime = std::chrono::high_resolution_clock::now();
	auto currentTime = std::chrono::high_resolution_clock::now();
	size_t t = std::chrono::duration<size_t, std::chrono::seconds::period>(std::chrono::duration_cast<std::chrono::seconds>(currentTime - fpsStartTime)).count();
	static size_t frames = 0;
	static bool printed = false;
	if (t && t % 10 == 0) {
		if (!printed) {
			printed = true;
			std::cout << "fps: " << frames / t << std::endl;
			frames = 0;
			fpsStartTime = std::chrono::high_resolution_clock::now();
		}
	} else {
		printed = false;
	}
	++frames;
}

void VulkanApp::updateUniformBuffer(size_t ind)
{
	static const glm::vec3 translation {0.0f, 30.0f, 0.0f};
	static glm::vec3 rotation {glm::radians(190.0f), 0.0f, 0.0f};
	static const glm::vec3 scale {1.0f, 1.0f, 1.0f};
	static const float fieldOfViewRadians = glm::radians(60.0f);
	static const float rotationSpeed = 1.2f;
	static const auto start = std::chrono::high_resolution_clock::now();
	static auto then = std::chrono::high_resolution_clock::now();
	static const float cameraRadius = 360.0f;
	static const float zNear = 1.0f;
	static const float zFar = 2000.0f;
	static const glm::vec3 target {0.0f, 0.0f, 0.0f};
	static const glm::vec3 up {0.0f, 1.0f, 0.0f};
	static const float spread = 170.0f;

	printFPS();
	auto [w, h] = m_core.getCurrentExtent();

	auto now = std::chrono::high_resolution_clock::now();
	float deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(now - then).count() * 0.001f;
	float sinceEpoch = std::chrono::duration_cast<std::chrono::milliseconds>(now - start).count() * 0.001f;
	then = now;
	rotation[1] += rotationSpeed * deltaTime;

	Uniform uni;
	uni.projection = glm::perspectiveFov(fieldOfViewRadians, static_cast<float>(w), static_cast<float>(h), zNear, zFar);

	glm::vec3 cameraPosition {cos(sinceEpoch) * cameraRadius, 0, sin(sinceEpoch) * cameraRadius};
	uni.view = glm::lookAt(cameraPosition, target, up);

	std::vector<glm::vec3> textPositions;
	size_t i = 0;
	for (int yy = -1; yy <= 1; ++yy) {
		for (int xx = -2; xx <= 2; ++xx, ++i) {
			uni.model = glm::translate(glm::identity<glm::mat4>(), {translation[0] + xx * spread, translation[1] + yy * spread, translation[2]});
			uni.model = glm::rotate(uni.model, rotation[0], {1.0f, 0.0f, 0.0f});
			uni.model = glm::rotate(uni.model, rotation[1] + yy * xx * 0.2f, {0.0f, 1.0f, 0.0f});
			uni.model = glm::rotate(uni.model, rotation[2] + sinceEpoch + (yy * 3 + xx) * 0.1f, {0.0f, 0.0f, 1.0f});
			uni.model = glm::scale(uni.model, scale);
			uni.model = glm::translate(uni.model, {-50.0f, -75.0f, 0.0f});
			textPositions.push_back({glm::value_ptr(uni.model)[12], glm::value_ptr(uni.model)[13], glm::value_ptr(uni.model)[14]});
			if (i) {
				m_uniforms.updateUniform(&uni, sizeof(uni), ind * (FCOUNT - 1) + i - 1, 0);
			} else {
				m_core.updateUniform(&uni, sizeof(uni), ind, 0);
			}
		}
	}

	Uniform2 uni2;
	uni2.projection = uni.projection;
	size_t j = 0;
	for (size_t i = 0; i < textPositions.size(); ++i) {
		for (size_t ii = 0; ii < m_names[i].size(); ++ii) {
			auto tr = m_textures[m_names[i][ii] - 'a'].getResolution(0);
			glm::vec3 tp = uni.view * glm::vec4(textPositions[i], 1.0f);
			auto fromEye = glm::normalize(tp);
			float amountToMoveTowardEye = 150.0f;
			float viewX = tp[0] - fromEye[0] * amountToMoveTowardEye;
			float viewY = tp[1] - fromEye[1] * amountToMoveTowardEye;
			float viewZ = tp[2] - fromEye[2] * amountToMoveTowardEye;
			float desiredScale = -1.0f / h;
			float scale = viewZ * desiredScale;
			uni2.model = glm::translate(glm::identity<glm::mat4>(), {viewX, viewY, viewZ});
			uni2.model = glm::scale(uni2.model, {tr.first / 5.0f * scale, tr.second / 5.0f * scale, 1.0f});
			uni2.model = glm::translate(uni2.model, {static_cast<float>(ii), 0.0f, 0.0f});
			m_uniforms1.updateUniform(&uni2, sizeof(uni2), ind * m_uni1count + (j++), 0);
		}
	}

	m_core.renderScene(m_cmdBufs[ind], ind);
}

void VulkanApp::cleanup()
{
	m_core.waitIdle();
	if (m_Buffers) {
		m_Buffers->destroy();
	}
	m_uniforms.destroy();
	m_uniforms1.destroy();
	m_pipe.destroy(true);
	for (auto &it : m_textures) {
		it.destroy();
	}
	m_core.destroyCommandBuffer(m_cmdBufs);
	m_core.destroyShaders(m_shaders);
	m_core.cleanup();
	m_pWindowControl->cleanup();
}

void VulkanApp::run()
{
	auto func = [&](bool recreate) {
		if (recreate) {
			m_core.recreateSwapChain();
			m_pipe.destroyPipe();
			initPipe();
		}
		for (uint32_t i = 0; i < m_cmdBufs.size(); ++i) {
			recordCommandBuffer(i);
		}
	};
	func(false);
	while (!m_pWindowControl->shouldClose()) {
		m_pWindowControl->pollEvents();
		if (m_pWindowControl->wasResized()) {
			func(true);
		}
		try {
			auto ind = m_core.getImageIndex();
			updateUniformBuffer(ind);
		} catch (vk::OutOfDateKHRError &) {
			func(true);
		} catch (exception::VulkanException &e) {
			if (e.isSubopt()) {
				func(true);
			} else {
				throw;
			}
		}
	}
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	auto argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#else
int main(int argc, const char *argv[])
{
#endif
	auto ret = EXIT_SUCCESS;
	VulkanApp app;
	try {
		app.init(DBG, std::filesystem::path(argv[0]).parent_path());
		app.run();
	} catch (exception::VulkanException &e) {
		std::cout << e.what() << std::endl;
		ret = EXIT_FAILURE;
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		ret =  EXIT_FAILURE;
	}
	app.cleanup();
	return ret;
}

