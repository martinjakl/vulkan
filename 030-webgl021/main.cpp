#ifdef _WIN32
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#endif
#include <vulkan_core.h>
#include <glfw_control.h>
#include <vulkan_exception.h>
#include <vulkan_prim.h>
#include <overlay.h>
#include <filesystem>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>

using namespace macsnet::vulkan;

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;
const bool WINDOW_FULL = false;
const int MAX_FRAMES_IN_FLIGHT = 2;
const bool RESIZE = true;
const std::string APPNAME = "Mipmap 1";

class VulkanApp {
	public:
		VulkanApp();
		~VulkanApp();
		void init(bool debug, const std::filesystem::path &path);
		void run();
		void cleanup();

	private:
		struct Vertex {
			glm::vec4 pos;
			glm::vec2 tex;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(Vertex),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(Vertex, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(Vertex, tex),
				}};
			};
		};

		struct Uniform {
			glm::mat4 projection;
			glm::mat4 view;
			glm::mat4 model;

			static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBindings() {
				return {{{
					.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
				}}};
			}
		};

		void recordCommandBuffer(size_t ind);
		void updateUniformBuffer(size_t ind);
		void printFPS();

		std::string m_appName {APPNAME};
		std::unique_ptr<glfw::GLFWControl> m_pWindowControl;
		core::VulkanCore m_core;
		overlay::Overlay m_ov;
		std::unique_ptr<core::Buffers> m_Buffers;
		std::vector<vk::CommandBuffer> m_cmdBufs;
		core::Uniforms m_uni;
		size_t m_FSize = 0;
		size_t m_QuadSize = 0;
		std::chrono::time_point<std::chrono::high_resolution_clock> m_fpsStartTime = std::chrono::high_resolution_clock::now();
		size_t m_frames = 0;
		std::string m_text = "FPS: 00";

		const float m_fieldOfViewRadians = glm::radians(60.0f);
		float m_modelXRot = 0.0f;
		float m_modelYRot = 0.0f;
		std::chrono::time_point<std::chrono::high_resolution_clock> m_then = std::chrono::high_resolution_clock::now();
		const float m_zNear = 1.0f;
		const float m_zFar = 2000.0f;
		const glm::vec3 m_cameraPos {0.0f, 0.0f, 200.0f};
		const glm::vec3 m_target {0.0f, 0.0f, 0.0f};
		const glm::vec3 m_up {0.0f, 1.0f, 0.0f};
};

VulkanApp::VulkanApp() : m_core(m_appName), m_ov(&m_core), m_uni(&m_core)
{
}

VulkanApp::~VulkanApp()
{
}

void VulkanApp::init(bool debug, const std::filesystem::path &path)
{
	m_pWindowControl = std::make_unique<glfw::GLFWControl>(m_appName);
	m_pWindowControl->init(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FULL, RESIZE);
	auto u = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(Uniform)}});
	m_core.init({
		.pWindowControl = m_pWindowControl.get(),
		.debug = debug,
	   	.vShader = path / "shader.vert.spv",
	   	.fShader = path / "shader.frag.spv",
	   	.clockwise = false,
	   	.maxFramesInFlight = MAX_FRAMES_IN_FLIGHT,
		.getVertexBindingDescription = Vertex::getBindingDescription,
		.getVertexAttributeDescription = Vertex::getAttributeDescription,
		.getLayoutBindings = Uniform::getLayoutBindings,
		.uniforms = u,
		.textures = {{path / ".." / ".." / "textures" / "mip-low-res-example.png", true}},
		.msaaFlags = vk::SampleCountFlagBits::e4,
		.sampleShading = true,
		.preferedFormat = vk::Format::eB8G8R8A8Srgb,
		.samplerAddressMode = vk::SamplerAddressMode::eMirroredRepeat,
	});

	auto swapCount = m_core.getImageCount();
	m_uni.init(Uniform::getLayoutBindings, u, swapCount);

	primitives::Primitives prim, primq;
	prim.createBigF();
	primq.createQuad(10.0f);
	const auto &f = prim.getVertex();
	const auto &q = primq.getVertex();

	std::vector<Vertex> fv, qv;
	fv.reserve(f.size());
	qv.reserve(q.size());
	std::transform(f.begin(), f.end(), std::back_inserter(fv), [](auto i) {return Vertex{i.pos, i.texcoord};});
	std::transform(q.begin(), q.end(), std::back_inserter(qv), [](auto i) {return Vertex{i.pos, i.texcoord2};});
	m_FSize = f.size();
	m_QuadSize = q.size();

	m_cmdBufs = m_core.createCommandBuffer(swapCount);
	m_Buffers = m_core.newBuffers();
	m_Buffers->create({
		{fv.data(), fv.size() * sizeof(fv[0]), vk::BufferUsageFlagBits::eVertexBuffer},
		{qv.data(), qv.size() * sizeof(qv[0]), vk::BufferUsageFlagBits::eVertexBuffer},
	}, true);
	m_ov.init(path / ".." / "..", swapCount, 300, 100, vk::Format::eR8G8B8A8Srgb);
}

void VulkanApp::recordCommandBuffer(size_t ind)
{
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse,
	};
	m_cmdBufs[ind].begin(beginInfo);
	auto cv = m_core.getClearValue({0.9f, 0.9f, 0.9f, 1.0f});
	vk::RenderPassBeginInfo renderPassInfo {
		.renderPass = m_core.getRenderPass(),
		.framebuffer = m_core.getFb(ind),
		.renderArea {{0, 0}, m_core.getCurrentExtent()},
		.clearValueCount = static_cast<uint32_t>(cv.size()),
		.pClearValues = cv.data(),
	};
	m_ov.addCommandsBegin(ind, m_cmdBufs[ind]);
	m_cmdBufs[ind].beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);

	m_cmdBufs[ind].bindPipeline(vk::PipelineBindPoint::eGraphics, m_core.getPipeline());
	m_cmdBufs[ind].bindVertexBuffers(0, {m_Buffers->getBuffer(0)}, {0});
	m_cmdBufs[ind].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_core.getPipelineLayout(), 0, m_core.getDescSets(ind, true), {}); 
	m_cmdBufs[ind].draw(m_FSize, 1, 0, 0);
	m_cmdBufs[ind].bindVertexBuffers(0, {m_Buffers->getBuffer(1)}, {0});
	auto ds = m_uni.getDescSets(ind);
	ds.push_back(m_core.getTextures()->getDecriptorSet());
	m_cmdBufs[ind].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_core.getPipelineLayout(), 0, ds, {}); 
	m_cmdBufs[ind].draw(m_QuadSize, 1, 0, 0);

	m_ov.addCommandsEnd(ind, m_cmdBufs[ind]);

	m_cmdBufs[ind].endRenderPass();
	m_cmdBufs[ind].end();
}

void VulkanApp::printFPS()
{
	auto currentTime = std::chrono::high_resolution_clock::now();
	size_t t = std::chrono::duration<size_t, std::chrono::seconds::period>(std::chrono::duration_cast<std::chrono::seconds>(currentTime - m_fpsStartTime)).count();
	if (t) {
		std::stringstream s;
		s << "FPS: " << m_frames / t;
		m_text = s.str();
		m_frames = 0;
		m_fpsStartTime = std::chrono::high_resolution_clock::now();
	}
	++m_frames;
}

void VulkanApp::updateUniformBuffer(size_t ind)
{
	auto [w, h] = m_core.getCurrentExtent();
	printFPS();

	auto now = std::chrono::high_resolution_clock::now();
	float deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(now - m_then).count() * 1e-6f;
	m_then = now;

	Uniform uni;
	uni.projection = glm::perspectiveFov(m_fieldOfViewRadians, static_cast<float>(w), static_cast<float>(h), m_zNear, m_zFar);
	uni.view = glm::lookAt(m_cameraPos, m_target, m_up);

	m_modelXRot += 1.2f * deltaTime;
	m_modelYRot += 0.7f * deltaTime;
	uni.model = glm::identity<glm::mat4>();
	uni.model = glm::rotate(uni.model, m_modelXRot, {1.0f, 0.0f, 0.0f});
	uni.model = glm::rotate(uni.model, m_modelYRot, {0.0f, 1.0f, 0.0f});
	m_core.updateUniform(&uni, sizeof(uni), ind, 0);
	uni.model = glm::identity<glm::mat4>();
	uni.model = glm::rotate(uni.model, glm::pi<float>(), {0.0f, 0.0f, 1.0f});
	uni.model = glm::translate(uni.model, {30.0f, 10.0f, 160.0f});
	m_uni.updateUniform(&uni, sizeof(uni), ind, 0);
	m_ov.update(ind, m_text);

	m_core.renderScene(m_cmdBufs[ind], ind);
}

void VulkanApp::cleanup()
{
	m_core.waitIdle();
	m_ov.destroy();
	if (m_Buffers) {
		m_Buffers->destroy();
	}
	m_core.destroyCommandBuffer(m_cmdBufs);
	m_uni.destroy();
	m_core.cleanup();
	m_pWindowControl->cleanup();
}

void VulkanApp::run()
{
	auto func = [&](bool recreate) {
		if (recreate) {
			m_core.recreateSwapChain();
			m_ov.reinit();
		}
		for (uint32_t i = 0; i < m_cmdBufs.size(); ++i) {
			recordCommandBuffer(i);
		}
	};
	func(false);
	while (!m_pWindowControl->shouldClose()) {
		m_pWindowControl->pollEvents();
		if (m_pWindowControl->wasResized()) {
			func(true);
		}
		try {
			auto ind = m_core.getImageIndex();
			updateUniformBuffer(ind);
		} catch (vk::OutOfDateKHRError &) {
			func(true);
		} catch (exception::VulkanException &e) {
			if (e.isSubopt()) {
				func(true);
			} else {
				throw;
			}
		}
	}
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	auto argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#else
int main(int argc, const char *argv[])
{
#endif
	auto ret = EXIT_SUCCESS;
	VulkanApp app;
	try {
		app.init(DBG, std::filesystem::path(argv[0]).parent_path());
		app.run();
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		ret = EXIT_FAILURE;
	}
	app.cleanup();
	return ret;
}

