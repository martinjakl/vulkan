#version 450

layout(location = 0) in vec4 a_position;
layout(location = 1) in vec2 a_texcoord;
layout(set = 0, binding = 0) uniform U {
	mat4 projection;
	mat4 view;
	mat4 model;
} u;
layout(location = 0) out vec2 v_texcoord;

void main() {
	gl_Position = u.projection * u.view * u.model * a_position;
	v_texcoord = a_texcoord;
}

