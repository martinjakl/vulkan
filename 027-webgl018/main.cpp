#ifdef _WIN32
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#endif
#include <vulkan_core.h>
#include <glfw_control.h>
#include <vulkan_exception.h>
#include <vulkan_prim.h>
#include <filesystem>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>

using namespace macsnet::vulkan;

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;
const bool WINDOW_FULL = false;
const int MAX_FRAMES_IN_FLIGHT = 2;
const bool RESIZE = true;
const std::string APPNAME = "3D Textures";

class VulkanApp {
	public:
		VulkanApp();
		~VulkanApp();
		void init(bool debug, const std::filesystem::path &path);
		void run();
		void cleanup();

	private:
		struct Vertex {
			glm::vec4 pos;
			glm::vec2 tex;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(Vertex),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(Vertex, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(Vertex, tex),
				}};
			};
		};

		struct Uniform {
			glm::mat4 model;
			glm::mat4 projection;
			glm::mat4 view;

			static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBindings() {
				return {{{
					.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
				}}};
			}
		};

		void recordCommandBuffer(size_t ind);
		void updateUniformBuffer(size_t ind);
		void printFPS();

		std::string m_appName {APPNAME};
		std::unique_ptr<glfw::GLFWControl> m_pWindowControl;
		core::VulkanCore m_core;
		size_t m_vertSize = 0;
		std::unique_ptr<core::Buffers> m_Buffers;
		std::vector<vk::CommandBuffer> m_cmdBufs;
};

VulkanApp::VulkanApp() : m_core(m_appName)
{
}

VulkanApp::~VulkanApp()
{
}

void VulkanApp::init(bool debug, const std::filesystem::path &path)
{
	m_pWindowControl = std::make_unique<glfw::GLFWControl>(m_appName);
	m_pWindowControl->init(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FULL, RESIZE);
	auto u = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(Uniform)}});
	m_core.init({
		.pWindowControl = m_pWindowControl.get(),
		.debug = debug,
	   	.vShader = path / "texture.vert.spv",
	   	.fShader = path / "texture.frag.spv",
	   	.clockwise = false,
	   	.maxFramesInFlight = MAX_FRAMES_IN_FLIGHT,
		.getVertexBindingDescription = Vertex::getBindingDescription,
		.getVertexAttributeDescription = Vertex::getAttributeDescription,
		.getLayoutBindings = Uniform::getLayoutBindings,
		.uniforms = u,
		.textures = {{path / "../../textures/f-texture.png", false}},
		.msaaFlags = vk::SampleCountFlagBits::e64,
		.sampleShading = true,
		.textureFormat = vk::Format::eR8G8B8A8Unorm,
	});

	primitives::Primitives prim;
	prim.createBigF();
	const auto &f = prim.getVertex();
	std::vector<Vertex> verticies;
	verticies.reserve(f.size());
	std::transform(f.begin(), f.end(), std::back_inserter(verticies), [](auto i) {return Vertex {i.pos, i.texcoord2};});
	m_vertSize = verticies.size();

	auto swapCount = m_core.getImageCount();
	m_cmdBufs = m_core.createCommandBuffer(swapCount);
	m_Buffers = m_core.newBuffers();
	m_Buffers->create({
		{verticies.data(), verticies.size() * sizeof(verticies[0]), vk::BufferUsageFlagBits::eVertexBuffer},
	}, true);
}

void VulkanApp::recordCommandBuffer(size_t ind)
{
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse,
	};
	m_cmdBufs[ind].begin(beginInfo);
	auto cv = m_core.getClearValue({0.9f, 0.9f, 0.9f, 1.0f});
	vk::RenderPassBeginInfo renderPassInfo {
		.renderPass = m_core.getRenderPass(),
		.framebuffer = m_core.getFb(ind),
		.renderArea {{0, 0}, m_core.getCurrentExtent()},
		.clearValueCount = static_cast<uint32_t>(cv.size()),
		.pClearValues = cv.data(),
	};
	m_cmdBufs[ind].beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
	m_cmdBufs[ind].bindPipeline(vk::PipelineBindPoint::eGraphics, m_core.getPipeline());
	m_cmdBufs[ind].bindVertexBuffers(0, {m_Buffers->getBuffer(0)}, {0});
	m_cmdBufs[ind].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_core.getPipelineLayout(), 0, m_core.getDescSets(ind), {}); 
	m_cmdBufs[ind].draw(m_vertSize, 1, 0, 0);
	m_cmdBufs[ind].endRenderPass();
	m_cmdBufs[ind].end();
}

void VulkanApp::printFPS()
{
	static auto fpsStartTime = std::chrono::high_resolution_clock::now();
	auto currentTime = std::chrono::high_resolution_clock::now();
	size_t t = std::chrono::duration<size_t, std::chrono::seconds::period>(std::chrono::duration_cast<std::chrono::seconds>(currentTime - fpsStartTime)).count();
	static size_t frames = 0;
	static bool printed = false;
	if (t && t % 10 == 0) {
		if (!printed) {
			printed = true;
			std::cout << "fps: " << frames / t << std::endl;
			frames = 0;
			fpsStartTime = std::chrono::high_resolution_clock::now();
		}
	} else {
		printed = false;
	}
	++frames;
}

void VulkanApp::updateUniformBuffer(size_t ind)
{
	static const float fieldOfViewRadians = glm::radians(60.0f);
	static auto then = std::chrono::high_resolution_clock::now();
	static const float zNear = 1.0f;
	static const float zFar = 2000.0f;
	static const glm::vec3 cameraPosition {0.0f, 0.0f, 200.0f};
	static const glm::vec3 target {0.0f, 0.0f, 0.0f};
	static const glm::vec3 up {0.0f, 1.0f, 0.0f};
	static float modelXRotationRadians = 0.0f;
	static float modelYRotationRadians = 0.0f;

	printFPS();
	auto [w, h] = m_core.getCurrentExtent();

	auto now = std::chrono::high_resolution_clock::now();
	float deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(now - then).count() * 0.001f;
	then = now;

	modelXRotationRadians += 1.2f * deltaTime;
	modelYRotationRadians += 0.7f * deltaTime;

	Uniform uni;
	uni.projection = glm::perspectiveFov(fieldOfViewRadians, static_cast<float>(w), static_cast<float>(h), zNear, zFar);

	uni.view = glm::lookAt(cameraPosition, target, up);
	uni.model = glm::rotate(glm::identity<glm::mat4>(), modelXRotationRadians, {1.0f, 0.0f, 0.0f});
	uni.model = glm::rotate(uni.model, modelYRotationRadians, {0.0f, 1.0f, 0.0f});
	m_core.updateUniform(&uni, sizeof(uni), ind, 0);

	m_core.renderScene(m_cmdBufs[ind], ind);
}

void VulkanApp::cleanup()
{
	m_core.waitIdle();
	if (m_Buffers) {
		m_Buffers->destroy();
	}
	m_core.destroyCommandBuffer(m_cmdBufs);
	m_core.cleanup();
	m_pWindowControl->cleanup();
}

void VulkanApp::run()
{
	auto func = [&](bool recreate) {
		if (recreate) {
			m_core.recreateSwapChain();
		}
		for (uint32_t i = 0; i < m_cmdBufs.size(); ++i) {
			recordCommandBuffer(i);
		}
	};
	func(false);
	while (!m_pWindowControl->shouldClose()) {
		m_pWindowControl->pollEvents();
		if (m_pWindowControl->wasResized()) {
			func(true);
		}
		try {
			auto ind = m_core.getImageIndex();
			updateUniformBuffer(ind);
		} catch (vk::OutOfDateKHRError &) {
			func(true);
		} catch (exception::VulkanException &e) {
			if (e.isSubopt()) {
				func(true);
			} else {
				throw;
			}
		}
	}
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	auto argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#else
int main(int argc, const char *argv[])
{
#endif
	auto ret = EXIT_SUCCESS;
	VulkanApp app;
	try {
		app.init(DBG, std::filesystem::path(argv[0]).parent_path());
		app.run();
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		ret = EXIT_FAILURE;
	}
	app.cleanup();
	return ret;
}

