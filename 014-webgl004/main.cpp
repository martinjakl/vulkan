#ifdef _WIN32
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#endif
#include <vulkan_core.h>
#include <glfw_control.h>
#include <memory>
#include <vulkan_exception.h>
#include <cstdlib>
#include <filesystem>
#include <cmath>
#include <iostream>
#include <numeric>
#include <execution>
#include <map>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using macsnet::vulkan::glfw::GLFWControl;
using macsnet::vulkan::core::VulkanCore;
using macsnet::vulkan::core::Buffers;
using macsnet::vulkan::core::CoreInit;
using macsnet::vulkan::exception::VulkanException;

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;
const bool WINDOW_FULL = false;
const int MAX_FRAMES_IN_FLIGHT = 2;
const bool RESIZE = true;
const std::string APPNAME = "Texture effect";

#ifdef _WIN32
#define STR std::wstring
#define STRP(arg) L##arg 
#else
#define STR std::string
#define STRP(arg) arg
#endif


class VulkanApp {
	public:
		VulkanApp(const STR &effect = {});
		~VulkanApp();
		void init(bool debug, const std::filesystem::path &path);
		void run();
		void cleanup();
	private:
		struct Vertex {
			glm::vec2 pos;
			glm::vec2 textcoord;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(Vertex),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(Vertex, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(Vertex, textcoord),
				}};
			};
		};

		struct Float {
			Float(float f) : f(f) {}
			alignas(16) float f;
		};

		static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBindings() {
			return {{{
				.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
			}}, {{
				.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eFragment,
			}, {
				.binding = 1,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eFragment,
			}}};
		}

		void recordCommandBuffer(const vk::CommandBuffer &buf, uint32_t ind);
		void updateUniformBuffer(uint32_t ind);
		float computeKernelWeight(const std::array<Float, 9> &kernel);
		void fillVerticies();

		std::string m_appName {APPNAME};
		std::unique_ptr<GLFWControl> m_pWindowControl;
		VulkanCore m_core;
		std::vector<vk::CommandBuffer> m_cmdBufs;
		std::vector<Vertex> verticies;
		std::unique_ptr<Buffers> m_Buffers;
		STR m_effect;
		std::map<STR, std::array<Float, 9>>::const_iterator m_effectsIt;
	public:
		static const std::map<STR, std::array<Float, 9>> effects;
};

const std::map<STR, std::array<VulkanApp::Float, 9>> VulkanApp::effects {
	{STRP("normal"), {0, 0, 0, 0, 1, 0, 0, 0, 0}},
	{STRP("gaussianBlur"), {0.045, 0.122, 0.045, 0.122, 0.332, 0.122, 0.045, 0.122, 0.045}},
	{STRP("gaussianBlur2"), {1, 2, 1, 2, 4, 2, 1, 2, 1}},
	{STRP("gaussianBlur3"), {0, 1, 0, 1, 1, 1, 0, 1, 0}},
	{STRP("unsharpen"), {-1, -1, -1, -1,  9, -1, -1, -1, -1}},
	{STRP("sharpness"), {0, -1,  0, -1,  5, -1, 0, -1,  0}},
	{STRP("sharpen"), {-1, -1, -1, -1, 16, -1, -1, -1, -1}},
	{STRP("edgeDetect"), {-0.125, -0.125, -0.125, -0.125, 1, -0.125, -0.125, -0.125, -0.125}},
	{STRP("edgeDetect2"), {-1, -1, -1, -1,  8, -1, -1, -1, -1}},
	{STRP("edgeDetect3"), {-5, 0, 0, 0, 0, 0, 0, 0, 5}},
	{STRP("edgeDetect4"), {-1, -1, -1, 0,  0,  0, 1,  1,  1}},
	{STRP("edgeDetect5"), {-1, -1, -1, 2,  2,  2, -1, -1, -1}},
	{STRP("edgeDetect6"), {-5, -5, -5, -5, 39, -5, -5, -5, -5}},
	{STRP("sobelHorizontal"), {1,  2,  1, 0,  0,  0, -1, -2, -1}},
	{STRP("sobelVertical"), {1, 0, -1, 2, 0, -2, 1, 0, -1}},
	{STRP("previtHorizontal"), {1, 1, 1, 0, 0, 0, -1, -1, -1}},
	{STRP("previtVertical"), {1, 0, -1, 1, 0, -1, 1, 0, -1}},
	{STRP("boxBlur"), {0.111, 0.111, 0.111, 0.111, 0.111, 0.111, 0.111, 0.111, 0.111}},
	{STRP("triangleBlur"), {0.0625, 0.125, 0.0625, 0.125, 0.25,  0.125, 0.0625, 0.125, 0.0625}},
	{STRP("emboss"), {-2, -1, 0, -1, 1, 1, 0, 1, 2}},
};

VulkanApp::VulkanApp(const STR &effect) : m_core(m_appName), m_effect(effect)
{
	if (m_effect.empty()) {
		m_effectsIt = effects.begin();
	} else {
		m_effectsIt = effects.find(m_effect);
	}
}

VulkanApp::~VulkanApp()
{
}

void VulkanApp::fillVerticies()
{
	auto [width, height] = m_core.getTextureResolution(0);
	verticies = {
		{{0, 0}, {0.0, 0.0}},
		{{width, 0}, {1.0, 0.0}},
		{{0, height}, {0.0, 1.0}},
		{{0, height}, {0.0, 1.0}},
		{{width, 0}, {1.0, 0.0}},
		{{width, height}, {1.0, 1.0}},
	};
}

void VulkanApp::init(bool debug, const std::filesystem::path &path)
{
	m_pWindowControl = std::make_unique<GLFWControl>(m_appName);
	m_pWindowControl->init(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FULL, RESIZE);
	m_core.init(CoreInit {
		.pWindowControl = m_pWindowControl.get(),
		.debug = debug,
	   	.vShader = path / "simple.vert.spv",
	   	.fShader = path / "simple.frag.spv",
	   	.clockwise = true,
	   	.maxFramesInFlight = MAX_FRAMES_IN_FLIGHT,
		.getVertexBindingDescription = Vertex::getBindingDescription,
		.getVertexAttributeDescription = Vertex::getAttributeDescription,
		.getLayoutBindings = getLayoutBindings,
		.uniforms = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(glm::vec2)}, {sizeof(std::array<Float, 9>), sizeof(float)}}),
		.textures = {{path / "../../textures/pic.jpg", true}},
		.textureFormat = vk::Format::eR8G8B8A8Unorm,
	});
	fillVerticies();
	auto swapCount = m_core.getImageCount();
	m_cmdBufs = m_core.createCommandBuffer(swapCount);
	m_Buffers = m_core.newBuffers();
	m_Buffers->create({
		{verticies.data(), verticies.size() * sizeof(verticies[0]), vk::BufferUsageFlagBits::eVertexBuffer},
	}, true);
}

void VulkanApp::recordCommandBuffer(const vk::CommandBuffer &buf, uint32_t ind)
{
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse,
	};

	auto clearValue = m_core.getClearValue({0.9f, 0.9f, 0.9f, 1.0f});

	vk::RenderPassBeginInfo renderPassInfo {
		.renderPass = m_core.getRenderPass(),
		.framebuffer = m_core.getFb(ind),
		.renderArea {{0, 0}, m_core.getCurrentExtent()},
		.clearValueCount = static_cast<uint32_t>(clearValue.size()),
		.pClearValues = clearValue.data(),
	};

	buf.begin(beginInfo);

	buf.beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
	buf.bindPipeline(vk::PipelineBindPoint::eGraphics, m_core.getPipeline());
	buf.bindVertexBuffers(0, {m_Buffers->getBuffer(0)}, {0});
	buf.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_core.getPipelineLayout(), 0, m_core.getDescSets(ind), {}); 
	buf.draw(verticies.size(), 1, 0, 0);
	buf.endRenderPass();

	buf.end();
}

float VulkanApp::computeKernelWeight(const std::array<Float, 9> &kernel) {
	auto weight = std::reduce(std::execution::seq, kernel.begin(), kernel.end(), Float {0}, [](Float prev, Float curr) {return Float {prev.f + curr.f};});
	return weight.f <= 0 ? 1 : weight.f;
}

void VulkanApp::updateUniformBuffer(uint32_t ind)
{
	static auto fpsStartTime = std::chrono::high_resolution_clock::now();
	auto currentTime = std::chrono::high_resolution_clock::now();
	size_t t = std::chrono::duration<size_t, std::chrono::seconds::period>(std::chrono::duration_cast<std::chrono::seconds>(currentTime - fpsStartTime)).count();
	if (t && t % 2 == 0) {
		fpsStartTime = std::chrono::high_resolution_clock::now();
		if (m_effect.empty()) {
			++m_effectsIt;
			if (m_effectsIt == effects.end()) {
				m_effectsIt = effects.begin();
			}
		}
	}
	auto extent = m_core.getCurrentExtent();
	glm::vec2 resolution {extent.width, extent.height};
	float f = computeKernelWeight(m_effectsIt->second);
	m_core.updateUniform(&resolution, sizeof(resolution), ind, 0);
	m_core.updateUniform(&m_effectsIt->second, sizeof(m_effectsIt->second), ind, 1);
	m_core.updateUniform(&f, sizeof(f), ind, 2);
}

void VulkanApp::cleanup()
{
	m_core.waitIdle();
	if (m_Buffers) {
		m_Buffers->destroy();
	}
	m_core.destroyCommandBuffer(m_cmdBufs);
	m_core.cleanup();
	m_pWindowControl->cleanup();
}

void VulkanApp::run()
{
	auto func = [&](bool recreate) {
		if (recreate) {
			m_core.recreateSwapChain();
		}
		for (uint32_t i = 0; i < m_cmdBufs.size(); ++i) {
			recordCommandBuffer(m_cmdBufs[i], i);
		}
	};
	func(false);
	while (!m_pWindowControl->shouldClose()) {
		m_pWindowControl->pollEvents();
		if (m_pWindowControl->wasResized()) {
			func(true);
		}
		try {
			auto ind = m_core.getImageIndex();
			updateUniformBuffer(ind);
			m_core.renderScene(m_cmdBufs[ind], ind);
		} catch (VulkanException &e) {
			if (e.isSubopt()) {
				func(true);
			} else {
				throw;
			}
		} catch (vk::OutOfDateKHRError &) {
			func(true);
		}
	}
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	auto argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#else
int main(int argc, const char *argv[])
{
#endif
	auto ret = EXIT_SUCCESS;
	STR effect;
	if (argc > 1) {
		if (VulkanApp::effects.find(argv[1]) != VulkanApp::effects.end()) {
			effect = argv[1];
		}
	}
	VulkanApp app(effect);
	try {
		app.init(DBG, std::filesystem::path(argv[0]).parent_path());
		app.run();
	} catch (VulkanException &e) {
		std::cout << e.what() << std::endl;
		ret = EXIT_FAILURE;
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		ret =  EXIT_FAILURE;
	}
	app.cleanup();
	return ret;
}

