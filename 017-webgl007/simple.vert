#version 450

layout(location = 0) in vec2 a_position;
layout(binding = 0) uniform Uniform {mat4 u_matrix;} u;

void main() {
	gl_Position = vec4((u.u_matrix * vec4(a_position, 1, 1)).xy, 0, 1000);
}

