#version 450

layout(set = 1, binding = 0) uniform Uni {vec4 u_color;} u;
layout(location = 0) out vec4 outColor;

void main() {
	outColor = u.u_color;
}

