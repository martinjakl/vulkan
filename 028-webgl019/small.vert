#version 450

layout(location = 0) in vec4 a_position;
layout(location = 1) in vec2 a_texcoord;
layout(location = 2) in vec2 a_trans;
layout(location = 3) in float a_speed;
layout(set = 0, binding = 0) uniform U1 {
	mat4 u_proj;
	mat4 u_view;
	mat4 u_model;
} u1;
layout(set = 0, binding = 1) uniform U2 {float u_time;} u2;
layout(location = 0) out vec2 v_texcoord;

void main() {
	mat4 t1 = mat4(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, -a_trans.x, 0, -a_trans.y, 1);
	mat4 t2 = mat4(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, a_trans.x, 0, a_trans.y, 1);
	vec4 pos = t1 * a_position;
	float angle = a_speed * u2.u_time;
	float s = sin(angle);
	float c = cos(angle);
	mat4 rot = mat4(c, 0, -s, 0, 0, 1, 0, 0, s, 0, c, 0, 0, 0, 0, 1);
	pos = rot * pos;
	pos = t2 * pos;
	gl_Position = u1.u_proj * u1.u_view * u1.u_model * pos;
	v_texcoord = a_texcoord;
}

