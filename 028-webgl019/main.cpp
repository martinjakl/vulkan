#ifdef _WIN32
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#endif
#include <vulkan_core.h>
#include <glfw_control.h>
#include <vulkan_exception.h>
#include <vulkan_prim.h>
#include <overlay.h>
#include <filesystem>
#include <iostream>
#include <cmath>
#include <random>
#include <glm/gtc/matrix_transform.hpp>

using namespace macsnet::vulkan;

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;
const bool WINDOW_FULL = false;
const int MAX_FRAMES_IN_FLIGHT = 3;
const bool RESIZE = true;
const bool FORCE_MAILBOX = false;
const std::string APPNAME = "Camera fixed to object with new libs and 1 VAO";

class VulkanApp {
	public:
		VulkanApp();
		~VulkanApp();
		void init(bool debug, const std::filesystem::path &path);
		void run();
		void cleanup();

	private:
		struct VertexSmall {
			glm::vec4 pos;
			glm::vec2 tex;
			glm::vec2 trans;
			float speed;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(VertexSmall),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(VertexSmall, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(VertexSmall, tex),
				}, {
					.location = 2,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(VertexSmall, trans),
				}, {
					.location = 3,
					.binding = 0,
					.format = vk::Format::eR32Sfloat,
					.offset = offsetof(VertexSmall, speed),
				}};
			};
		};

		struct VertexBig {
			glm::vec4 pos;
			glm::vec4 color;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(VertexBig),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(VertexBig, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(VertexBig, color),
				}};
			};
		};

		struct Uniform {
			glm::mat4 projection;
			glm::mat4 view;
			glm::mat4 model;

			static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBindings() {
				return {{{
					.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
				}}};
			}

			static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBindings1() {
				return {{{
					.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
				}, {
					.binding = 1,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
				}}};
			}
		};

		void recordCommandBuffer(size_t ind);
		void updateUniformBuffer(size_t ind);
		void printFPS();
		std::vector<VertexSmall> createMany(const primitives::Primitives &p);
		void initPipe();

		std::string m_appName {APPNAME};
		std::unique_ptr<glfw::GLFWControl> m_pWindowControl;
		core::VulkanCore m_core;
		size_t m_vertBigSize = 0;
		size_t m_vertSmallSize = 0;
		std::unique_ptr<core::Buffers> m_Buffers;
		std::vector<vk::CommandBuffer> m_cmdBufs;
		const uint32_t m_numFs = 35;
		const float m_radius = 750.0f;
		const int m_rows = 47;
		std::vector<vk::ShaderModule> m_shaders;
		core::Pipeline m_pipe;
		core::Uniforms m_uni;
		overlay::Overlay m_ov;
		std::chrono::time_point<std::chrono::high_resolution_clock> m_fpsStartTime = std::chrono::high_resolution_clock::now();
		size_t m_frames = 0;
		std::string m_text = "FPS: 00";
};

VulkanApp::VulkanApp() : m_core(m_appName), m_pipe(&m_core), m_uni(&m_core), m_ov(&m_core)
{
}

VulkanApp::~VulkanApp()
{
}

std::vector<VulkanApp::VertexSmall> VulkanApp::createMany(const primitives::Primitives &p)
{
	std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_real_distribution<float> fdist(0.0f, 1.0f);
	std::uniform_int_distribution<int> intd(0, 1);
	const auto &pv = p.getVertex();
	auto ident = glm::identity<glm::mat4>();
	std::vector<VertexSmall> prim(m_numFs * m_rows * pv.size());
	int ypos = 0;
	for (int ytr = -(m_rows - 1) / 2; ytr <= (m_rows - 1) / 2; ++ytr) {
		int y = ytr * 200;
		for (uint32_t i = 0; i < m_numFs; ++i) {
			float angle = static_cast<float>(i) * glm::pi<float>() * 2.0f / m_numFs;
			float x = cos(angle) * m_radius;
			float z = sin(angle) * m_radius;
			auto matrix = glm::rotate(ident, fdist(generator) * 2.0f * glm::pi<float>(), {0.0f, 1.0f, 0.0f});
			auto matrix2 = glm::translate(ident, {x, static_cast<float>(y), z});
			float speed = (fdist(generator) * 12.0f * glm::pi<float>()) - (6.0f * glm::pi<float>());
			int t = intd(generator);
			for (size_t ii = 0; ii < pv.size(); ++ii) {
				size_t pos = pv.size() * m_numFs * ypos + i * pv.size() + ii;
				prim[pos].trans = {x, z};
				prim[pos].speed = speed;
				prim[pos].pos = matrix2 * matrix * pv[ii].pos;
				prim[pos].tex = t ? pv[ii].texcoord : pv[ii].texcoord2;
			}
		}
		++ypos;
	}
	return prim;
}

void VulkanApp::initPipe()
{
	m_pipe.init({
		.vs = m_shaders[0],
		.fs = m_shaders[1],
		.getVertexBindingDescription = VertexSmall::getBindingDescription,
		.getVertexAttributeDescription = VertexSmall::getAttributeDescription,
		.extent = m_core.getCurrentExtent(),
		.sampleShading = true,
		.msaaFlags = m_core.getMSAAFlags(),
		.uniLayout = &m_uni.getLayout(),
		.textureLayout = &m_core.getTextures()->getDSLayout(),
		.renderPass = m_core.getRenderPass(),
	});
}

void VulkanApp::init(bool debug, const std::filesystem::path &path)
{
	m_pWindowControl = std::make_unique<glfw::GLFWControl>(m_appName);
	m_pWindowControl->init(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FULL, RESIZE);
	auto u = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(Uniform)}});
	auto u1 = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(Uniform), sizeof(float)}});
	m_core.init({
		.pWindowControl = m_pWindowControl.get(),
		.debug = debug,
	   	.vShader = path / "big.vert.spv",
	   	.fShader = path / "big.frag.spv",
	   	.clockwise = false,
	   	.maxFramesInFlight = MAX_FRAMES_IN_FLIGHT,
		.getVertexBindingDescription = VertexBig::getBindingDescription,
		.getVertexAttributeDescription = VertexBig::getAttributeDescription,
		.getLayoutBindings = Uniform::getLayoutBindings,
		.uniforms = u,
		.textures = {{path / ".." / ".." / "textures" / "f-texture.png", false}},
		.msaaFlags = vk::SampleCountFlagBits::e4,
		.sampleShading = true,
		.textureFormat = vk::Format::eR8G8B8A8Unorm,
		.preferedPresentMode = vk::PresentModeKHR::eMailbox,
		.forcePresentMode = FORCE_MAILBOX,

	});

	primitives::Primitives prim;
	prim.createBigF();
	prim.center();
	const auto &f = prim.getVertex();

	std::vector<VertexBig> big;
	big.reserve(f.size());
	std::transform(f.begin(), f.end(), std::back_inserter(big), [](auto i) {return VertexBig {i.pos, i.color};});
	m_vertBigSize = big.size();

	auto smalls = createMany(prim);
	m_vertSmallSize = smalls.size();

	auto swapCount = m_core.getImageCount();
	m_cmdBufs = m_core.createCommandBuffer(swapCount);
	m_Buffers = m_core.newBuffers();
	m_Buffers->create({
		{big.data(), big.size() * sizeof(big[0]), vk::BufferUsageFlagBits::eVertexBuffer},
		{smalls.data(), smalls.size() * sizeof(smalls[0]), vk::BufferUsageFlagBits::eVertexBuffer},
	}, true);
	m_shaders = m_core.readShaders({path / "small.vert.spv", path / "small.frag.spv"});
	m_uni.init(Uniform::getLayoutBindings1, u1, swapCount);
	initPipe();
	m_ov.init(path / ".." / "..", swapCount, 300, 100, vk::Format::eR8G8B8A8Unorm);
}

void VulkanApp::recordCommandBuffer(size_t ind)
{
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse,
	};
	m_cmdBufs[ind].begin(beginInfo);
	auto cv = m_core.getClearValue({0.9f, 0.9f, 0.9f, 1.0f});
	vk::RenderPassBeginInfo renderPassInfo {
		.renderPass = m_core.getRenderPass(),
		.framebuffer = m_core.getFb(ind),
		.renderArea {{0, 0}, m_core.getCurrentExtent()},
		.clearValueCount = static_cast<uint32_t>(cv.size()),
		.pClearValues = cv.data(),
	};
	m_ov.addCommandsBegin(ind, m_cmdBufs[ind]);
	m_cmdBufs[ind].beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);

	m_cmdBufs[ind].bindPipeline(vk::PipelineBindPoint::eGraphics, m_core.getPipeline());
	m_cmdBufs[ind].bindVertexBuffers(0, {m_Buffers->getBuffer(0)}, {0});
	m_cmdBufs[ind].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_core.getPipelineLayout(), 0, m_core.getDescSets(ind, false), {}); 
	m_cmdBufs[ind].draw(m_vertBigSize, 1, 0, 0);

	m_cmdBufs[ind].bindPipeline(vk::PipelineBindPoint::eGraphics, m_pipe.getPipeline());
	m_cmdBufs[ind].bindVertexBuffers(0, {m_Buffers->getBuffer(1)}, {0});
	auto ds = m_uni.getDescSets(ind);
	ds.push_back(m_core.getTextures()->getDecriptorSet());
	m_cmdBufs[ind].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_pipe.getPipelineLayout(), 0, ds, {}); 
	m_cmdBufs[ind].draw(m_vertSmallSize, 1, 0, 0);

	m_ov.addCommandsEnd(ind, m_cmdBufs[ind]);

	m_cmdBufs[ind].endRenderPass();
	m_cmdBufs[ind].end();
}

void VulkanApp::printFPS()
{
	auto currentTime = std::chrono::high_resolution_clock::now();
	size_t t = std::chrono::duration<size_t, std::chrono::seconds::period>(std::chrono::duration_cast<std::chrono::seconds>(currentTime - m_fpsStartTime)).count();
	if (t) {
		std::stringstream s;
		s << "FPS: " << m_frames / t;
		m_text = s.str();
		m_frames = 0;
		m_fpsStartTime = std::chrono::high_resolution_clock::now();
	}
	++m_frames;
}

void VulkanApp::updateUniformBuffer(size_t ind)
{
	static const float fieldOfViewRadians = glm::radians(60.0f);
	static const auto start = std::chrono::high_resolution_clock::now();
	static auto then = std::chrono::high_resolution_clock::now();
	static const float zNear = 1.0f;
	static const float zFar = 7000.0f;
	static const glm::vec3 target {m_radius, 0.0f, 0.0f};
	static const glm::vec3 up {0.0f, 1.0f, 0.0f};
	static float camAngle = 0.0f;

	auto [w, h] = m_core.getCurrentExtent();
	printFPS();

	auto now = std::chrono::high_resolution_clock::now();
	float deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(now - then).count() * 1e-6f;
	float epoch = std::chrono::duration_cast<std::chrono::microseconds>(now - start).count() * 1e-6f;
	then = now;

	Uniform uni;
	uni.projection = glm::perspectiveFov(fieldOfViewRadians, static_cast<float>(w), static_cast<float>(h), zNear, zFar);
	
	camAngle += 60.0f * deltaTime;
	if (camAngle >= 360.0f) {
		camAngle -= 360.0f;
	}

	glm::vec3 cameraPosition {m_radius * 1.5f * cos(glm::radians(camAngle)), -1000.0f, m_radius * 2.0f * sin(glm::radians(camAngle))};
	uni.view = glm::lookAt(cameraPosition, target, up);
	uni.model = glm::identity<glm::mat4>();
	m_uni.updateUniform(&uni, sizeof(uni), ind, 0);
	m_uni.updateUniform(&epoch, sizeof(float), ind, 1);
	uni.model = glm::scale(uni.model, {7.0f, 7.0f, 7.0f});
	m_core.updateUniform(&uni, sizeof(uni), ind, 0);
	m_ov.update(ind, m_text);

	m_core.renderScene(m_cmdBufs[ind], ind);
}

void VulkanApp::cleanup()
{
	m_core.waitIdle();
	m_ov.destroy();
	if (m_Buffers) {
		m_Buffers->destroy();
	}
	m_core.destroyCommandBuffer(m_cmdBufs);
	m_core.destroyShaders(m_shaders);
	m_pipe.destroy(true);
	m_uni.destroy();
	m_core.cleanup();
	m_pWindowControl->cleanup();
}

void VulkanApp::run()
{
	auto func = [&](bool recreate) {
		if (recreate) {
			m_core.recreateSwapChain();
			m_pipe.destroyPipe();
			initPipe();
			m_ov.reinit();
		}
		for (uint32_t i = 0; i < m_cmdBufs.size(); ++i) {
			recordCommandBuffer(i);
		}
	};
	func(false);
	while (!m_pWindowControl->shouldClose()) {
		m_pWindowControl->pollEvents();
		if (m_pWindowControl->wasResized()) {
			func(true);
		}
		try {
			auto ind = m_core.getImageIndex();
			updateUniformBuffer(ind);
		} catch (vk::OutOfDateKHRError &) {
			func(true);
		} catch (exception::VulkanException &e) {
			if (e.isSubopt()) {
				func(true);
			} else {
				throw;
			}
		}
	}
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	auto argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#else
int main(int argc, const char *argv[])
{
#endif
	auto ret = EXIT_SUCCESS;
	VulkanApp app;
	try {
		app.init(DBG, std::filesystem::path(argv[0]).parent_path());
		app.run();
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		ret = EXIT_FAILURE;
	}
	app.cleanup();
	return ret;
}

