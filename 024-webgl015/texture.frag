#version 450

layout(location = 0) in vec2 v_texcoord;
layout(set = 1, binding = 0) uniform U {vec4 u_color;} u;
layout(set = 2, binding = 0) uniform sampler2D u_texture;
layout(location = 0) out vec4 outColor;

void main() {
	outColor = texture(u_texture, v_texcoord) * u.u_color;
}

