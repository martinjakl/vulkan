#version 450

layout(location = 0) in vec2 v_texcoord;
layout(set = 1, binding = 0) uniform sampler2D u_texture;
layout(set = 1, binding = 1) uniform sampler2D u_texture2;
layout(location = 0) out vec4 outColor;

void main() {
	outColor = texture(u_texture, v_texcoord) * texture(u_texture2, v_texcoord);
}

