#version 450

layout(set = 1, binding = 0) uniform sampler2D u_image0;
layout(set = 1, binding = 1) uniform sampler2D u_image1;
layout(location = 0) in vec2 v_textCoord;
layout(location = 0) out vec4 outColor;

void main() {
	vec4 color0 = texture(u_image0, v_textCoord);
	vec4 color1 = texture(u_image1, v_textCoord);
	outColor = color0 * color1;
}

