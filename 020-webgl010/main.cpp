#ifdef _WIN32
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#endif
#include <vulkan_core.h>
#include <glfw_control.h>
#include <vulkan_exception.h>
#include <filesystem>
#include <iostream>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using macsnet::vulkan::glfw::GLFWControl;
using macsnet::vulkan::core::VulkanCore;
using macsnet::vulkan::core::Buffers;
using macsnet::vulkan::core::CoreInit;
using macsnet::vulkan::exception::VulkanException;

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;
const bool WINDOW_FULL = false;
const int MAX_FRAMES_IN_FLIGHT = 2;
const bool RESIZE = true;
const std::string APPNAME = "Perspective";

class VulkanApp {
	public:
		VulkanApp();
		~VulkanApp();
		void init(bool debug, const std::filesystem::path &path);
		void run();
		void cleanup();

	private:
		struct Vertex {
			glm::vec3 pos;
			glm::vec4 color;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(Vertex),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32B32Sfloat,
					.offset = offsetof(Vertex, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(Vertex, color),
				}};
			};
		};

		static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBindings() {
			return {{{
				.binding = 0,
				.descriptorType = vk::DescriptorType::eUniformBuffer,
				.descriptorCount = 1,
				.stageFlags = vk::ShaderStageFlagBits::eVertex,
			}}};
		}

		void recordCommandBuffer(size_t ind);
		void updateUniformBuffer(size_t ind);
		void fillVerticies();
		void fillColor();
		void printFPS();

		std::string m_appName {APPNAME};
		std::unique_ptr<GLFWControl> m_pWindowControl;
		VulkanCore m_core;
		std::vector<Vertex> verticies;
		std::unique_ptr<Buffers> m_Buffers;
		std::vector<vk::CommandBuffer> m_cmdBufs;
};

VulkanApp::VulkanApp() : m_core(m_appName)
{
}

VulkanApp::~VulkanApp()
{
}

void VulkanApp::fillVerticies()
{
	verticies = {
		// left column front
		{{0,   0,   0}},
		{{0, 150,   0}},
		{{30,  0,   0}},
		{{0, 150,   0}},
		{{30,150,   0}},
		{{30,  0,   0}},

		// top rung front
		{{30,   0,  0}},
		{{30,  30,  0}},
		{{100,  0,  0}},
		{{30,  30,  0}},
		{{100, 30,  0}},
		{{100,  0,  0}},

		// middle rung front
		{{30,  60,  0}},
		{{30,  90,  0}},
		{{67,  60,  0}},
		{{30,  90,  0}},
		{{67,  90,  0}},
		{{67,  60,  0}},

		// left column back
		{{0,   0,  30}},
		{{30,  0,  30}},
		{{0, 150,  30}},
		{{0, 150,  30}},
		{{30,  0,  30}},
		{{30, 150, 30}},

		// top rung back
		{{30,   0, 30}},
		{{100,  0, 30}},
		{{30,  30, 30}},
		{{30,  30, 30}},
		{{100,  0, 30}},
		{{100, 30, 30}},

		// middle rung back
		{{30,  60, 30}},
		{{67,  60, 30}},
		{{30,  90, 30}},
		{{30,  90, 30}},
		{{67,  60, 30}},
		{{67,  90, 30}},

		// top
		{{0,    0,  0}},
		{{100,  0,  0}},
		{{100,  0, 30}},
		{{0,    0,  0}},
		{{100,  0, 30}},
		{{0,    0, 30}},

		// top rung right
		{{100,  0,  0}},
		{{100, 30,  0}},
		{{100, 30, 30}},
		{{100,  0,  0}},
		{{100, 30, 30}},
		{{100,  0, 30}},

		// under top rung
		{{30,  30,  0}},
		{{30,  30, 30}},
		{{100, 30, 30}},
		{{30,  30,  0}},
		{{100, 30, 30}},
		{{100, 30,  0}},

		// between top rung and middle
		{{30,  30,  0}},
		{{30,  60, 30}},
		{{30,  30, 30}},
		{{30,  30,  0}},
		{{30,  60,  0}},
		{{30,  60, 30}},

		// top of middle rung
		{{30,  60,  0}},
		{{67,  60, 30}},
		{{30,  60, 30}},
		{{30,  60,  0}},
		{{67,  60,  0}},
		{{67,  60, 30}},

		// right of middle rung
		{{67,  60,  0}},
		{{67,  90, 30}},
		{{67,  60, 30}},
		{{67,  60,  0}},
		{{67,  90,  0}},
		{{67,  90, 30}},

		// bottom of middle rung.
		{{30,  90,  0}},
		{{30,  90, 30}},
		{{67,  90, 30}},
		{{30,  90,  0}},
		{{67,  90, 30}},
		{{67,  90,  0}},

		// right of bottom
		{{30,  90,  0}},
		{{30, 150, 30}},
		{{30,  90, 30}},
		{{30,  90,  0}},
		{{30, 150,  0}},
		{{30, 150, 30}},

		// bottom
		{{0,  150,  0}},
		{{0,  150, 30}},
		{{30, 150, 30}},
		{{0,  150,  0}},
		{{30, 150, 30}},
		{{30, 150,  0}},

		// left side
		{{0,    0,  0}},
		{{0,    0, 30}},
		{{0,  150, 30}},
		{{0,    0,  0}},
		{{0,  150, 30}},
		{{0,  150,  0}},
	};
	for (auto &vert : verticies) {
		vert.pos.y *= -1;
	}
}

void VulkanApp::fillColor()
{
	std::vector<glm::u8vec3> col(verticies.size());
	for (size_t i = 0; i < 16 * 6; ++i) {
		switch (static_cast<size_t>(floor(static_cast<double>(i) / 6))) {
			case 0:
			case 1:
			case 2:
				col[i] = {200, 70, 120};
				break;
			case 3:
			case 4:
			case 5:
				col[i] = {80, 70, 200};
				break;
			case 6:
				col[i] = {70, 200, 210};
				break;
			case 7:
				col[i] = {200, 200, 70};
				break;
			case 8:
				col[i] = {210, 100, 70};
				break;
			case 9:
				col[i] = {210, 160, 70};
				break;
			case 10:
				col[i] = {70, 180, 210};
				break;
			case 11:
				col[i] = {100, 70, 210};
				break;
			case 12:
				col[i] = {76, 210, 100};
				break;
			case 13:
				col[i] = {140, 210, 80};
				break;
			case 14:
				col[i] = {90, 130, 110};
				break;
			case 15:
				col[i] = {160, 160, 220};
				break;
		}
	}
	for (size_t i = 0; i < col.size(); ++i) {
		verticies[i].color = {static_cast<float>(col[i].r) / 255, static_cast<float>(col[i].g) / 255, static_cast<float>(col[i].b) / 255, 1};
	}
}

void VulkanApp::init(bool debug, const std::filesystem::path &path)
{
	m_pWindowControl = std::make_unique<GLFWControl>(m_appName);
	m_pWindowControl->init(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FULL, RESIZE);
	m_core.init({
		.pWindowControl = m_pWindowControl.get(),
		.debug = debug,
	   	.vShader = path / "simple.vert.spv",
	   	.fShader = path / "simple.frag.spv",
	   	.clockwise = false,
	   	.maxFramesInFlight = MAX_FRAMES_IN_FLIGHT,
		.getVertexBindingDescription = Vertex::getBindingDescription,
		.getVertexAttributeDescription = Vertex::getAttributeDescription,
		.getLayoutBindings = getLayoutBindings,
		.uniforms = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(glm::mat4)}}),
		.msaaFlags = vk::SampleCountFlagBits::e64,
	});
	fillVerticies();
	fillColor();
	auto swapCount = m_core.getImageCount();
	m_cmdBufs = m_core.createCommandBuffer(swapCount);
	m_Buffers = m_core.newBuffers();
	m_Buffers->create({
		{verticies.data(), verticies.size() * sizeof(verticies[0]), vk::BufferUsageFlagBits::eVertexBuffer},
	}, true);
}

void VulkanApp::recordCommandBuffer(size_t ind)
{
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse,
	};
	m_cmdBufs[ind].begin(beginInfo);
	auto cv = m_core.getClearValue({0.9f, 0.9f, 0.9f, 1.0f});
	vk::RenderPassBeginInfo renderPassInfo {
		.renderPass = m_core.getRenderPass(),
		.framebuffer = m_core.getFb(ind),
		.renderArea {{0, 0}, m_core.getCurrentExtent()},
		.clearValueCount = static_cast<uint32_t>(cv.size()),
		.pClearValues = cv.data(),
	};

	m_cmdBufs[ind].beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
	m_cmdBufs[ind].bindPipeline(vk::PipelineBindPoint::eGraphics, m_core.getPipeline());
	m_cmdBufs[ind].bindVertexBuffers(0, {m_Buffers->getBuffer(0)}, {0});
	m_cmdBufs[ind].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_core.getPipelineLayout(), 0, m_core.getDescSets(ind), {}); 
	m_cmdBufs[ind].draw(verticies.size(), 1, 0, 0);
	m_cmdBufs[ind].endRenderPass();

	m_cmdBufs[ind].end();
}

void VulkanApp::printFPS()
{
	static auto fpsStartTime = std::chrono::high_resolution_clock::now();
	auto currentTime = std::chrono::high_resolution_clock::now();
	size_t t = std::chrono::duration<size_t, std::chrono::seconds::period>(std::chrono::duration_cast<std::chrono::seconds>(currentTime - fpsStartTime)).count();
	static size_t frames = 0;
	static bool printed = false;
	if (t && t % 10 == 0) {
		if (!printed) {
			printed = true;
			std::cout << "fps: " << frames / t << std::endl;
			frames = 0;
			fpsStartTime = std::chrono::high_resolution_clock::now();
		}
	} else {
		printed = false;
	}
	++frames;
}

void VulkanApp::updateUniformBuffer(size_t ind)
{
	printFPS();
	auto [w, h] = m_core.getCurrentExtent();
	auto matrix = glm::perspectiveFov(glm::radians(60.0f), static_cast<float>(w), static_cast<float>(h), 1.0f, 2000.0f);
	matrix = glm::translate(matrix, {-150, 0, -360});
	matrix = glm::rotate(matrix, -glm::radians(190.0f), {1, 0, 0});
	matrix = glm::rotate(matrix, glm::radians(40.0f), {0, 1, 0});
	matrix = glm::rotate(matrix, -glm::radians(30.0f), {0, 0, 1});
	m_core.updateUniform(&matrix, sizeof(matrix), ind, 0);
	m_core.renderScene(m_cmdBufs[ind], ind);
}

void VulkanApp::cleanup()
{
	m_core.waitIdle();
	if (m_Buffers) {
		m_Buffers->destroy();
	}
	m_core.destroyCommandBuffer(m_cmdBufs);
	m_core.cleanup();
	m_pWindowControl->cleanup();
}

void VulkanApp::run()
{
	auto func = [&](bool recreate) {
		if (recreate) {
			m_core.recreateSwapChain();
		}
		for (uint32_t i = 0; i < m_cmdBufs.size(); ++i) {
			recordCommandBuffer(i);
		}
	};
	func(false);
	while (!m_pWindowControl->shouldClose()) {
		m_pWindowControl->pollEvents();
		if (m_pWindowControl->wasResized()) {
			func(true);
		}
		try {
			auto ind = m_core.getImageIndex();
			updateUniformBuffer(ind);
		} catch (VulkanException &e) {
			if (e.isSubopt()) {
				func(true);
			} else {
				throw;
			}
		} catch (vk::OutOfDateKHRError &) {
			func(true);
		}
	}
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	auto argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#else
int main(int argc, const char *argv[])
{
#endif
	auto ret = EXIT_SUCCESS;
	VulkanApp app;
	try {
		app.init(DBG, std::filesystem::path(argv[0]).parent_path());
		app.run();
	} catch (VulkanException &e) {
		std::cout << e.what() << std::endl;
		ret = EXIT_FAILURE;
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		ret =  EXIT_FAILURE;
	}
	app.cleanup();
	return ret;
}

