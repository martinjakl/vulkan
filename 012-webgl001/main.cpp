#ifdef _WIN32
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#endif
#include <vulkan_core.h>
#include <glfw_control.h>
#include <memory>
#include <vulkan_exception.h>
#include <cstdlib>
#include <filesystem>
#include <cmath>
#include <iostream>
#include <random>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using macsnet::vulkan::glfw::GLFWControl;
using macsnet::vulkan::core::VulkanCore;
using macsnet::vulkan::core::Buffers;
using macsnet::vulkan::core::CoreInit;
using macsnet::vulkan::exception::VulkanException;

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;
const bool WINDOW_FULL = false;
const int MAX_FRAMES_IN_FLIGHT = 2;
const bool RESIZE = true;
const std::string APPNAME = "Basics";

class VulkanApp {
	public:
		VulkanApp();
		~VulkanApp();
		void init(bool debug, const std::filesystem::path &path);
		void run();
		void cleanup();
	private:
		struct Vertex {
			glm::vec2 pos;
			glm::vec4 color;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				return {{
					.binding = 0,
					.stride = sizeof(Vertex),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				return {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(Vertex, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32B32A32Sfloat,
					.offset = offsetof(Vertex, color),
				}};
			};
		};

		struct Uniform {
			glm::vec2 res;

			static std::vector<std::vector<vk::DescriptorSetLayoutBinding>> getLayoutBinding() {
				return {{{
					.binding = 0,
					.descriptorType = vk::DescriptorType::eUniformBuffer,
					.descriptorCount = 1,
					.stageFlags = vk::ShaderStageFlagBits::eVertex,
				}}};
			}
		};

		void recordCommandBuffer(const vk::CommandBuffer &buf, uint32_t ind);
		void updateUniformBuffer(uint32_t ind);
		int randomInt(int range);
		void addRectangle(int x, int y, int width, int height, const glm::vec4 &color);

		std::string m_appName {APPNAME};
		std::unique_ptr<GLFWControl> m_pWindowControl;
		VulkanCore m_core;
		std::vector<vk::CommandBuffer> m_cmdBufs;
		std::vector<Vertex> verticies;
		std::unique_ptr<Buffers> m_Buffers;
		std::default_random_engine generator;
};

VulkanApp::VulkanApp() : m_core(m_appName), generator(std::chrono::system_clock::now().time_since_epoch().count())
{
}

VulkanApp::~VulkanApp()
{
}

void VulkanApp::init(bool debug, const std::filesystem::path &path)
{
	m_pWindowControl = std::make_unique<GLFWControl>(m_appName);
	m_pWindowControl->init(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FULL, RESIZE);
	m_core.init(CoreInit {
		.pWindowControl = m_pWindowControl.get(),
		.debug = debug,
	   	.vShader = path / "simple.vert.spv",
	   	.fShader = path / "simple.frag.spv",
	   	.clockwise = false,
	   	.maxFramesInFlight = MAX_FRAMES_IN_FLIGHT,
		.getVertexBindingDescription = Vertex::getBindingDescription,
		.getVertexAttributeDescription = Vertex::getAttributeDescription,
		.getLayoutBindings = Uniform::getLayoutBinding,
		.uniforms = std::make_shared<std::vector<std::vector<size_t>>>(std::vector<std::vector<size_t>> {{sizeof(Uniform)}}),
		.msaaFlags = vk::SampleCountFlagBits::e64,
		.sampleShading = true,
	});
	auto swapCount = m_core.getImageCount();
	m_cmdBufs = m_core.createCommandBuffer(swapCount);
}

int VulkanApp::randomInt(int range)
{
	std::uniform_int_distribution<int> distribution(0, range);
	return distribution(generator);
}

void VulkanApp::addRectangle(int x, int y, int width, int height, const glm::vec4 &color)
{
	int x1 = x;
	int x2 = x + width;
	int y1 = y;
	int y2 = y + height;
	verticies.push_back({{x1, y1}, color});
	verticies.push_back({{x2, y1}, color});
	verticies.push_back({{x1, y2}, color});
	verticies.push_back({{x1, y2}, color});
	verticies.push_back({{x2, y1}, color});
	verticies.push_back({{x2, y2}, color});
}

void VulkanApp::recordCommandBuffer(const vk::CommandBuffer &buf, uint32_t ind)
{
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse,
	};

	auto clearValue = m_core.getClearValue({1.0f, 1.0f, 1.0f, 1.0f});

	vk::RenderPassBeginInfo renderPassInfo {
		.renderPass = m_core.getRenderPass(),
		.framebuffer = m_core.getFb(ind),
		.renderArea {{0, 0}, m_core.getCurrentExtent()},
		.clearValueCount = static_cast<uint32_t>(clearValue.size()),
		.pClearValues = clearValue.data(),
	};

	buf.begin(beginInfo);

	buf.beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
	buf.bindPipeline(vk::PipelineBindPoint::eGraphics, m_core.getPipeline());
	buf.bindVertexBuffers(0, {m_Buffers->getBuffer(0)}, {0});
	buf.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_core.getPipelineLayout(), 0, m_core.getDescSets(ind), {}); 
	buf.draw(verticies.size(), 1, 0, 0);
	buf.endRenderPass();

	buf.end();
}

void VulkanApp::updateUniformBuffer(uint32_t ind)
{
	auto extent = m_core.getCurrentExtent();
	Uniform ubo {
		.res = {extent.width, extent.height},
	};
	m_core.updateUniform(&ubo, sizeof(ubo), ind, 0);
}

void VulkanApp::cleanup()
{
	m_core.waitIdle();
	if (m_Buffers) {
		m_Buffers->destroy();
	}
	m_core.destroyCommandBuffer(m_cmdBufs);
	m_core.cleanup();
	m_pWindowControl->cleanup();
}

void VulkanApp::run()
{
	auto func = [&](bool recreate) {
		if (recreate) {
			m_core.recreateSwapChain();
		}
		verticies.clear();
		for (size_t ii = 0; ii < 500; ++ii) {
			auto extent = m_core.getCurrentExtent();
			std::uniform_real_distribution<float> distribution(0.0f, 1.0f);
			glm::vec4 color {distribution(generator), distribution(generator), distribution(generator), 1.0f};
			addRectangle(randomInt(extent.width - 300), randomInt(extent.height - 300), randomInt(300), randomInt(300), color);
		}
		if (m_Buffers) {
			m_Buffers->destroy();
		}
		m_Buffers = m_core.newBuffers();
		m_Buffers->create({
				{verticies.data(), verticies.size() * sizeof(verticies[0]), vk::BufferUsageFlagBits::eVertexBuffer},
				}, true);
		for (uint32_t i = 0; i < m_cmdBufs.size(); ++i) {
			recordCommandBuffer(m_cmdBufs[i], i);
		}
	};
	func(false);
	while (!m_pWindowControl->shouldClose()) {
		m_pWindowControl->pollEvents();
		if (m_pWindowControl->wasResized()) {
			func(true);
		}
		try {
			auto ind = m_core.getImageIndex();
			updateUniformBuffer(ind);
			m_core.renderScene(m_cmdBufs[ind], ind);
		} catch (VulkanException &e) {
			if (e.isSubopt()) {
				func(true);
			} else {
				throw;
			}
		} catch (vk::OutOfDateKHRError &) {
			func(true);
		}
	}
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	auto argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#else
int main(int argc, const char *argv[])
{
#endif
	auto ret = EXIT_SUCCESS;
	VulkanApp app;
	try {
		app.init(DBG, std::filesystem::path(argv[0]).parent_path());
		app.run();
	} catch (VulkanException &e) {
		std::cout << e.what() << std::endl;
		ret = EXIT_FAILURE;
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		ret =  EXIT_FAILURE;
	}
	app.cleanup();
	return ret;
}

