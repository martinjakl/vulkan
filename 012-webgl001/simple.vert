#version 450

precision mediump float;

layout(location = 0) in vec2 a_position;
layout(location = 1) in vec4 a_color;
layout(location = 0) out vec4 o_color;
layout (binding = 0) uniform Uniform {vec2 u_resolution;} ubo;
void main() {
	vec2 zeroToOne = a_position / ubo.u_resolution;
	vec2 zeroToTwo = zeroToOne * 2.0;
	vec2 clipSpace = zeroToTwo - 1.0;
	gl_Position = vec4(clipSpace * vec2(1, -1), 0, 1);
	o_color = a_color;
}
