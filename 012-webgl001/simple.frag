#version 450

precision mediump float;

layout(location = 0) out vec4 outColor;
layout(location = 0) in vec4 a_color;

void main() {
	outColor = a_color;
}

