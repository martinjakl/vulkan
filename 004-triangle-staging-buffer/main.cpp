#ifdef _WIN32
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#endif
#include <vulkan_core.h>
#include <glfw_control.h>
#include <memory>
#include <iostream>
#include <vulkan_exception.h>
#include <cstdlib>
#include <filesystem>
#include <glm/glm.hpp>

using macsnet::vulkan::glfw::GLFWControl;
using macsnet::vulkan::core::VulkanCore;
using macsnet::vulkan::core::Buffers;
using macsnet::vulkan::core::CoreInit;
using macsnet::vulkan::exception::VulkanException;

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;
const bool WINDOW_FULL = false;
const int MAX_FRAMES_IN_FLIGHT = 2;
const bool RESIZE = true;
const std::string APPNAME = "Triangle staging buffer";

class VulkanApp {
	public:
		VulkanApp();
		~VulkanApp();
		void init(bool debug, const std::filesystem::path &path);
		void run();
		void cleanup();
	private:
		struct Vertex {
			glm::vec2 pos;
			glm::vec3 color;

			static std::vector<vk::VertexInputBindingDescription> getBindingDescription() {
				std::vector<vk::VertexInputBindingDescription> bd {{
					.binding = 0,
					.stride = sizeof(Vertex),
					.inputRate = vk::VertexInputRate::eVertex,
				}};
				return bd;
			};

			static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription() {
				std::vector<vk::VertexInputAttributeDescription> ad {{
					.location = 0,
					.binding = 0,
					.format = vk::Format::eR32G32Sfloat,
					.offset = offsetof(Vertex, pos),
				}, {
					.location = 1,
					.binding = 0,
					.format = vk::Format::eR32G32B32Sfloat,
					.offset = offsetof(Vertex, color),
				}};
				return ad;
			};
		};

		void recordCommandBuffer(const vk::CommandBuffer &buf, uint32_t ind);

		std::string m_appName = APPNAME;
		std::unique_ptr<GLFWControl> m_pWindowControl;
		VulkanCore m_core;
		std::vector<vk::CommandBuffer> m_cmdBufs;
		const std::vector<Vertex> verticies = {
			{{ 0.0f, -0.5f}, {1.0f, 1.0f, 1.0f}},
			{{ 0.5f,  0.5f}, {0.0f, 1.0f, 0.0f}},
			{{-0.5f,  0.5f}, {0.0f, 0.0f, 1.0f}},
		};
		const std::vector<Vertex> verticies2 = {
			{{ 0.5f, -0.5f}, {1.0f, 1.0f, 0.0f}},
			{{ 0.0f,  0.5f}, {1.0f, 1.0f, 1.0f}},
			{{-0.5f, -0.5f}, {1.0f, 0.0f, 1.0f}}
		};
		std::unique_ptr<Buffers> m_vertBuffer;
};

VulkanApp::VulkanApp() : m_core(m_appName)
{
}

VulkanApp::~VulkanApp()
{
}

void VulkanApp::init(bool debug, const std::filesystem::path &path)
{
	m_pWindowControl = std::make_unique<GLFWControl>(m_appName);
	m_pWindowControl->init(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FULL, RESIZE);
	m_core.init(CoreInit {
		.pWindowControl = m_pWindowControl.get(),
		.debug = debug,
	   	.vShader = path / "simple.vert.spv",
	   	.fShader = path / "simple.frag.spv",
	   	.clockwise = true,
	   	.maxFramesInFlight = MAX_FRAMES_IN_FLIGHT,
		.getVertexBindingDescription = Vertex::getBindingDescription,
		.getVertexAttributeDescription = Vertex::getAttributeDescription,
	});
	m_cmdBufs = m_core.createCommandBuffer(m_core.getImageCount());
	m_vertBuffer = m_core.newBuffers();
	m_vertBuffer->create({{verticies.data(), verticies.size() * sizeof(Vertex), vk::BufferUsageFlagBits::eVertexBuffer}, {verticies2.data(), verticies2.size() * sizeof(Vertex), vk::BufferUsageFlagBits::eVertexBuffer}}, true);
}

void VulkanApp::recordCommandBuffer(const vk::CommandBuffer &buf, uint32_t ind)
{
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse,
	};

	auto clearValue = m_core.getClearValue({0.0f, 0.0f, 0.0f, 1.0f});

	vk::RenderPassBeginInfo renderPassInfo {
		.renderPass = m_core.getRenderPass(),
		.framebuffer = m_core.getFb(ind),
		.renderArea {{0, 0}, m_core.getCurrentExtent()},
		.clearValueCount = static_cast<uint32_t>(clearValue.size()),
		.pClearValues = clearValue.data(),
	};

	buf.begin(beginInfo);

	buf.beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
	buf.bindPipeline(vk::PipelineBindPoint::eGraphics, m_core.getPipeline());
	buf.bindVertexBuffers(0, {m_vertBuffer->getBuffer(0)}, {0});
	buf.draw(verticies.size(), 1, 0, 0);
	buf.bindVertexBuffers(0, {m_vertBuffer->getBuffer(1)}, {0});
	buf.draw(verticies2.size(), 1, 0, 0);
	buf.endRenderPass();

	buf.end();
}

void VulkanApp::cleanup()
{
	m_core.waitIdle();
	m_vertBuffer->destroy();
	m_core.destroyCommandBuffer(m_cmdBufs);
	m_core.cleanup();
	m_pWindowControl->cleanup();
}

void VulkanApp::run()
{
	auto func = [&](bool recreate) {
		if (recreate) {
			m_core.recreateSwapChain();
		}
		for (uint32_t i = 0; i < m_cmdBufs.size(); ++i) {
			recordCommandBuffer(m_cmdBufs[i], i);
		}
	};
	func(false);
	while (!m_pWindowControl->shouldClose()) {
		m_pWindowControl->pollEvents();
		if (m_pWindowControl->wasResized()) {
			func(true);
		}
		try {
			auto ind = m_core.getImageIndex();
			m_core.renderScene(m_cmdBufs[ind], ind);
		} catch (VulkanException &e) {
			if (e.isSubopt()) {
				func(true);
			} else {
				throw;
			}
		} catch (vk::OutOfDateKHRError &) {
			func(true);
		}
	}
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	auto argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#else
int main(int argc, const char *argv[])
{
#endif
	auto ret = EXIT_SUCCESS;
	VulkanApp app;
	try {
		app.init(DBG, std::filesystem::path(argv[0]).parent_path());
		app.run();
	} catch (VulkanException &e) {
		std::cout << e.what() << std::endl;
		ret = EXIT_FAILURE;
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		ret =  EXIT_FAILURE;
	}
	app.cleanup();
	return ret;
}

